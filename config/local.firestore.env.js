'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"local-firestore"',
  VERSION: JSON.stringify(require('../package.json').version)
})
