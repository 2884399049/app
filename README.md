# Scribeless Interface

## Release

## TODO
- changed local to TEST env
- do all queries run on PROD e.g. build indexes?

- Stationery editor


- Create campaign
  - Add recipients
    - `[Vue warn]: Error in v-on handler: "TypeError: Cannot read property 'load_stationary' of undefined"`

## Setup
1. Install local packages: `npm install .; cd functions; npm install .`
2. Setup the emulators `./node_modules/.bin/firebase setup:emulators:firestore`

## Getting started
- `npm run start:local` opens local interface pointing at local firebase env (see Running locally for more details)
- `npm run start:dev` opens local interface pointing at remote DEV firebase env
- `npm run start:test` opens local interface pointing at remote TEST firebase env
- `npm run start:prod` opens local interface pointing at remote PROD firebase env

### Running locally
Scheduled functions or firestore triggers cannot be emulated locally and we use the remote version of these functions to test. Only onCall or onRequest triggers are emulated locally.

We can emulate firebase functions locally. This can be started in two ways:
- `npm run start:local-with-emulator` which launches the emulator along with the local server
- `npm run start:local-emulator` which just launches the emulator and then you can run `npm run start:local` to launch just the local server

We can use the firebase shell to call scheduled functions `npm run start:shell` to start the shell

## Linting
- `npm run lint` lints all code
- `npm run lint:interface` lints front end code
- `npm run lint:functions` lints back end code

## Documentation
We use jsdoc to create documentation
- `npm run doc` creates these docs

## Firestore migrations
https://github.com/kevlened/fireway
- `npm run migrate:dev` Apply migration to dev environment
- `npm run migrate:dev-dryrun` Dryrun migration to dev environment

## Tests
We test functions, firestore rules, vue components, and integration tests. We use jest as our test framework. For firebase we emulator local a firestore.

- `npm run test` Run all tests with emulators
- `npm run test:integration` Run integration tests with cypress, requires dev environment emulated
- `npm run test:functions` Run firebase function tests (without emulator)
- `npm run test:functions-with-emulator` Run firebase functions tests
- `npm run test:interface` Run vue interface tests (without emulator)
- `npm run test:interface-with-emulator` Run vue interface tests
- `npm run test:rules` Run firestore rules tests (without emulator)
- `npm run test:rules-with-emulator` Run firestore functions tests

## Clean
Cleans test environment by deleting all users and firestore database
- `cd scripts; npm run delete:dev` - Deletes users and firestore data from DEV env

## Deploy
- `npm run deploy:dev` Deploys to development env
- Deploy to test by pushing to testing branch
- Deploy to production by pushing to master branch

## Deploy to Testing
- `npm run deploy:test` Deploys project to testing env
- `npm run deploy:test:functions` Deploys only the functions to testing env

If there is any issue regarding the test cases, remove `npm run test` from the `deploy:test` script.

Individual function can also be deployed by replacing the `--only functions` from `deploy:test:functions` script with `--only functions:{function-name-to-deploy}`


## Tools
- fireway
- firebase
- jest
- eslint
- webpack
- Vue
