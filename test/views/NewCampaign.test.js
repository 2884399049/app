import Vue from 'vue'
import VueMixin from '@/lib/vue_mixin'

import mocksdk from 'firebase-mock'

import { shallowMount } from "@vue/test-utils"
import NewCampaign from "@/views/NewCampaign.vue"
import * as constants from '@/../functions/shared/constants.js'
import api from '@/services/api.js'

import * as firebase from "@/lib/firebase.js"

let mock_firestore = new mocksdk.MockFirestore()
let mock_auth = new mocksdk.MockAuthentication()

firebase.db = mock_firestore
firebase.auth = {currentUser: {uid: '1'}}

// jest.mock('@/lib/firebase.js', () => {
//   return {
//     db: mock_firestore,
//     auth: {
//       currentUser: {uid: '1'}
//     },
//     serverTimestamp: jest.fn(),
//     firestoreDocumentID: jest.fn()
//   }
// })

Vue.mixin(VueMixin)

const mock_user_profile = {
  id: '1',
  address: {}
}

const mock_price_table = {
  // TODO
}

const $route = {
  query: {},
  params: {id: 'new'}
}

const $refs = {
  message: {
    check_bad_text: jest.fn()
  },
  wizard: {
    nextTab: jest.fn(),
    prevTab: jest.fn()
  },
  payment: {
    validate: jest.fn().mockResolvedValue(true)
  }
}

const mock_campaign_id = '123'
const mock_stationary_id = '2'

mock_firestore.collection('campaigns').doc(mock_campaign_id).autoFlush()

const StubMessageEdit = {
  render: () => {},
  methods: {
  	check_bad_text: jest.fn()
  }
}

describe("NewCampaign.vue", () => {
  let component_wrapper
  let mock_swal
  let mock_campaign_options

  beforeEach(async () => {
    mock_swal = jest.fn()

    mock_campaign_options = {
      stationary_required: false,
      product: constants.PRODUCT.full_service,
      data: [{
        'index': 0,
        'first name': null,
        'last name': 'mock recipient 1 last name',
        'company': null,
        'address line 1': null,
        'address line 2': null,
        'city': null,
        'region': null,
        'postal_code': null,
        'country': null
      }],
      is_sample: false,
      data_source: constants.DATA_SOURCE.upload,
      delivery: {
        sender: constants.SENDER.ship_for_me,
        due_date: '01/01/2000',
        user_address: {last_name: 'last_name'}
      },
      envelope: {
        required: constants.ENVELOPE_REQUIRED.no,
        paper: constants.ENVELOPE_SIZES.C5
      }
    }

    component_wrapper = shallowMount(NewCampaign, {
      propsData: {user_profile:  mock_user_profile, price_table: mock_price_table},
      mocks: {
        $swal: mock_swal,
        $route,
        $firestoreRefs: {new_campaign: {id: mock_campaign_id}, stationary: {id: mock_stationary_id}}
      },
      stubs: {
        MessageEdit: StubMessageEdit
      }
    })
  })

  it("shows error if api returns an error", async () => {
    component_wrapper.setData({campaign: mock_campaign_options})

    const mock_add_recipients = jest.fn().mockResolvedValue({data: {success: false, error_message: 'error'}})
    api.add_recipients = mock_add_recipients

    let was_success = await component_wrapper.vm.upload_campaign()

    expect(component_wrapper.vm.new_campaign_id).toEqual(mock_campaign_id)
    expect(was_success).toEqual(false)
    expect(mock_swal).toHaveBeenCalled()
  })

  // Throwing error not working?
  it.skip("shows error if api throws an error", async () => {
    component_wrapper.setData({campaign: mock_campaign_options})

    const mock_add_recipients = jest.fn().mockRejectedValue('API error')
    api.add_recipients = mock_add_recipients

    let was_success = await component_wrapper.vm.upload_campaign()

    expect(component_wrapper.vm.new_campaign_id).toEqual(mock_campaign_id)
    expect(was_success).toEqual(false)
    expect(mock_swal).toHaveBeenCalled()
  })

  it("adds user address for sample campaigns", async () => {
    mock_campaign_options.is_sample = true
    component_wrapper.setData({campaign: mock_campaign_options})

    const mock_add_recipients = jest.fn().mockResolvedValue({data: {success: true}})
    api.add_recipients = mock_add_recipients

    let was_success = await component_wrapper.vm.upload_campaign()

    expect(component_wrapper.vm.new_campaign_id).toEqual(mock_campaign_id)
    expect(was_success).toEqual(true)
    expect(mock_swal).not.toHaveBeenCalled()

    let expected_recipients = [{
      'index': 0,
      'first name': null,
      'last name': 'last_name',
      'company': null,
      'address line 1': null,
      'address line 2': null,
      'city': null,
      'region': null,
      'postal_code': null,
      'country': null
    }]

    expect(mock_add_recipients).toHaveBeenCalledWith(expected_recipients, mock_campaign_id)
  })

  it.skip("if add_recipients_only dosen't update campaign or stationery", async () => {
    mock_campaign_options.add_recipients_only = true
    mock_campaign_options.stationary_required = true
    component_wrapper.setData({campaign: mock_campaign_options})

    mock_firestore.collection('stationary').doc(mock_stationary_id).failNext('set', new Error('123'))
    mock_firestore.collection('campaigns').doc(mock_campaign_id).failNext('set', new Error('123'))

    const mock_add_recipients = jest.fn().mockResolvedValue({data: {success: true}})
    api.add_recipients = mock_add_recipients

    let was_success = await component_wrapper.vm.upload_campaign()

    expect(component_wrapper.vm.new_campaign_id).toEqual(mock_campaign_id)
    expect(was_success).toEqual(true)
    expect(mock_swal).not.toHaveBeenCalled()

    expect(mock_add_recipients).toHaveBeenCalledWith(mock_campaign_options.data, mock_campaign_id)
  })

  it.skip("it updates campaign and stationery", async () => {
    mock_campaign_options.stationary_required = true
    component_wrapper.setData({campaign: mock_campaign_options})

    mock_firestore.collection('stationary').doc(mock_stationary_id).failNext('set', new Error('123'))
    mock_firestore.collection('campaigns').doc(mock_campaign_id).failNext('set', new Error('123'))

    const mock_add_recipients = jest.fn().mockResolvedValue({data: {success: true}})
    api.add_recipients = mock_add_recipients

    let was_success = await component_wrapper.vm.upload_campaign()

    expect(component_wrapper.vm.new_campaign_id).toEqual(mock_campaign_id)
    expect(was_success).toEqual(true)
    expect(mock_swal).not.toHaveBeenCalled()

    expect(mock_add_recipients).toHaveBeenCalledWith(mock_campaign_options.data, mock_campaign_id)
  })
})
