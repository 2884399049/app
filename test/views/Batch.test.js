import Vue from "vue";

import { db, auth } from "@/lib/firebase.js";
import { shallowMount } from "@vue/test-utils";
import Batch from "@/views/Batch.vue";

import VueMixin from "@/lib/vue_mixin";

Vue.mixin(VueMixin);

const mock_batch = {
	id: "1"
};

const $route = {
	params: {
		id: mock_batch.id
	}
};

describe("Batch.vue", () => {
	let component_wrapper;
	let mock_user_profile;

	beforeEach(async () => {
		mock_user_profile = {
			id: "1",
			admin: true
		};

		component_wrapper = shallowMount(Batch, {
			mocks: { $route },
			propsData: { user_profile: mock_user_profile }
		});

		component_wrapper.setData({ batch: mock_batch });

		component_wrapper.vm.$firestoreRefs = {
			batch: {
				update: jest.fn()
			}
		};
	});

	it("has correct audit reference", () => {
		expect(component_wrapper.vm.audit_ref).toEqual(
			db
				.collection("batches")
				.doc(mock_batch.id)
				.collection("audit")
		);
	});

	it("adds meta data to update", async () => {
		await component_wrapper.vm.update_batch();
		expect(component_wrapper.vm.batch.updated_by).toEqual(
			mock_user_profile.id
		);
	});
});
