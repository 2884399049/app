import Vue from 'vue'

// import { db, auth } from "@/lib/firebase.js"
import { shallowMount } from "@vue/test-utils"
import CampaignList from "@/views/CampaignList.vue"
import * as constants from '@/../functions/shared/constants.js'

const mock_user_profile = {
  id: '1'
}

const mock_user_admin_profile = {
  id: '1',
  admin: true
}

const mock_campaign_1 = {
  uid: '1',
  status: constants.CAMPAIGN_STATUS.deleted
}

const mock_campaign_2 = {
  uid: '2',
  status: constants.CAMPAIGN_STATUS.pending
}

const $route = {
  query: {}
}

describe("CampaignList.vue", () => {
  it("hides deleted campaigns for non admin users", () => {
    let component_wrapper = shallowMount(CampaignList, {
      propsData: { user_profile:  mock_user_profile},
      mocks: {$route}
    })

    component_wrapper.setData({campaigns: [mock_campaign_1, mock_campaign_2]})

    component_wrapper.vm.$options.watch.campaigns.handler.call(component_wrapper.vm)

    expect(component_wrapper.vm.items).toEqual([mock_campaign_2])
    expect(component_wrapper.vm.loaded_users).toEqual({})
  })

  it("shows deleted campaigns for admin users", () => {
    let component_wrapper = shallowMount(CampaignList, {
      propsData: { user_profile:  mock_user_admin_profile},
      mocks: {$route}
    })

    component_wrapper.setData({campaigns: [mock_campaign_1, mock_campaign_2]})

    component_wrapper.vm.$options.watch.campaigns.handler.call(component_wrapper.vm)

    expect(component_wrapper.vm.items).toEqual([mock_campaign_1, mock_campaign_2])
    expect(component_wrapper.vm.loaded_users).toEqual({})
  });
});
