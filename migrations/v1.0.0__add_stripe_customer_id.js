const utils = require('../scripts/utils')

async function action(user, ref) {
  if (user.stripe_customer) {
    return ref.update({stripe_customer_id: user.stripe_customer.id})
  }
}

module.exports.migrate = async ({firestore}) => {
  // await utils.break_up_query(firestore.collection('users'), action)

  // let user_docs = await firestore.collection('users').get()
  // let proms = []
  // for (let user_doc of user_docs.docs) {
  //   let user = user_doc.data()
  //   if (user.stripe_customer) {
  //     // TODO proms.push(firestore.collection('users').doc(user_doc.id).update({stripe_customer_id: user.stripe_customer.id}))
  //   }
  // }

  // return Promise.all(proms)
}
