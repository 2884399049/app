let constants = require('../functions/shared/constants')
const utils = require('../scripts/utils')

let products = {}

products[constants.GOOD.stamp_gb_first_class] = {
  goods: [constants.GOOD.stamp_gb_first_class],
  tiers: {
    'eur': {1: 76 * constants.CURRENCY.gbp_to_eur},
    'usd': {1: 76 * constants.CURRENCY.gbp_to_usd},
    'gbp': {1: 76}
  }
}

products[constants.GOOD.stamp_gb_second_class] = {
  goods: [constants.GOOD.stamp_gb_second_class],
  tiers: {
    'eur': {1: 65 * constants.CURRENCY.gbp_to_eur},
    'usd': {1: 65 * constants.CURRENCY.gbp_to_usd},
    'gbp': {1: 65}
  }
}


module.exports.migrate = async ({firestore}) => {
  let proms = []
  for (let product_key in products) {
    let product = products[product_key]
    proms.push(firestore.collection('products').doc(product_key).set(product))
  }

  return Promise.all(proms)
}
