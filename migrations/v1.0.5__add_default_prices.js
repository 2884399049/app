let constants = require('../functions/shared/constants')
const utils = require('../scripts/utils')

let products = {}
products['Self Service'] = {
  goods: [constants.GOOD.self_service_a4, constants.GOOD.self_service_a5, constants.GOOD.self_service_a6, constants.GOOD.self_service_c5, constants.GOOD.self_service_c6, constants.GOOD.self_service_dl],
  tiers: {
    'eur': {
      1: 55 * constants.CURRENCY.gbp_to_eur,
      500: 45 * constants.CURRENCY.gbp_to_eur,
      2500: 35 * constants.CURRENCY.gbp_to_eur,
      10000: 25 * constants.CURRENCY.gbp_to_eur
    },
    'usd': {
      1: 55 * constants.CURRENCY.gbp_to_usd,
      500: 45 * constants.CURRENCY.gbp_to_usd,
      2500: 35 * constants.CURRENCY.gbp_to_usd,
      10000: 25 * constants.CURRENCY.gbp_to_usd
    },
    'gbp': {
      1: 55,
      500: 45,
      2500: 35,
      10000: 25
    }
  }
}

products[constants.GOOD.full_service_a4_pack] = {
  goods: [constants.GOOD.full_service_a4_pack],
  tiers: {
    'eur': {
      1: 250, 100: 235, 250: 220, 500: 205, 1000: 190, 2500: 180, 5000: 170, 7500: 160, 10000: 140, 50000: 130, 100000: 120
    },
    'usd': {
      1: 275, 100: 260, 250: 245, 500: 230, 1000: 215, 2500: 200, 5000: 185, 7500: 170, 10000: 155, 50000: 140, 100000: 125
    },
    'gbp': {
      1: 210, 100: 199, 250: 188, 500: 177, 1000: 166, 2500: 155, 5000: 144, 7500: 133, 10000: 122, 50000: 111, 100000: 100
    }
  }
}

products[constants.GOOD.full_service_a5_pack] = {
  goods: [constants.GOOD.full_service_a5_pack],
  tiers: {
    'eur': {
      1: 250, 100: 235, 250: 220, 500: 205, 1000: 190, 2500: 180, 5000: 170, 7500: 160, 10000: 140, 50000: 130, 100000: 120
    },
    'usd': {
      1: 275, 100: 260, 250: 245, 500: 230, 1000: 215, 2500: 200, 5000: 185, 7500: 170, 10000: 155, 50000: 140, 100000: 125
    },
    'gbp': {
      1: 210, 100: 199, 250: 188, 500: 177, 1000: 166, 2500: 155, 5000: 144, 7500: 133, 10000: 122, 50000: 111, 100000: 100
    }
  }
}

products[constants.GOOD.full_service_a6_pack] = {
  goods: [constants.GOOD.full_service_a6_pack],
  tiers: {
    'eur': {
      1: 250, 100: 235, 250: 220, 500: 205, 1000: 190, 2500: 180, 5000: 170, 7500: 160, 10000: 140, 50000: 130, 100000: 120
    },
    'usd': {
      1: 275, 100: 260, 250: 245, 500: 230, 1000: 215, 2500: 200, 5000: 185, 7500: 170, 10000: 155, 50000: 140, 100000: 125
    },
    'gbp': {
      1: 210, 100: 199, 250: 188, 500: 177, 1000: 166, 2500: 155, 5000: 144, 7500: 133, 10000: 122, 50000: 111, 100000: 100
    }
  }
}

products[constants.GOOD.full_service_a4] = {
  goods: [constants.GOOD.full_service_a4],
  tiers: {
    'eur': {
      1: 156, 100: 147, 250: 138, 500: 128, 1000: 119, 2500: 113, 5000: 106, 7500: 100, 10000: 88, 50000: 81, 100000: 75
    },
    'usd': {
      1: 172, 100: 163, 250: 153, 500: 144, 1000: 134, 2500: 125, 5000: 116, 7500: 106, 10000: 97, 50000: 88, 100000: 78
    },
    'gbp': {
      1: 131, 100: 124, 250: 118, 500: 111, 1000: 104, 2500: 97, 5000: 90, 7500: 83, 10000: 76, 50000: 69, 100000: 63
    }
  }
}

products[constants.GOOD.full_service_a5] = {
  goods: [constants.GOOD.full_service_a5],
  tiers: {
    'eur': {
      1: 156, 100: 147, 250: 138, 500: 128, 1000: 119, 2500: 113, 5000: 106, 7500: 100, 10000: 88, 50000: 81, 100000: 75
    },
    'usd': {
      1: 172, 100: 163, 250: 153, 500: 144, 1000: 134, 2500: 125, 5000: 116, 7500: 106, 10000: 97, 50000: 88, 100000: 78
    },
    'gbp': {
      1: 131, 100: 124, 250: 118, 500: 111, 1000: 104, 2500: 97, 5000: 90, 7500: 83, 10000: 76, 50000: 69, 100000: 63
    }
  }
}

products[constants.GOOD.full_service_a6] = {
  goods: [constants.GOOD.full_service_a6],
  tiers: {
    'eur': {
      1: 156, 100: 147, 250: 138, 500: 128, 1000: 119, 2500: 113, 5000: 106, 7500: 100, 10000: 88, 50000: 81, 100000: 75
    },
    'usd': {
      1: 172, 100: 163, 250: 153, 500: 144, 1000: 134, 2500: 125, 5000: 116, 7500: 106, 10000: 97, 50000: 88, 100000: 78
    },
    'gbp': {
      1: 131, 100: 124, 250: 118, 500: 111, 1000: 104, 2500: 97, 5000: 90, 7500: 83, 10000: 76, 50000: 69, 100000: 63
    }
  }
}

products[constants.GOOD.full_service_c5] = {
  goods: [constants.GOOD.full_service_c5],
  tiers: {
    'eur': {
      1: 156, 100: 147, 250: 138, 500: 128, 1000: 119, 2500: 113, 5000: 106, 7500: 100, 10000: 88, 50000: 81, 100000: 75
    },
    'usd': {
      1: 172, 100: 163, 250: 153, 500: 144, 1000: 134, 2500: 125, 5000: 116, 7500: 106, 10000: 97, 50000: 88, 100000: 78
    },
    'gbp': {
      1: 131, 100: 124, 250: 118, 500: 111, 1000: 104, 2500: 97, 5000: 90, 7500: 83, 10000: 76, 50000: 69, 100000: 63
    }
  }
}

products[constants.GOOD.full_service_c6] = {
  goods: [constants.GOOD.full_service_c6],
  tiers: {
    'eur': {
      1: 156, 100: 147, 250: 138, 500: 128, 1000: 119, 2500: 113, 5000: 106, 7500: 100, 10000: 88, 50000: 81, 100000: 75
    },
    'usd': {
      1: 172, 100: 163, 250: 153, 500: 144, 1000: 134, 2500: 125, 5000: 116, 7500: 106, 10000: 97, 50000: 88, 100000: 78
    },
    'gbp': {
      1: 131, 100: 124, 250: 118, 500: 111, 1000: 104, 2500: 97, 5000: 90, 7500: 83, 10000: 76, 50000: 69, 100000: 63
    }
  }
}

products[constants.GOOD.full_service_dl] = {
  goods: [constants.GOOD.full_service_dl],
  tiers: {
    'eur': {
      1: 156, 100: 147, 250: 138, 500: 128, 1000: 119, 2500: 113, 5000: 106, 7500: 100, 10000: 88, 50000: 81, 100000: 75
    },
    'usd': {
      1: 172, 100: 163, 250: 153, 500: 144, 1000: 134, 2500: 125, 5000: 116, 7500: 106, 10000: 97, 50000: 88, 100000: 78
    },
    'gbp': {
      1: 131, 100: 124, 250: 118, 500: 111, 1000: 104, 2500: 97, 5000: 90, 7500: 83, 10000: 76, 50000: 69, 100000: 63
    }
  }
}

products[constants.GOOD.insert_light] = {
  goods: [constants.GOOD.insert_light],
  tiers: {
    'eur': {1: 50 * constants.CURRENCY.gbp_to_eur},
    'usd': {1: 50 * constants.CURRENCY.gbp_to_usd},
    'gbp': {1: 50}
  }
}

products[constants.GOOD.stamp_gb_first_class] = {
  goods: [constants.GOOD.stamp_gb_first_class],
  tiers: {
    'eur': {1: 70 * constants.CURRENCY.gbp_to_eur},
    'usd': {1: 70 * constants.CURRENCY.gbp_to_usd},
    'gbp': {1: 70}
  }
}

products[constants.GOOD.stamp_gb_second_class] = {
  goods: [constants.GOOD.stamp_gb_second_class],
  tiers: {
    'eur': {1: 61 * constants.CURRENCY.gbp_to_eur},
    'usd': {1: 61 * constants.CURRENCY.gbp_to_usd},
    'gbp': {1: 61}
  }
}

products[constants.GOOD.stamp_gb_europe] = {
  goods: [constants.GOOD.stamp_gb_europe],
  tiers: {
    'eur': {1: 135 * constants.CURRENCY.gbp_to_eur},
    'usd': {1: 135 * constants.CURRENCY.gbp_to_usd},
    'gbp': {1: 135}
  }
}

products[constants.GOOD.stamp_gb_world_zone_1] = {
  goods: [constants.GOOD.stamp_gb_world_zone_1],
  tiers: {
    'eur': {1: 155 * constants.CURRENCY.gbp_to_eur},
    'usd': {1: 155 * constants.CURRENCY.gbp_to_usd},
    'gbp': {1: 155}
  }
}

products[constants.GOOD.stamp_gb_world_zone_2] = {
  goods: [constants.GOOD.stamp_gb_world_zone_2],
  tiers: {
    'eur': {1: 155 * constants.CURRENCY.gbp_to_eur},
    'usd': {1: 155 * constants.CURRENCY.gbp_to_usd},
    'gbp': {1: 155}
  }
}

products[constants.GOOD.stamp_us_international] = {
  goods: [constants.GOOD.stamp_us_international],
  tiers: {
    'eur': {1: 115 * constants.CURRENCY.usd_to_eur},
    'usd': {1: 115},
    'gbp': {1: 115 * constants.CURRENCY.usd_to_gbp}
  }
}

products[constants.GOOD.stamp_us_first_class] = {
  goods: [constants.GOOD.stamp_us_first_class],
  tiers: {
    'eur': {1: 55 * constants.CURRENCY.usd_to_eur},
    'usd': {1: 55},
    'gbp': {1: 55 * constants.CURRENCY.usd_to_gbp}
  }
}

products[constants.GOOD.ship_to_self_us] = {
  goods: [constants.GOOD.ship_to_self_us],
  tiers: {
    'eur': {1: 3000 * constants.CURRENCY.gbp_to_eur},
    'usd': {1: 3000 * constants.CURRENCY.gbp_to_usd},
    'gbp': {1: 3000}
  }
}

products[constants.GOOD.ship_to_self_gb] = {
  goods: [constants.GOOD.ship_to_self_gb],
  tiers: {
    'eur': {1: 3000 * constants.CURRENCY.gbp_to_eur},
    'usd': {1: 3000 * constants.CURRENCY.gbp_to_usd},
    'gbp': {1: 3000}
  }
}

module.exports.migrate = async ({firestore}) => {
  await utils.delete_collection(firestore.collection('products'))

  let proms = []
  for (let product_key in products) {
    let product = products[product_key]
    proms.push(firestore.collection('products').doc(product_key).set(product))
  }

  return Promise.all(proms)
}
