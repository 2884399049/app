const utils = require('../scripts/utils')

async function action(user, ref) {
  if (!user.created) {
    let created = new Date()
    created.setMonth(created.getMonth() - 6)
    created.setHours(0, 0, 0)
    created.setMilliseconds(0)
    return ref.update({created: created})
  }
}

module.exports.migrate = async ({firestore}) => {
  await utils.break_up_query(firestore.collection('users'), action)
}
