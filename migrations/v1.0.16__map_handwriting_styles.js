let constants = require('../functions/shared/constants')
const utils = require('../scripts/utils')

const STYLE_LOOKUP = {
  'Jane': 'Stafford', // Autumn 5WGWVZ1G00XB
  'Ernest': 'Stafford', // Barrow
  'Mark': 'Stafford', // Selfridge
  'Virginia': 'Stafford', // Thama
  'William': 'Stafford', // Hampton

  'Usther': 'Usther',
  'Williams': 'Stafford',
  'Kunjar': 'Stafford',
  'Stafford': 'Stafford',
  'Tremblay': 'Tremblay',
  'Ginsberg': 'Stafford',
  'George': 'George',
  'Foster': 'Foster',

  'Whitman': 'Stafford', // Autumn, 5WGWVZ1G00XB
  'Dickinson': 'George', // Irwin, 8X3WQ64R00B2
  'Plath': 'Stafford', // Abbey, 8X3WQ4D800B0
  'Emerson': 'Stafford', // Dumont, 8X3WPWHG00AQ

  'Peyton': 'Stafford',
  'Easley': 'Stafford',
  'Lytle': 'George',
  'Parker': 'Stafford',
  'Brady': 'George',
  'Sawdon': 'Stafford',
  'Sutton': 'Stafford',
  'Abrahim': 'Stafford',
  'Mabus': 'Stafford',

  'Simon': 'Stafford',
  'Freud': 'Stafford',
  'Sellers': 'Stafford',
  'Chaplin': 'Stafford',
  'Robinson': 'Stafford',
  'Thomas': 'Stafford'
}

const COLOUR_LOOKUP = {
  'blue': 'Blue',
  'black': 'Black'
}

async function action(stationary, ref) {
  let updates = {
    handwriting_style: STYLE_LOOKUP[stationary.handwriting_style] || 'Stafford',
    handwriting_colour: COLOUR_LOOKUP[stationary.handwriting_colour] || 'Blue',
  }

  if (['Stafford', 'George', 'Foster', 'Tremblay', 'Usther'].indexOf(stationary.handwriting_style) == -1 || ['Blue', 'Black'].indexOf(stationary.handwriting_colour) == -1) {
    return ref.update(updates)
  }
}

module.exports.migrate = async ({firestore}) => {
  await utils.break_up_query(firestore.collection('stationary').where('handwriting_colour', '==', 'black'), action)
}
