const utils = require('../scripts/utils')
let FieldValue = require('firebase-admin').firestore.FieldValue

async function action(user, ref) {
  if (user.stripe_discount) {
    return ref.update({stripe_discount: null}) // TODO should be FieldValue.delete()
  }
}

module.exports.migrate = async ({firestore}) => {
  await utils.break_up_query(firestore.collection('users'), action)
}
