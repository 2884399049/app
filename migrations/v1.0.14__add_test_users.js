let constants = require('../functions/shared/constants')
const utils = require('../scripts/utils')
const admin = require('firebase-admin')

// admin.initializeApp({
//   credential: admin.credential.cert(require("../config/hc-application-interface-prod.json")),
//   databaseURL: 'https://hc-application-interface-prod.firebaseio.com'
// })

module.exports.migrate = async ({firestore}) => {
  let users = [
    {country: 'US', currency: 'usd', currency_symbol: '$', email: 'ar+test-US@scribeless.co', name: 'TESTUS', admin: false},
    {country: 'GB', currency: 'gbp', currency_symbol: '£', email: 'ar+test-GB@scribeless.co', name: 'TESTGB', admin: false},
    {country: 'GB', currency: 'gbp', currency_symbol: '£', email: 'ar+test-admin-GB@scribeless.co', name: 'TESTADMINGB', admin: true},
  ]

  for (let user_data of users) {
    let user

    try {
      user = await admin.auth().createUser({
        email: user_data.email,
        emailVerified: true,
        password: user_data.email,
        displayName: `${user_data.name} ${user_data.name}`,
        disabled: false
      })
    } catch (err) {
      user = await admin.auth().getUserByEmail(user_data.email)
    }

    let user_profile = {
      email: user_data.email,
      samples_left: 1,
      country: user_data.country,
      currency: user_data.currency,
      currency_symbol: user_data.currency_symbol,
      full_name: `${user_data.name} ${user_data.name}`,
      first_name: user_data.name,
      last_name: user_data.name,
      phone: '+11234567890',
      onboarding_complete: true,
      billing_enabled: true,
      api_key: `${user.uid}$1`,
      password: user_data.email,
      created: new Date(),
      last_updated: new Date(),
      address: {
        first_name: 'First',
        last_name: 'Last',
        country: user_data.country,
        address_line_1: 'Address line 1',
        address_line_2: 'Address line 2',
        city: 'CITY',
        postcode: 'POST CODE',
        region: 'AB'
      },
      products_enabled: [constants.PRODUCT.full_service],
      stationery_enabled: [],
      handwriting_styles_enabled: [],
      admin: user_data.admin
    }

    await firestore.collection('users').doc(user.uid).set(user_profile, {merge: true})
  }

  return true
}
