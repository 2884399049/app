// TODO https://github.com/prescottprue/cypress-firebase

import firebase from 'firebase'

firebase.initializeApp({
  apiKey: "AIzaSyCsZ4aXGN1hW9Suwbb6GbaTx7cn9wpCDS0",
  authDomain: "hc-application-interface-dev.firebaseapp.com",
  databaseURL: "https://hc-application-interface-dev.firebaseio.com",
  projectId: "hc-application-interface-dev",
  storageBucket: "hc-application-interface-dev.appspot.com",
  messagingSenderId: "952852633903",
  appId: "1:952852633903:web:375d2c61fe6107e5"
})

context('Stationery', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080')
    firebase.auth().signInWithEmailAndPassword('test-user-1@scribeless.co', 'test-user-1@scribeless.co')
  })

  it('Logs in', () => {
    cy.get('#login_link').click()

    cy.get('#email').type('test-user-1@scribeless.co')
    cy.get('#password').type('test-user-1@scribeless.co')
    cy.get('#login').click()
  })
})
