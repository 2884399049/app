const readline = require('readline-sync')
const admin = require('firebase-admin')
const utils = require('./utils')
let FieldValue = require('firebase-admin').firestore.FieldValue

// TODO due to very large document size this is very slow so done seperatly

// admin.initializeApp({
//   credential: admin.credential.cert(require("../config/hc-application-interface-dev.json")),
//   databaseURL: 'https://hc-application-interface-dev.firebaseio.com'
// })

admin.initializeApp({
  credential: admin.credential.cert(require("../config/hc-application-interface-prod.json")),
  databaseURL: 'https://hc-application-interface-prod.firebaseio.com'
})


let db = admin.firestore()

async function action(user) {
  if (user.stripe_customer) {
    console.log('Update ', user.id)
    return db.collection('users').doc(user.id).update({stripe_customer: FieldValue.delete(), subscription: FieldValue.delete(), stripe_customer_id: user.stripe_customer.id})
  }

  if (user.subscription) {
    console.log('Delete ', user.id)
    return db.collection('users').doc(user.id).update({subscription: FieldValue.delete()})
  }
}

async function main() {
  let answer = readline.question('This will add stripe_customer_id to users! Are you sure you want to continue [yN]? ')
  if (answer !== 'y') {
    console.log('Exiting.')
    return
  }

  await utils.break_up_query(db.collection('users'), action)
}

main()
