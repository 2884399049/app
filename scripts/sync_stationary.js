const readline = require('readline-sync')
const admin = require('firebase-admin')
const constants = require('../functions/shared/constants')

admin.initializeApp({
  credential: admin.credential.cert(require("../config/hc-application-interface-dev.json")),
  databaseURL: 'https://hc-application-interface-dev.firebaseio.com'
})
const db_dev = admin.firestore()

let admin_prod = admin.initializeApp({
  credential: admin.credential.cert(require("../config/hc-application-interface-prod.json")),
  databaseURL: 'https://hc-application-interface-prod.firebaseio.com'
}, "prod")
const db_prod = admin_prod.firestore()

async function main() {
  let answer = readline.question('This will sync PRODUCTION stationary to DEVELOPMENT. Are you sure you want to continue [yN]? ')
  if (answer !== 'y') {
    console.log('Exiting.')
    return
  }

  console.log('Syncing...')
  // await sync()
  console.log('Synced.')
  console.log('Migrating...')
  await migrate()
  console.log('Migrated.')
}

async function migrate() {
  let stationary_front_html = `
  <div class='paper-preview' style="position: absolute; box-sizing: border-box; width: WIDTH; height: HEIGHT;">
    <link id='font-loader' rel="stylesheet" />

    <div id='vertical-line'></div>
    <div id='horizontal-line'></div>

    <div class='paper-header draggable hover-highlight' style="display: none; margin: auto; width: 50mm; height: 25mm; color: #333; font-family: 'Poiret One', cursive; top: 6mm !important; position: relative; overflow: hidden;">
      <p id="header-text" style="margin-top: 0px; display: none; font-size: 8mm; text-align: center; width: 100%;"></p>
      <img id="header-image" class='paper-logo' style="display: none; margin: 0 auto; width: 100%;" />
    </div>

    <div id="background-image-container" class='paper-element' style='position: absolute; width: WIDTH; height: HEIGHT'>
      <img id="background-image" width="100%" src="SRC"/>
    </div>

    <div id="handwriting-loading-container" class='paper-text text-center' style="box-sizing: border-box; display: none; padding-left: 6mm; padding-right: 6mm; width: 100%; height: 20mm; z-index: 1; position: absolute; background-size: 56.57px 56.57px; top: HANDWRITING_MARGIN_HEIGHT;">
      <span class='fas fa-spinner fa-pulse mr-2 text_loading_spinner'></span>
    </div>

    <div id="handwriting-image-container" class='paper-text text-center' style="box-sizing: border-box; padding-left: 6mm; padding-right: 6mm; width: 100%; height: HANDWRITING_HEIGHT; overflow: hidden; z-index: 0; position: absolute; background-size: 56.57px 56.57px; top: HANDWRITING_MARGIN_HEIGHT;">
      <img id="handwriting-image" width="100%" />
    </div>

    <div class='paper-footer' style="display: none; width: 100%; color: #333; font-family: 'Carlson'; position: absolute !important; bottom: 4mm;">
      <p id="footer-text" style="display: none; font-size: 5mm; text-align: center; white-space: pre-line; margin: 0px; padding-right: 4mm; padding-left: 4mm;"></p>
    </div>
  </div>`

  let bleed = `${constants.BLEED}mm`
  let bleed_mark_margin = `${constants.BLEED_MARK_MARGIN}mm`

  let stationary_back_html = `
  <div class='paper-preview back-preview' style="position: absolute; width: WIDTH; height: HEIGHT;">
    <div id='vertical-line'></div>
    <div id='horizontal-line'></div>

    <div id="bleed-marks" style="display: none">
      <div style="box-sizing: border-box; height: ${bleed}; width: ${bleed}; position: absolute; top: 0px; left: 0px; pointer-events: none; border-style: solid; border-color: black; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 0px; border-top-width: 0px;"></div>
      <div style="height: ${bleed_mark_margin}; width: ${bleed_mark_margin}; position: absolute; top: calc(${bleed} - ${bleed_mark_margin}); left: calc(${bleed} - ${bleed_mark_margin}); pointer-events: none; background: white;"></div>

      <div style="box-sizing: border-box; height: ${bleed}; width: ${bleed}; position: absolute; bottom: 0px; left: 0px; pointer-events: none; border-style: solid; border-color: black; border-right-width: 1px; border-top-width: 1px; border-left-width: 0px; border-bottom-width: 0px;"></div>
      <div style="height: ${bleed_mark_margin}; width: ${bleed_mark_margin}; position: absolute; bottom: calc(${bleed} - ${bleed_mark_margin}); left: calc(${bleed} - ${bleed_mark_margin}); pointer-events: none; background: white;"></div>

      <div style="box-sizing: border-box; height: ${bleed}; width: ${bleed}; position: absolute; top: 0px; right: 0px; pointer-events: none; border-style: solid; border-color: black; border-left-width: 1px; border-bottom-width: 1px; border-right-width: 0px; border-top-width: 0px;"></div>
      <div style="height: ${bleed_mark_margin}; width: ${bleed_mark_margin}; position: absolute; top: calc(${bleed} - ${bleed_mark_margin}); right: calc(${bleed} - ${bleed_mark_margin}); pointer-events: none; background: white;"></div>

      <div style="box-sizing: border-box; height: ${bleed}; width: ${bleed}; position: absolute; bottom: 0px; right: 0px; pointer-events: none; border-style: solid; border-color: black; border-left-width: 1px; border-top-width: 1px; border-right-width: 0px; border-bottom-width: 0px;"></div>
      <div style="height: ${bleed_mark_margin}; width: ${bleed_mark_margin}; position: absolute; bottom: calc(${bleed} - ${bleed_mark_margin}); right: calc(${bleed} - ${bleed_mark_margin}); pointer-events: none; background: white;"></div>
    </div>

    <div id="back-image-container" class='draggable hover-highlight' style="width: 100%; height: 100%; position: absolute !important">
      <img id="back-image" class="back_image" src="SRC" style="display: block; margin: auto; height: 100%; width: 100%; object-fit: contain; z-index: 1" />
    </div>
  </div>`

  let _stationary_doc_1 = await db_dev.collection('stationary').doc('Wsf1P5v9SkEq53Uc5b1D').get()
  let _stationary_doc_2 = await db_dev.collection('stationary').doc('Mk2I9ZlMLFRMPG8Bx7Ys').get()
  let _stationary_doc_3 = await db_dev.collection('stationary').doc('xC7v8O3sksVp7X7i79lD').get()
  let _stationary_doc_4 = await db_dev.collection('stationary').doc('5CooEvfaSnXcvCG5uAdV').get()
  let _stationary_doc_5 = await db_dev.collection('stationary').doc('u2EYpwV7HWLmarLk7qpf').get()
  let stationary_docs = {docs: [_stationary_doc_1, _stationary_doc_2, _stationary_doc_3, _stationary_doc_4, _stationary_doc_5]}
  let proms = []
  for (let stationary_doc of stationary_docs.docs) {
    let stationary = stationary_doc.data()
    stationary.id = stationary_doc.id

    if (stationary.html || stationary.back_html) {
      console.warn(`stationary ${stationary.id} already has html!`)
      continue
    }

    if (!stationary.paper) {
      console.warn(`stationary ${stationary.id} has no paper!`)
      continue
    }

    let updates = {
      front_type: 'custom',
      show_bleed: true,
      add_bleed: true,
      flipped: false,
      footer_text_size: 5,
      header_text_size: 8,
      footer_text_align: 'center',
      header_image_align: 'center',
    }

    if (stationary.paper.key === 'A4' || stationary.paper.key === 'A5' || stationary.paper.key === 'A5_landscape' || stationary.paper.key === 'A6_landscape') {
      let text_y = 30 // TOP MARGIN
      if (stationary.header_type !== 'Logo') {
        if (stationary.header_text && stationary.header_text.trim().length > 0) {
          text_y = 18 + 6
        } else {
          text_y = 10 + 6
        }
      }

      let footer_margin = 10

      front_html = stationary_front_html.replace(/SRC/g, stationary.background)
      front_html = front_html.replace(/WIDTH/g, stationary.paper.width + 'mm')
      front_html = front_html.replace(/HANDWRITING_HEIGHT/g, `calc(100% - ${text_y + footer_margin}mm)`)
      front_html = front_html.replace(/HANDWRITING_MARGIN_HEIGHT/g, text_y + 'mm')
      front_html = front_html.replace(/HEIGHT/g, stationary.paper.height + 'mm')
      updates.front_image = stationary.background
      updates.html = front_html
    } else {
      console.warn(`stationary ${stationary.id} has unknown paper!`, stationary.paper)
      continue
    }

    back_html = stationary_back_html.replace(/SRC/g, stationary.back_image || '')
    back_html = back_html.replace(/WIDTH/g, stationary.paper.width + 'mm')
    back_html = back_html.replace(/HEIGHT/g, stationary.paper.height + 'mm')
    updates.back_html = back_html

    updates['paper'] = constants.PAPER_SIZES[stationary.paper.key]

    proms.push(stationary_doc.ref.update(updates))
  }

  return Promise.all(proms)

  // TODO also cleanup stationary object by deleting stuff e.g. header_text
}

async function sync(nextPageToken) {
  // TODO
  let proms = []
  let docs = await db_prod.collection('stationary').limit(100).get()
  for (let doc of docs.docs) {
    proms.push(db_dev.collection('stationary').doc(doc.id).set(doc.data()))
  }

  return Promise.all(proms)
}

main()
