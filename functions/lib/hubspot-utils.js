const functions = require('firebase-functions')
const configs = functions.config()
const admin = require('firebase-admin')
const db = admin.firestore()
const constants = require('../shared/constants')
const moment = require('moment')

const Hubspot = require('hubspot')
const hubspot = new Hubspot({apiKey: configs.hubspot.api_key})

function convert_properties (obj) {
  let properties = []
  for (let key in obj) {
    properties.push({property: key, value: obj[key]})
  }
  return properties
}

function date_now(time) {
  let out = moment.utc(moment(time).format('YYYY-MM-DD'), 'YYYY-MM-DD').unix()

  return out + '000'
}

async function update_contact (user_profile, email, properties) {
  try {
    if (configs.hubspot.enabled && configs.hubspot.enabled !== 'false') {
      let contact
      try {
        contact = await hubspot.contacts.getByEmail(email)
      } catch (err) { // Create a contact
        user_properties = [{'property': 'email', 'value': email}]

        if (user_profile.first_name) user_properties.push({'property': 'firstname', 'value': user_profile.first_name})
        if (user_profile.last_name) user_properties.push({'property': 'lastname', 'value': user_profile.last_name})
        if (user_profile.phone) user_properties.push({'property': 'phone', 'value': user_profile.phone})
        user_properties.push({'property': 'account_created', 'value': date_now()})
        if (user_profile.onboarding_complete !== undefined) user_properties.push({'property': 'account_onboarding_complete', 'value': user_profile.onboarding_complete})

        contact = await hubspot.contacts.create({properties: user_properties})
      }

      return hubspot.contacts.update(contact.vid, {properties: convert_properties(properties)}).then((() => {
        return contact
      }).bind(null, contact))
    }
  } catch (err) {
    console.error('Hubspot error', err)
  }
}

async function update_deal_for_invoice (user_profile, invoice) {
  if (!configs.hubspot.enabled || configs.hubspot.enabled === 'false') {
    console.log(`Hubspot disabled, nothing to update`)
    return
  }

  if (user_profile.admin === true) {
    console.log(`User is admin, no hubspot to update`)
    return
  }

  const HUBSPOT_STAGE_NAMES = {
    'draft': 1428884,
    'charged': 1427707
  }

  let dealstage = HUBSPOT_STAGE_NAMES.draft
  if (invoice.status === constants.INVOICE_STATUS.open || invoice.status === constants.INVOICE_STATUS.paid) {
    dealstage = HUBSPOT_STAGE_NAMES.charged
  }

  let amount_gbp = (invoice.total_gbp || 0) / 100
  let credit_subscription_gbp = (invoice.credit_subscription_gbp || 0) / 100

  let billing_to = invoice.billing_to
  if (billing_to.toDate) {
    billing_to = billing_to.toDate()
  }

  let billing_from = invoice.billing_from
  if (billing_from.toDate) {
    billing_from = billing_from.toDate()
  }

  if (!invoice.hubspot_deal_id && amount_gbp === 0 && credit_subscription_gbp === 0) {
    console.log('No deal to update and no amount, skipping')
    return
  }

  try {
    // Update an existing deal
    if (invoice.hubspot_deal_id) {
      let contact = await hubspot.contacts.getByEmail(user_profile.email)
      let deal_data = {
        properties: [
          {name: 'dealstage', value: dealstage},
          {name: 'closedate', value: date_now(billing_to)},
          {name: 'amount', value: `${amount_gbp}`},
          {name: 'recurring_revenue_amount', value: `${credit_subscription_gbp}`},
          {name: 'stripe', value: `${invoice.stripe_admin_invoice_url || ''}`}
        ]
      }
      console.log('Deal data', deal_data)
      let deal = await hubspot.deals.updateById(invoice.hubspot_deal_id, deal_data)
    } else {
      let from = moment(billing_from).format('DD/MM/YY')
      let to = moment(billing_to).format('DD/MM/YY')

      // Create a new deal
      let contact = await hubspot.contacts.getByEmail(user_profile.email)
      let deal_data = {
        associations: {associatedVids: [contact.vid]},
        properties: [
          {name: 'dealname', value: `${user_profile.full_name} invoice ${from}-${to}`},
          {name: 'dealstage', value: dealstage},
          {name: 'pipeline', value: '1427701'}, // Platform pipeline
          {name: 'closedate', value: date_now(billing_to)},
          {name: 'amount', value: `${amount_gbp}`},
          {name: 'recurring_revenue_amount', value: `${credit_subscription_gbp}`},
          {name: 'stripe', value: `${invoice.stripe_admin_invoice_url || ''}`}
        ]
      }
      console.log('Deal data', deal_data)
      let deal = await hubspot.deals.create(deal_data)

      await db.collection('invoices').doc(invoice.id).update({hubspot_deal_id: deal.dealId})
    }
  } catch (err) {
    console.error('Hubspot error', err)
    console.error(JSON.stringify(err))
  }
}

module.exports = {
  update_contact,
  date_now,
  update_deal_for_invoice
}
