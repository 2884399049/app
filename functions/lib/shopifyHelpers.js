const functions = require('firebase-functions')
const crypto = require('crypto')
const request = require('node-fetch')
const querystring = require('querystring')
const configs = functions.config()
const {
  api_key: apiKey, api_secret: apiSecret
} = configs.shopify

module.exports = {
  verifyHmac (data, hmac) {
    if (!hmac) {
      return false
    } else if (!data || typeof data !== 'object') {
      return false
    }
    const calculatedSignature = crypto.createHmac('sha256', apiSecret)
      .update(Buffer.from(data), 'utf8')
      .digest('base64')
    return calculatedSignature === hmac
  },

  verifyOAuth (query = {}) {
    if (!query.hmac) {
      return false
    }
    const hmac = query.hmac
    // delete query.hmac;
    // const sortedQuery = Object.keys(query).map(key => `${key}=${Array(query[key]).join(',')}`).sort().join('&');
    // const calculatedSignature = crypto.createHmac('sha256', apiSecret).update(sortedQuery).digest('hex');
    //
    // return calculatedSignature === hmac;

    const map = Object.assign({}, query)
    delete map['signature']
    delete map['hmac']
    const message = querystring.stringify(map)
    const providedHmac = Buffer.from(hmac, 'utf-8')
    const generatedHash = Buffer.from(
      crypto
        .createHmac('sha256', apiSecret)
        .update(message)
        .digest('hex'),
      'utf-8'
    )

    return crypto.timingSafeEqual(generatedHash, providedHmac)
  },

  obtainAccessToken (shop, code) {
    // Exchange temporary code for a permanent access token
    const accessTokenRequestUrl = 'https://' + shop + '/admin/oauth/access_token'
    const accessTokenPayload = {
      client_id: apiKey,
      client_secret: apiSecret,
      code
    }
    const params = {
      method: 'POST',
      body: JSON.stringify(accessTokenPayload),
      headers: {'Content-Type': 'application/json'}
    }

    return request(accessTokenRequestUrl, params)
      .then((res) => {
        if (res.ok) { // res.status >= 200 && res.status < 300
          return res
        } else {
          throw Error(res.statusText)
        }
      })
      .then(res => res.json())
  },

  customerToRecipient (data, addressPropName) {
    const {
      first_name, last_name,
      [addressPropName]: {
        first_name: addressFirstName, last_name: addressLastName, zip, city, country, company,
        address1, address2, address3, department, province
      } = {}
    } = data

    const recipient = {
      'postal_code': zip || null,
      city: city || null,
      'first name': (addressFirstName || first_name) || null,
      'last name': (addressLastName || last_name) || null,
      country: country || null,
      company: company || '',
      'region': province || '',
      department: department || '',
      'address line 1': address1 || null,
      'address line 2': address2 || '',
      'address line 3': address3 || ''
    }

    return recipient
  }

}
