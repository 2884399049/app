const functions = require('firebase-functions')
const admin = require('firebase-admin')
const utils = require('./utils')
const moment = require('moment')
const Hubspot = require('./hubspot-utils')
const constants = require('../shared/constants')
const pricing = require('../shared/pricing')
const configs = functions.config()
const db = admin.firestore()

const stripe = require('stripe')(configs.stripe.secretkey)

/**
 * add_usage_record - Adds a usage record for a user
 *
 * @param {User} user
 * @param {Goods} goods
 * @param {Integer} recipients_count
 */
async function add_usage_record (user, goods, recipients_count, dont_trigger_aggregation, source) {
  if (!user.billing_period_start_date) {
    await start_new_billing_period(user)
  }

  return db.collection('usage').doc().set({
    uid: user.id,
    recipients_count: recipients_count,
    billing_period_start_date: user.billing_period_start_date,
    goods: goods,
    created: new Date(),
    invoice_doc: false,
    dont_trigger_aggregation: dont_trigger_aggregation || false,
    admin: user.admin ? true : false,
    source: source || constants.RECIPIENT_SOURCE.unknown
  })
}

/**
 * aggregate_usage_records - Aggregates a users usage this billing period
 * Goes through their usage this month and works out ??
 * @param {object} user - User object
 * @return {Object} The goods and usage uds
 */
async function aggregate_usage_records (user) {
  let usage_docs = await db.collection('usage').where('uid', '==', user.id).where('invoice_doc', '==', false).get()
  let goods = {}
  let recipients_count = 0
  let usage = []
  for (let usage_doc of usage_docs.docs) {
    let _usage = usage_doc.data()
    _usage['id'] = usage_doc.id
    usage.push(_usage)
    recipients_count += parseInt(usage_doc.data().recipients_count || 0)
    for (let good in usage_doc.data().goods) {
      if (!goods[good]) {
        goods[good] = 0
      }
      goods[good] += parseInt(usage_doc.data().goods[good])
    }
  }

  return {goods, recipients_count, usage}
}

/**
 * Adds new credits to user if they have a subscription
 * @param {User} user - User object
 */
async function start_new_billing_period (user) {
  let billing_period_start_date = new Date()
  let billing_period_end_date = moment().add(constants.BILLING_PERIOD_DAYS, 'days').toDate()

  if (!user.currency_symbol) {
    console.error(`No currency symbol for user ${user.id}`)
    return
  }

  let invoice = {
    status: constants.INVOICE_STATUS.draft,
    created: new Date(),
    billing_from: billing_period_start_date,
    billing_to: billing_period_end_date,
    uid: user.id,
    amount: 0,
    amount_gbp: 0,
    recipients_count: 0,
    goods: {},
    currency_symbol: user.currency_symbol,
    admin: user.admin || false
  }

  await db.collection('invoices').doc().set(invoice)

  user.billing_period_start_date = billing_period_start_date

  return db.collection('users').doc(user.id).update({
    billing_period_start_date: billing_period_start_date,
    billing_period_end_date: billing_period_end_date,
    billing_period_recipients_count: 0
  })
}

/**
 * build_price_table - Build price table with user overrides
 *
 * @param  {User} user
 * @return {PriceTable}
 */
async function build_price_table (user) {
  let price_table = {}
  let products_docs = await db.collection('products').get()
  for (let products_doc of products_docs.docs) {
    price_table[products_doc.id] = products_doc.data()
  }

  let user_price_table_docs = await db.collection('users').doc(user.id).collection('products').get()

  let user_price_table = {}
  for (let user_price of user_price_table_docs.docs) {
    user_price_table[user_price.id] = user_price.data()
  }

  for (let price_key in price_table) {
    if (user_price_table[price_key]) {
      price_table[price_key] = user_price_table[price_key]
    }
  }

  return price_table
}

/**
 * find_draft_invoice - Finds a draft invoice for a user
 * @param {User} user
 * @returns {Invoice|undefined} the invoice object if found otherwise undefined
 */
async function find_draft_invoice (user) {
  let invoice_docs = await db.collection('invoices').where('uid', '==', user.id).where('status', '==', constants.INVOICE_STATUS.draft).get()

  if (invoice_docs.docs.length > 1) {
    console.error(`Multiple draft invoices found for user ${user.id}`)
    return
  }

  let invoice
  for (let invoice_doc of invoice_docs.docs) {
    invoice = invoice_doc.data()
    invoice.id = invoice_doc.id
  }

  return invoice
}

/**
 * Does final aggregation, attempts to charge user, emails user the invoice (via stripe)
 * TODO NOTE race condition with update_aggregate_usage called at same time as finalise_invoice
 * @param {object} user - User object
 */
async function finalise_invoice (user, invoice) {
  let invoice_doc = db.collection('invoices').doc(invoice.id)

  console.log(`[User: ${user.id}, Invoice: ${invoice.id}] Creating invoice...`)

  if (user.credit_subscription) {
    let goods = {}
    goods[constants.GOOD.membership] = parseInt(user.credit_subscription)
    await add_usage_record(user, goods, 0, true, constants.RECIPIENT_SOURCE.system)
  }

  let {goods, usage, recipients_count} = await aggregate_usage_records(user)
  let price_table = await build_price_table(user)

  let credit = (user.credit || 0) + (goods[constants.GOOD.membership] || 0)
  console.log(`[User: ${user.id}, Invoice: ${invoice.id}] credit: ${credit}`)

  let price = pricing.caluclate_price(price_table, user, goods, 0, credit)
  console.log(`[User: ${user.id}, Invoice: ${invoice.id}] price: ${JSON.stringify(price)}`)

  let credit_used = credit - price.credit_left

  let tax_rate = pricing.caluclate_customer_tax_rate(user)
  let tax_rates = []
  if (tax_rate === constants.TAX_RATE.outside_eu_0_vat || tax_rate === constants.TAX_RATE.gb_0_vat) {
    tax_rates.push(configs.stripe.gb_no_vat_tax_rate_id)
  } else if (tax_rate === constants.TAX_RATE.gb_20_vat) {
    tax_rates.push(configs.stripe.gb_vat_tax_rate_id)
  } else {
    throw Error(`Unknown tax rate: ${tax_rate}`)
  }

  let credit_subscription = 0
  if (user.credit_subscription) {
    if (user.currency === 'usd') {
      credit_subscription = user.credit_subscription * constants.CURRENCY.usd_to_gbp
    } else if (user.currency === 'eur') {
      credit_subscription = user.credit_subscription * constants.CURRENCY.eur_to_gbp
    } else if (user.currency === 'gbp') {
      credit_subscription = user.credit_subscription
    } else {
      throw new Error(`Unknown currency: ${user.currency}`)
    }
  }

  let billing_to = moment()
  let billing_from = moment(invoice.billing_from.toDate())

  invoice.goods = goods
  invoice.recipients_count = recipients_count
  invoice.status = constants.INVOICE_STATUS.paid
  invoice.billing_to = billing_to.toDate()
  invoice.total = price.total
  invoice.total_with_tax = price.total_with_tax
  invoice.total_gbp = price.total_gbp
  invoice.credit_start = credit
  invoice.credit_used = credit_used
  invoice.credit_end = price.credit_left
  invoice.currency_symbol = user.currency_symbol
  invoice.credit_subscription_gbp = parseInt(credit_subscription)

  // TODO

  if (price.total > 0) {
    let invoice_item = await stripe.invoiceItems.create({
      customer: user.stripe_customer_id,
      amount: price.total,
      tax_rates: tax_rates,
      currency: price.currency,
      description: `Scribeless handwritten services to ${recipients_count} recipients between ${billing_from.format('DD/MM/YY')} to ${billing_to.format('DD/MM/YY')}`,
    })

    let stripe_invoice
    if (!user.billing_collection_method || user.billing_collection_method === 'charge_automatically') {
      stripe_invoice = await stripe.invoices.create({
        customer: user.stripe_customer_id,
        collection_method: 'charge_automatically',
        auto_advance: true
      })
    } else {
      stripe_invoice = await stripe.invoices.create({
        customer: user.stripe_customer_id,
        collection_method: user.billing_collection_method,
        days_until_due: constants.BILLING_INVOICE_DUE_DAYS
      })
    }

    stripe_invoice = await stripe.invoices.finalizeInvoice(stripe_invoice.id)

    invoice.status = constants.INVOICE_STATUS.open
    invoice['stripe_invoice_id'] = stripe_invoice.id
    invoice['invoice'] = stripe_invoice.invoice_pdf ? stripe_invoice.invoice_pdf : null
    invoice['stripe_total'] = stripe_invoice.total
    invoice['stripe_tax'] = stripe_invoice.tax
    invoice['stripe_amount_due'] = stripe_invoice.amount_due
    invoice['stripe_starting_balance'] = stripe_invoice.starting_balance
    invoice['stripe_ending_balance'] = stripe_invoice.ending_balance
    invoice['stripe_admin_invoice_url'] = `https://dashboard.stripe.com/invoices/${stripe_invoice.id}`

    console.log(`[User: ${user.id}] Created stripe invoice ${stripe_invoice.id}`)
  }

  console.log(`[User: ${user.id}] Setting invoice ${invoice_doc.id}`)
  await invoice_doc.update(invoice)

  console.log(`[User: ${user.id}] Updating usage documents to invoice ${invoice_doc.id}`)
  let proms = []
  for (let usage_item of usage) {
    // let price = await pricing.caluclate_price(price_table, user, usage_item.goods)
    proms.push(db.collection('usage').doc(usage_item.id).update({invoice_doc: invoice_doc})) // , price: price
  }

  // Update users credit
  proms.push(db.collection('users').doc(user.id).update({credit: price.credit_left}))

  invoice.id = invoice_doc.id
  await Hubspot.update_deal_for_invoice(user, invoice)

  return Promise.all(proms)
}

/**
 * check_billing_period - Does final aggregation, attempts to charge user
 * @param {User} user
 */
async function check_billing_period () {
  let user_docs = await db.collection('users').where('billing_period_end_date', '<=', new Date()).get()
  let proms = []
  for (let user of user_docs.docs) {
    proms.push(end_of_billing_period(user))
  }

  return Promise.all(proms)
}

/**
 * check_billing_period - Does final aggregation, attempts to charge user
 * @param {User} user
 */
async function sync_hubspot_invoices () {
  let invoice_docs = await db.collection('invoices').get()
  let proms = []
  for (let invoice_doc of invoice_docs.docs) {
    let invoice = invoice_doc.data()
    invoice.id = invoice_doc.id

    proms.push(sync_hubspot_invoice(invoice))
  }

  return Promise.all(proms)
}

/**
 * check_billing_period - Does final aggregation, attempts to charge user
 * @param {User} user
 */
async function sync_hubspot_invoice (invoice) {
  let user = await utils.get_user_profile(invoice.uid)
  return Hubspot.update_deal_for_invoice(user, invoice)
}

/**
 * end_of_billing_period - Stops current billing period and starts a new one
 * @param {User} user
 */
async function end_of_billing_period (user) {
  let invoice = await find_draft_invoice(user)

  if (!invoice) {
    console.error(`No draft invoice found for user ${user.id}`)
    return start_new_billing_period(user)
  }

  await finalise_invoice(user, invoice)
  return start_new_billing_period(user)
}


/**
 * end_of_billing_period_function - Helper function to call end billing period for specific uid
 * @param {data} Firebase functions onCall data
 * @param {context} Firebase functions onCall context
 */
async function end_of_billing_period_function (data, context) {
  let user = await utils.get_user_profile(data.uid)
  await end_of_billing_period(user)

  return {success: true}
}

/**
 * sync_update_customer - Syncs the stripe balance with scribeless
 *
 * @param {Object} customer Stripe object
 */
async function sync_update_customer (customer) {
  console.log(`Syncing customer ${customer.id}`)
  var users = await db
    .collection('users')
    .where('stripe_customer_id', '==', customer.id)
    .limit(1)
    .get()
  if (users.empty) {
    console.error(`No user found with customer.id of ${customer.id}`)
    return
  }

  let user_doc = users.docs[0]
  let user = user_doc.data()

  if (user.balance === customer.balance) {
    console.log(
      `Stripe customer is identical with user ${user_doc.id} no update`
    )
    return
  }

  return await user_doc.ref.update({ balance: customer.balance })
}

/**
 * sync_update_charge - Syncs a stripe invoice status with scribeless
 *
 * @param {Object} invoice Stripe invoice object
 */
async function sync_update_invoice (invoice) {
  console.log(`Syncing invoice ${invoice.id}`)
  let invoice_docs = await db.collection('invoices').where('stripe_invoice_id', '==', invoice.id).limit(1).get()
  for (let invoice_doc of invoice_docs.docs) {
    return db.collection('invoices').doc(invoice_doc.id).update({status: invoice.status})
  }
}

/**
 * sync_update_payment_method - Syncs a stripe payment method
 *
 * @param {Object} invoice Stripe invoice object
 */
async function sync_update_payment_method (payment_method, detach) {
  console.log(`Syncing payment_method ${payment_method.id}`)

  if (payment_method.customer) { // No customer attached
    return false
  }

  let user_docs = await db.collection('users').where('stripe_customer_id', '==', payment_method.customer).limit(1).get()
  for (let user_doc of user_docs.docs) {
    let user = user_doc.data()

    let payment_methods = {}
    if (user.payment_methods) {
      payment_methods = user.payment_methods
    }

    if (detach) {
      if (payment_methods[payment_method.id]) {
        delete payment_methods[payment_method.id]
      }
    } else {
      payment_methods[payment_method.id] = payment_method
    }

    return db.collection('users').doc(user_doc.id).update({payment_methods})
  }
}

/**
 * stripe_webhook - Syncs stripe with scribeless
 *
 * @param {type} req Description
 * @param {type} res Description
 *
 * @return {type} Description
 */
async function stripe_webhook (req, res) {
  let { data, type } = req.body

  if (!data || data.test) {
    res.send('Success')
    return null
  }

  console.log('---------------------')
  let { previous_attributes, object } = data

  console.log(data, type)

  try {
    if (type === 'customer.updated') {
      await sync_update_customer(object)
    }

    if (type === 'invoice.updated') {
      await sync_update_invoice(object)
    }

    if (type === 'payment_method.updated' || type === 'payment_method.attached') {
      await sync_update_payment_method(object)
    }

    if (type === 'payment_method.detached') {
      await sync_update_payment_method(object, true)
    }
  } catch (err) {
    console.log('webhook error: ', err)
    return res.send(200)
  }

  return res.send(200)
}

/**
 * stripe_add_card - Adds a card to a users stripe profile
 */
async function stripe_add_card (data, context) {
  if (!data || data.test) {
    console.log('Running to avoid cold boot')
    return null
  }

  let user_profile = await utils.get_user_profile(context.auth.uid)

  let error, paymentMethod
  try {
    let out = await stripe.paymentMethods.attach(
      data.payment_method_id,
      { customer: user_profile.stripe_customer_id }
    )

    error = out.error
    paymentMethod = out.paymentMethod

    await stripe.customers.update(user_profile.stripe_customer_id, {
      invoice_settings: {
        default_payment_method: data.payment_method_id
      }
    })
  } catch (err) {
    console.log(`Stripe error:`, err)
    return false
  }

  if (error) {
    console.warn(err)
    return error
  }

  if (data.tax_id) {
    try {
      let out = await stripe.customers.createTaxId(
        user_profile.stripe_customer_id,
        { type: data.tax_type, value: data.tax_id }
      )
      error = out.error
      paymentMethod = out.paymentMethod

      if (error) {
        console.log(err)
        return error
      }
    } catch (err) {
      console.log(err)
      return { error: { message: 'Invalid tax ID' } }
    }
  }

  try {
    await Hubspot.update_contact(user_profile, user_profile.email, {
      billing_method_added_date: Hubspot.date_now()
    })
  } catch (err) {
    console.error(`HubSpot Error`)
    console.error(err)
  }


  return db
    .collection('users')
    .doc(context.auth.uid)
    .update({ billing_enabled: true })
}

async function stripe_request_setupintent (data, context) {
  if (data && data.test) {
    console.log('Running to avoid cold boot')
    return null
  }

  return stripe.setupIntents.create({ usage: 'off_session' })
}

async function create_customer (email) {
  return stripe.customers.create({
    email: email
  })
}

async function stripe_add_coupon (data, context) {
  var user_profile = await utils.get_user_profile(context.auth.uid)

  try {
    return stripe.customers.update(user_profile.stripe_customer_id, {
      coupon: data.coupon
    })
  } catch (err) {
    console.log(`Stripe error:`, err)
    return false
  }
}

module.exports = {
  finalise_invoice,
  caluclate_price: pricing.caluclate_price,
  price_good: pricing.price_good,
  build_price_table,
  goods_to_stamps: pricing.goods_to_stamps,
  aggregate_usage_records,
  check_billing_period,
  stripe_webhook,
  stripe_add_card,
  stripe_request_setupintent,
  create_customer,
  stripe_add_coupon,
  campaign_to_goods: pricing.campaign_to_goods,
  add_usage_record,
  start_new_billing_period,
  end_of_billing_period_function,
  printer_cost: pricing.printer_cost,
  find_draft_invoice,
  campaign_recipient_batch_good: pricing.campaign_recipient_batch_good,
  sync_hubspot_invoices: sync_hubspot_invoices
}
