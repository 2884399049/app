var _ = require('underscore')
const country_data = require('country-data')

var ILLEGAL_CHARACTERS = {
  '’': "'",
  '‘': "'",
  '،': ',',
  '‐': '-',
  '‒': '-',
  '–': '-',
  '—': '-',
  '―': '-',
  '一': '-',
  '-': '-',
  ' ': '-',
  '“': '"',
  '”': '"',
  '/': '/',
  '⧸': '/',
  '⁄': '/',
  '᠎': '',
  ' ': ' ',
  ' ': ' ',
  ' ': ' ',
  ' ': ' ',
  ' ': ' ',
  ' ': ' ',
  ' ': ' ',
  ' ': ' ',
  ' ': ' ',
  ' ': ' ',
  ' ': ' ',
  ' ': ' ',
  ' ': ' ',
  '': '',
  ' ': ' ',
  ' ': ' ',
  '　': ' ',
  '﻿': '',
  '\xc2\xa0': ' ',
  '\xe2\x80\x88': ' ',
  '\xe2\x80\x8B': ' ',
  '…': '...',
  '′': "'",
  '″': "''",
  '‴': "'''"
}

function remove_illigal_characters (text) {
  var clean_text = ''

  if (!text) {
    return clean_text
  }

  for (var i in text) {
    if (ILLEGAL_CHARACTERS[text[i]] !== undefined) {
      clean_text = clean_text + ILLEGAL_CHARACTERS[text[i]]
    } else {
      clean_text = clean_text + text[i]
    }
  }

  // Remove multiple spaces, leading spaces
  // clean_text = clean_text.replace(/ +(?= )/g,'');
  clean_text = clean_text.replace(/ ,/g, ',')
  clean_text = clean_text.replace(/ \./g, '.')
  clean_text = clean_text.replace(/,+/g, ',')

  return clean_text
}

function check_bad_text (text) {
  var new_text = remove_illigal_characters(text)
  var search_text = new_text.replace(/{{{.+}}}/g, '')
  if (!/^[\x00-\x7F]*$/.test(search_text) || /[\{\}\[\]\^\\_<>\`]/.test(search_text)) {
    var bad_chars = _.uniq(search_text.replace(/[\{\}a-zA-Z0-9!\?<>#_@$£€¢§¶%&*()\-+=.,\,;:'"\ \/ \n\r\t\\n\\r\\t|`ßÂâÃãÁáÀàÅåÄäÆæÇçÉéÈèËëÊêÍíÌìÎîÏïÑñÔôÖöÒòÓóŒœØøÕõŠšÚúÙùÜüÛûŸÿŽž]/gm, '').trim().split(''))
    text = new_text

    if (bad_chars && bad_chars.length) {
      return [false, new_text, `Character not supported. Please remove characters: ${bad_chars}.`]
    }
  }

  text = new_text
  return [true, new_text, undefined]
}

function to_title_case (str) {
  return str.replace(
    /\w\S*/g,
    (txt) => {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
    }
  )
}

function template_text (template, variables, include_country_name, is_return_address) {
  if (variables === undefined) {
    variables = {
      'title': 'Mr',
      'first name': 'John',
      'last name': 'Doe',
      'department': 'Marketing',
      'company': 'Company',
      'address line 1': '1 Alfred Place',
      'address line 2': 'Fitzrovia',
      'address line 3': 'Address line 3',
      'city': 'London',
      'region': 'London',
      'postal_code': 'TW11 9PB',
      'country': 'United Kingdom',
      'custom 1': 'Custom 1',
      'custom 2': 'Custom 2',
      'custom 3': 'Custom 3',
      'custom 4': 'Custom 4',
      'custom 5': 'Custom 5',
      'custom 6': 'Custom 6'
    }
  }

  console.log(variables)

  var address = ''
  if (is_return_address) address += `Return Address\n`
  if (variables['title']) address += `${variables['title']} `
  if (variables['first name']) address += `${to_title_case(variables['first name']).trim()} `
  if (variables['last name']) address += `${to_title_case(variables['last name']).trim()}\n`
  if (variables['department']) address += `${variables['department']}\n`
  if (variables['company']) address += `${variables['company']}\n`
  if (variables['address line 1']) address += `${to_title_case(variables['address line 1']).trim()}\n`
  if (variables['address line 2']) address += `${to_title_case(variables['address line 2']).trim()}\n`
  if (variables['address line 3']) address += `${to_title_case(variables['address line 3']).trim()}\n`

  let postal_code = variables['zip/postal code'] || variables['postal_code']
  let region = variables['state/region'] || variables['region']

  if (variables['country'] && ['US'].indexOf(variables['country']) > -1) {
    if (variables['city'] && region) {
      address += `${to_title_case(variables['city']).trim()} ${region}`
      if (postal_code) address += ` ${postal_code}\n`
    } else if (variables['city']) {
      address += `${to_title_case(variables['city']).trim()}`
      if (postal_code) address += ` ${postal_code}\n`
    } else if (region) {
      address += `${region}`
      if (postal_code) address += ` ${postal_code}\n`
    } else {
      if (postal_code) address += `${postal_code}\n`
    }
  } else {
    if (variables['city'] && region) {
      address += `${to_title_case(variables['city']).trim()}, ${region}\n`
    } else if (variables['city']) {
      address += `${to_title_case(variables['city']).trim()}\n`
    } else if (region) {
      address += `${region}\n`
    }

    if (postal_code) address += `${postal_code}\n`
  }

  if (include_country_name) {
    if (variables['country'] && country_data.countries[variables['country'].toUpperCase()]) {
      let country_info = country_data.countries[variables['country'].toUpperCase()]
      address += `${country_info.name}\n`
    } else if (variables['country']) {
      address += `${variables['country']}\n`
    }
  }

  variables['address'] = address.trim()

  for (var key in variables) {
    if (variables[key]) {
      template = template.replace(new RegExp(`{{{${key}}}}`, 'g'), variables[key])
    }
  }

  template = template.replace(/{{{.+}}}/g, '') // Clean any missing

  return template
}

module.exports = {
  check_bad_text,
  template_text
}
