const functions = require('firebase-functions')
const admin = require('firebase-admin')
const utils = require('./utils')
const pricing = require('./pricing')
const slack = require('./slack')
const archiver = require('archiver')
const download = require('download')
const moment = require('moment')
const _ = require('underscore')
const printer_utils = require('../shared/printer')

const constants = require('../shared/constants')

const configs = functions.config()
const db = admin.firestore()

const bucket = admin.storage().bucket(configs.firestore.storagebucket)

async function get_printer(printer_ref) {
  let printer_doc = await printer_ref.get()
  let printer = printer_doc.data()
  printer['id'] = printer_doc.id
  return printer
}

async function ship_batch (change) {
  let batch = change.after.data()
  batch['id'] = change.after.id

  if (
    change.before.data().status === batch.status ||
    batch.status !== constants.BATCH_STATUS.shipped
  ) {
    return false
  }

  console.log(`Shipping batch ${change.after.id}`)

  let _recipient_docs = await db
    .collection('batches')
    .doc(batch.id)
    .collection('recipients')
    .get()

  let promises = []

  let printer = await get_printer(batch.printer_doc)
  let result = await batch_printer_check(batch)

  promises.push(change.after.ref.update({cost: result.cost, warnings: result.warnings, shipped_date: new Date()}))

  _recipient_docs.forEach(_recipient_doc => {
    let recipient_doc = _recipient_doc.data().recipient_doc
    promises.push(recipient_doc.update({status: constants.RECIPIENT_STATUS.shipped, updated: new Date(), batch: {id: batch.id, timestamp: new Date()}, printer_doc: batch.printer_doc}))
  })

  await slack.post(`📦 Shipped ${_recipient_docs.docs.length} recipients to ${printer.name} (£${result.cost / 100})`, configs.slack.channels.operations)

  // Send emails
  if (batch.zip && printer.email) {
    console.log(`Notification email to ${printer.email}`)
    let message = `New order from Scribeless.<br><br>Please download <a href="${batch.zip}">ZIP file</a>. Please reply to this email with any problems.`
    if (batch.notes) {
      message += `<br><br><strong>Notes:</strong><br>${batch.notes.replace(
        /\n/g,
        '<br>'
      )}`
    }

    if (configs.environment && (configs.environment.name === 'production')) {
      promises.push(
        utils.send_email(
          printer.email,
          `Scribeless: Printing order`,
          message,
          'printing@scribeless.co',
          'printing@scribeless.co'
        )
      )
    } else {
      console.log(`Would have sent email to ${printer.email}`)
    }
  } else {
    await slack.post(`⚠️ Batch shipped without zip or email to printer ${printer.name}`, configs.slack.channels.operations)
  }

  return Promise.all(promises)
}

async function zip_batches_function (change, context) {
  var batch = change.after.data()

  if (batch.zip === false) {
    console.log(`Zipping batch ${change.after.id}`)

    db.collection('batches')
      .doc(change.after.id)
      .update({ zip: null })

    var _recipient_docs = await db
      .collection('batches')
      .doc(change.after.id)
      .collection('recipients')
      .get()

    let batch_recipients = {}

    var promises = []
    _recipient_docs.forEach(_recipient_doc => {
      batch_recipients[_recipient_doc.data().recipient_doc.id] = _recipient_doc.data()
      promises.push(_recipient_doc.data().recipient_doc.get())
    })

    // TODO get campaign and get goods?
    // TODO domestic international

    console.log(`Found ${promises.length} recipients`)
    var recipient_docs = await Promise.all(promises)

    var urls = {}
    for (let recipient_doc of recipient_docs) {
      var recipient = recipient_doc.data()
      let batch_recipient = batch_recipients[recipient_doc.id]

      if (recipient.status !== constants.RECIPIENT_STATUS.ready_to_print) {
        continue
      }

      let batch_good = batch_recipient.batch_good ? batch_recipient.batch_good : 'pdfs'

      if (recipient.letter_url) {
        urls[recipient.letter_url] = {url: recipient.letter_url, folder: batch_good}
      }

      if (recipient.insert_urls) {
        for (let insert_url of recipient.insert_urls) {
          urls[insert_url] = {url: insert_url, folder: batch_good}
        }
      }

      if (recipient.envelope_url) {
        urls[recipient.envelope_url] = {url: recipient.envelope_url, folder: batch_good}
      }
    }

    var zip_url = await zip_list_of_files(change.after.id, urls)

    if (!zip_url) {
      console.warn(`Failed to ZIP batch ${change.after.id}`)
      await slack.post(`⚠️ Failed to ZIP batch ${change.after.id}`, configs.slack.channels.operations)
      return false
    }

    return db
      .collection('batches')
      .doc(change.after.id)
      .update({ zip: zip_url })
    }

    return ship_batch(change)
}

async function zip_list_of_files (zip_id, _urls) {
  console.log(`Zipping to ${zip_id} urls:`)
  console.log(_urls)

  let urls = Object.values(_urls)

  if (urls.length === 0) {
    return null
  }

  if (urls.length === 1) {
    return urls[0]
  }

  const metadata = {
    contentType: 'application/zip'
  }
  const archive = archiver('zip', {
    // Sets the compression level from 1 - fast to 9 - slow
    // zlib: { level: 1 }
    store: true
  })

  const bucketWriteStream = bucket.file(`zips/${zip_id}.zip`).createWriteStream({metadata})
  archive.pipe(bucketWriteStream)

  bucketWriteStream.on('error', (err) => {
    console.log(err)
  })

  archive.on('error', (err) => {
    console.log(err)
  })

  // Number of files to download in parallel
  // 4 or 5 is ok. 10 - out of memory
  const numOfFilesInParallel = 5
  let index = 0

  try {
    let fileUrl
    do {
      // get file's urls for downloading in parallel
      fileUrl = urls.slice(index, index + numOfFilesInParallel)
      index += numOfFilesInParallel

      // start downloading of 5 files in parallel
      const getFiles = []
      fileUrl.forEach(item => {
        getFiles.push(addToArchive(archive, item, download(item.url)))
      })

      // wait untill all 5 files will be downloaded
      await Promise.all(getFiles)
    } while (fileUrl.length)
  } catch (error) {
    console.log(error)
    return false
  }

  console.log('Archive & Upload...')
  archive.finalize()

  await utils.end_stream(bucketWriteStream)

  const url = `https://storage.googleapis.com/${configs.firestore.storagebucket}/zips/${zip_id}.zip?t=${Date.now()}`
  console.log(url)
  return url
}

async function addToArchive (archive, item, getData) {
  archive.append(await getData, { name: `/${item.folder}/${extractFileName(item.url)}` })
}

function extractFileName (fileUrl) {
  let fileName = fileUrl.substr(fileUrl.lastIndexOf('/') + 1)
  if (fileName.indexOf('?') !== -1) {
    fileName = fileName.substr(0, fileName.indexOf('?'))
  }
  return fileName
}

async function batch_printer_check_function (data, context) {
  let batch_doc = db.collection('batches').doc(data.batch_id)

  await batch_doc.update({cost: null, warnings: []})

  let batch_snap = await batch_doc.get()
  let batch = batch_snap.data()
  batch['id'] = batch_snap.id

  let result = await batch_printer_check(batch)
  await batch_doc.update({
    cost: result.cost,
    warnings: result.warnings
  })

  return {success: true}
}

async function batch_printer_check (batch) {
  let printer_doc = await batch.printer_doc.get()
  let printer = printer_doc.data()
  printer['id'] = printer_doc.id

  let warnings = []
  let campaigns = {}

  let _recipient_docs = await db.collection('batches').doc(batch.id).collection('recipients').get()
  let promises = []
  _recipient_docs.forEach(_recipient_doc => {
    let campaign_id = _recipient_doc.data().campaign
    if (!campaigns[campaign_id]) {
      campaigns[campaign_id] = {recipients: []}
    }
    promises.push(_recipient_doc.data().recipient_doc.get())
  })

  let recipient_docs = await Promise.all(promises)
  recipient_docs.forEach(recipient_doc => {
    let recipient = recipient_doc.data()

    recipient.id = recipient_doc.id
    let campaign_id = recipient_doc.ref.path.split('/')[1]

    if (recipient.country && printer.serviceable_countries.indexOf(recipient.country) === -1) {
      warnings.push(`Unserviceable country found: ${recipient.country}`)
    } else if (!recipient.country) {
      warnings.push(`No country found for campaign: ${campaign_id}`)
    }

    campaigns[campaign_id].recipients.push(recipient)
  })

  let batch_goods = {}
  for (let campaign_id in campaigns) {
    let campaign_doc = await db.collection('campaigns').doc(campaign_id).get()
    let campaign = campaign_doc.data()

    console.log('checking stationary', campaign.stationary_required, campaign.stationary)

    let stationary
    if (campaign.stationary_required && campaign.stationary) {
      let stationary_doc = await db.collection('stationary').doc(campaign.stationary).get()
      stationary = stationary_doc.data()
      stationary.id = stationary_doc.id
    }

    let campaign_goods = pricing.campaign_to_goods(campaign, campaigns[campaign_id].recipients, stationary)
    // TODO add estimate delivery time to recipients

    for (let good in campaign_goods) {
      if (!batch_goods[good]) {
        batch_goods[good] = 0
      }
      batch_goods[good] += campaign_goods[good]
    }
  }

  let printer_cost_out = pricing.printer_cost(printer, batch_goods)

  if (printer_cost_out.code_error) {
    console.error(`Unknown printer cost, code error: ${printer_cost_out.code_error}`)
    warnings.push(`Unknown printer cost, code error: ${printer_cost_out.code_error}`)
  }

  let cost = printer_cost_out.code_output || 0

  // Check serviceable goods
  for (let good in batch_goods) {
    if (printer.serviceable_goods.indexOf(good) === -1) {
      warnings.push(`Unserviceable good found: ${good}`)
    }
  }

  warnings = _.uniq(warnings)

  return {warnings, cost}
}

async function send_to_batches() {
  // Order by priority

  let printer_docs = await db.collection('printers').get()
  let printers = {}
  let fallback_printer

  for (let printer_doc of printer_docs.docs) {
    let printer = printer_doc.data()
    printer.id = printer_doc.id
    printers[printer.id] = printer

    if (printer.is_fallback) {
      fallback_printer = printer
    }
  }

  let printer_updates = {}
  let campaign_docs = await db.collection('campaigns').where('status', '==', constants.CAMPAIGN_STATUS.ready_to_ship).where('product', '==', constants.PRODUCT.full_service).get()
  for (let campaign_doc of campaign_docs.docs) {
    let campaign = campaign_doc.data()
    campaign.id = campaign_doc.id

    let stationary
    if (campaign.stationary_required && campaign.stationary) {
      let stationary_doc = await db.collection('stationary').doc(campaign.stationary).get()
      stationary = stationary_doc.data()
      stationary.id = stationary_doc.id
    }

    let recipient_docs = await db.collection('campaigns').doc(campaign.id).collection('recipients').where('status', '==', constants.RECIPIENT_STATUS.ready_to_print).get()

    let printer_updates_for_campaign = {}

    if (campaign.print_override_location === 'None') {
      console.log(`Skipping campaign ${campaign.id} as override location is None`)
      continue
    }

    for (let recipient_doc of recipient_docs.docs) {
      let recipient = recipient_doc.data()
      recipient.id = recipient_doc.id

      let country = recipient.country
      let selected_printer

      if (campaign.print_override_location) {
        selected_printer = printers[campaign.print_override_location]
      } else {
        for (let printer_id in printers) { // Order by priority?
          let printer = printers[printer_id]
          if (printer.serviceable_countries.indexOf(country) > -1) {
            selected_printer = printer
            break
          }
        }
      }

      if (!selected_printer) {
        if (fallback_printer) {
          selected_printer = fallback_printer
        } else {
          await slack.post(`⚠️ Error No printer choosen for recipient ${recipient.id} (${recipient['first name'] - recipient['last name'] - recipient['address line 1']})of campaign https://app.scribeless.co/campaign/${campaign.id}`, configs.slack.channels.operations)
          throw Error(`Error No printer choosen for recipient ${recipient.id} of campaign ${campaign.id}`)
        }
      }

      let recipient_needs_rerender = false
      let return_address = recipient.return_address || null
      if (campaign.is_sample) {
        if (selected_printer.return_address) {
          return_address = selected_printer.return_address
          if (!campaign.delivery.include_return_address) {
            await recipient_doc.ref.update({ return_address: return_address })
            campaign_doc.ref.update({ 'delivery.include_return_address': true })
          }
          if (JSON.stringify(recipient.return_address) !== JSON.stringify(return_address)) {
            console.info("Updating return address for " + recipient.id + " to " + selected_printer.name + "'s return address")
            recipient_needs_rerender = true
          }
        }
        else {
          console.error("Printer " + selected_printer.name + " has no return address and is sending out sample: " + campaign.id)
        }
      }

      let include_country_name = campaign.delivery.include_country_name ? true : selected_printer.location !== recipient.country
      if (include_country_name !== recipient.include_country_name) {
        recipient_needs_rerender = true
        console.info("Updating include_country_name to " + include_country_name + " for " + recipient.id)
      }

      if (recipient_needs_rerender) {
        await recipient_doc.ref.update({ return_address: return_address, include_country_name: include_country_name, status: constants.RECIPIENT_STATUS.pending })
      }

      if (!printer_updates_for_campaign[selected_printer.id]) {
        printer_updates_for_campaign[selected_printer.id] = {all_recipients: [], due_soon: [], notes: []}
      }

      printer_updates_for_campaign[selected_printer.id].all_recipients.push(recipient)
    }

    for (let printer_id in printer_updates_for_campaign) {
      let printer = printers[printer_id]
      let result = printer_utils.calculate_lead_time(printer, campaign, printer_updates_for_campaign[printer_id].all_recipients)
      if (result.code_error || !result.code_output || result.code_output.length === 0) {
        await slack.post(`⚠️ Error calculating lead time, printer: ${printer.name}, campaign: ${campaign.id}. Error: ${result.code_error}`, configs.slack.channels.operations)
        throw Error(`Error calculating lead time, printer: ${printer.id}, campaign: ${campaign.id}. Error: ${result.code_error}`)
      }

      for (let i in printer_updates_for_campaign[printer_id].all_recipients) {
        let days = result.code_output[i]
        let estimated_delivery_date = moment().add(days, 'days')
        let recipient = printer_updates_for_campaign[printer_id].all_recipients[i]

        if (campaign.delay_delivery_days || !campaign.delivery || !campaign.delivery.due_date) {
          new_due_date = moment(recipient.created.toDate()).add(campaign.delay_delivery_days || 0, 'days')
        } else {
          new_due_date = moment(campaign.delivery.due_date.toDate())
        }


        // days set by the user here
        if (campaign.delivery && campaign.delivery.due_date && (new_due_date > estimated_delivery_date)) {
          console.log(`Skipping recipient ${recipient.id} for campaign ${campaign.id} as delivery date is > ${days} days away`)
          continue
        }


        if (recipient.testing) {
          console.log(`Skipping recipient ${recipient.id} for campaign ${campaign.id} as testing`)
          continue
        }

        recipient.estimated_delivery_date = estimated_delivery_date.toDate()
        recipient.recipient_doc = db.collection('campaigns').doc(campaign.id).collection('recipients').doc(recipient.id)
        recipient.campaign_id = campaign.id
        recipient.batch_good = pricing.campaign_recipient_batch_good(campaign, recipient, stationary)

        printer_updates_for_campaign[printer_id].due_soon.push(recipient)
      }

      if (printer_updates_for_campaign[printer_id].due_soon.length === 0) {
        continue
      }

      if (!printer_updates[printer_id]) {
        printer_updates[printer_id] = {campaigns: [], recipients: [], printer: printer, notes: []}
      }

      if (campaign.delivery && campaign.delivery.sender === constants.SENDER.ship_to_self) {
        let address = `Ship to self order for "${campaign.title}", please deliver to:
${campaign.delivery.user_address.first_name} ${campaign.delivery.user_address.last_name}
${campaign.delivery.user_address.job_title} - ${campaign.delivery.user_address.organisation}
${campaign.delivery.user_address.address_line_1}
${campaign.delivery.user_address.address_line_2}
${campaign.delivery.user_address.city}
${campaign.delivery.user_address.postcode}
${campaign.delivery.user_address.region}
${campaign.delivery.user_address.country}\n\n`
        printer_updates_for_campaign[printer_id].notes.push(address)
      }

      printer_updates[printer_id].campaigns.push(campaign)
      printer_updates[printer_id].recipients = printer_updates[printer_id].recipients.concat(printer_updates_for_campaign[printer_id].due_soon)
      printer_updates[printer_id].notes = printer_updates[printer_id].notes.concat(printer_updates_for_campaign[printer_id].notes)
    }
  }

  for (let printer_id in printer_updates) {
    let updates = printer_updates[printer_id]
    let printer_doc = db.collection('printers').doc(printer_id)
    let batch_doc
    let batch_query = await db.collection('batches').where('status', '==', constants.BATCH_STATUS.pending).where('printer_doc', '==', printer_doc).get()
    batch_query.forEach((_batch_doc) => {
      console.log(`Found batch_doc ${_batch_doc.id}`)
      batch_doc = _batch_doc.ref
    })

    if (!batch_doc) {
      batch_doc = db.collection('batches').doc()
    }

    let promises = []
    // Remove old campaigns
    let batch_campaigns = await batch_doc.collection('campaigns').get()
    batch_campaigns.forEach((batch_campaign_doc) => {
      promises.push(batch_campaign_doc.ref.delete())
    })

    // Remove old recipients
    let batch_recipients = await batch_doc.collection('recipients').get()
    batch_recipients.forEach((batch_recipient_doc) => {
      promises.push(batch_recipient_doc.ref.delete())
    })

    await Promise.all(promises)

    if (updates.recipients.length === 0) { // No updates required
      continue
    }

    let batch_good_counts = {}
    for (let recipient of updates.recipients) {

      if (!batch_good_counts[recipient.batch_good]) {
        batch_good_counts[recipient.batch_good] = 0
      }
      batch_good_counts[recipient.batch_good]++

      promises.push(batch_doc.collection('recipients').doc(recipient.campaign_id + recipient.id).set({
        recipient_doc: recipient.recipient_doc,
        campaign: recipient.campaign_id,
        batch_good: recipient.batch_good
      }))
      promises.push(db.collection('campaigns').doc(recipient.campaign_id).collection('recipients').doc(recipient.id).update({estimated_delivery_date: recipient.estimated_delivery_date}))
    }

    for (let campaign of updates.campaigns) {
      promises.push(batch_doc.collection('campaigns').doc(campaign.id).set({campaign_doc: db.collection('campaigns').doc(campaign.id)}))
    }

    await Promise.all(promises)

    let str = ''
    for (let key in batch_good_counts) {
      str += `- ${batch_good_counts[key]} ${key}\n`
    }

    updates.notes.push(`Count of items: \n${str}`)
    await batch_doc.set({printer_doc: printer_doc, zip: false, status: constants.BATCH_STATUS.pending, created: new Date(), notes: updates.notes.join('\n'), count: updates.recipients.length})


    await batch_printer_check_function({batch_id: batch_doc.id})
  }
}

module.exports = {
  send_to_batches,
  zip_list_of_files,
  zip_batches_function,
  batch_printer_check,
  batch_printer_check_function,
  ship_batch,
}
