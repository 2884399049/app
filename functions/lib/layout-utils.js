const functions = require('firebase-functions')
const admin = require('firebase-admin')

const PDFDocument = require('pdfkit')
const utils = require('./utils')
const constants = require('../shared/constants')
const download = require('image-downloader')
const download_js = require('download')
const gs = require('gs')
const sharp = require('sharp')
const fs = require('fs')
const puppeteer = require('puppeteer');
const jsdom = require("jsdom")
const { JSDOM } = jsdom
const HummusRecipe = require('hummus-recipe')
const Path = require('path')
const fetch = require('node-fetch')

const configs = functions.config()
const bucket = admin.storage().bucket(configs.firestore.storagebucket)

let BROWSER

function pdf_mm_to_px (mm) {
  // Default PPI is 72 per inch for PDF
  return mm * constants.MM_IN_PX * 72
}

function mm_to_px (mm, PPI) {
  if (!PPI) {
    PPI = 600
  }
  return parseInt(parseInt(mm) * constants.MM_IN_PX * PPI)
}

async function upload_image(location, file_path, retry) {
  try {
    await bucket.upload(file_path, {
      destination:  location
    })
  } catch (err) {
    console.warn(`Error: Failed to upload ${file_path} to ${location}`, err)

    if (!retry) {
      return await upload_image(location, file_path, true)
    } else {
      throw new Error(`Error: Failed to upload`)
    }
  }


  return `https://storage.googleapis.com/${configs.firestore.storagebucket}/${location}?t=${parseInt(Math.random() * 10000000000)}`
}

async function copy_file(location, url) {
  try {
    let path = random_path('pdf')
    await fs.writeFileSync(path, await download_js(url));
    let link = await upload_image(location, path)
    fs.unlinkSync(path)
    return link
  } catch (err) {
    console.error(err)
  }
}

const API_URL = 'https://us-central1-hc-handwriting-engine-dev.cloudfunctions.net' // TODO
var STYLES = undefined
async function get_handwriting_style(style_label) {
  if (!STYLES) {
    let response = await fetch(`${API_URL}/api/getFonts?api_key=Qarl8oEIa2rO34R1jXop`)
    let data = await response.json()
    STYLES = data.fonts
  }

  for (let style of STYLES) {
    if (style.label === style_label) {
      return style
    }
  }
}

var COLOURS = undefined
async function get_handwriting_colour(colour_label) {
  if (!COLOURS) {
    let response = await fetch(`${API_URL}/api/getColours?api_key=Qarl8oEIa2rO34R1jXop`)
    let data = await response.json()
    COLOURS = data.colours
  }

  for (let colour of COLOURS) {
    if (colour.label === colour_label) {
      return colour
    }
  }
}

async function modify_stationary_html(stationary, _html, no_bleed, alter_paper_to, text, no_padding) {
  let bleed_mark_margin = '2mm'

  stationary.paper = constants.PAPER_SIZES[stationary.paper.key]

  // Fetch the style
  let handwriting_style = await get_handwriting_style(stationary.handwriting_style)

  // Fetch the colour
  let handwriting_colour = await get_handwriting_colour(stationary.handwriting_colour)

  let handwriting_size = 8
  if (stationary.handwriting_size === 'Small') {
    handwriting_size = 6
  } else if (stationary.handwriting_size === 'Medium') {
    handwriting_size = 7
  } else if (stationary.handwriting_size === 'Large') {
    handwriting_size = 8
  }

  let html = `
  <html>
    <head>
      <!-- TODO -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
      <script src="https://hc-handwriting-engine-de-79d8b.web.app/utils.js"></script>

      <style>
        body {
          -webkit-font-smoothing: antialiased;
          -moz-osx-font-smoothing: grayscale;
          margin: 0px;
          padding: 0px;
        }

        * {
          box-sizing: border-box;
        }

        #text-shape {
          shape-outside: ellipse(20px 100px at 0% 50%);
          width: 100px;
          height: 300px;
          float: left;
        }

        .home-text {
          /* width: 100%;
          height: 100%; */
          font-feature-settings: "rand";
          white-space: pre-wrap;
          font-variant-ligatures: none;
        }
      </style>
    </head>
    <body style="margin: 0px;">
      <div id="paper-container">
        <div id="bleed-marks" style="display: none;">
          <div style="box-sizing: border-box; height: ${constants.BLEED}mm; width: ${constants.BLEED}mm; position: absolute; top: 0px; left: 0px; pointer-events: none; border-style: solid; border-color: black; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 0px; border-top-width: 0px;"></div>
          <div style="height: ${bleed_mark_margin}; width: ${bleed_mark_margin}; position: absolute; top: calc(${constants.BLEED}mm - ${bleed_mark_margin}); left: calc(${constants.BLEED}mm - ${bleed_mark_margin}); pointer-events: none; background: white;"></div>

          <div style="box-sizing: border-box; height: ${constants.BLEED}mm; width: ${constants.BLEED}mm; position: absolute; bottom: 0px; left: 0px; pointer-events: none; border-style: solid; border-color: black; border-right-width: 1px; border-top-width: 1px; border-left-width: 0px; border-bottom-width: 0px;"></div>
          <div style="height: calc(${bleed_mark_margin} + 1px); width: calc(${bleed_mark_margin} + 1px); position: absolute; bottom: calc(${constants.BLEED}mm - ${bleed_mark_margin}); left: calc(${constants.BLEED}mm - ${bleed_mark_margin}); pointer-events: none; background: white;"></div>

          <div style="box-sizing: border-box; height: ${constants.BLEED}mm; width: ${constants.BLEED}mm; position: absolute; top: 0px; right: 0px; pointer-events: none; border-style: solid; border-color: black; border-left-width: 1px; border-bottom-width: 1px; border-right-width: 0px; border-top-width: 0px;"></div>
          <div style="height: ${bleed_mark_margin}; width: ${bleed_mark_margin}; position: absolute; top: calc(${constants.BLEED}mm - ${bleed_mark_margin}); right: calc(${constants.BLEED}mm - ${bleed_mark_margin}); pointer-events: none; background: white;"></div>

          <div style="box-sizing: border-box; height: ${constants.BLEED}mm; width: ${constants.BLEED}mm; position: absolute; bottom: 0px; right: 0px; pointer-events: none; border-style: solid; border-color: black; border-left-width: 1px; border-top-width: 1px; border-right-width: 0px; border-bottom-width: 0px;"></div>
          <div style="height: calc(${bleed_mark_margin} + 1px); width: calc(${bleed_mark_margin} + 1px); position: absolute; bottom: calc(${constants.BLEED}mm - ${bleed_mark_margin}); right: calc(${constants.BLEED}mm - ${bleed_mark_margin}); pointer-events: none; background: white;"></div>
        </div>
        ${_html}
      </div>

      <svg xmlns="http://www.w3.org/2000/svg" version="1.1">
        <defs>
          <filter id="text-colour" color-interpolation-filters="sRGB">
            <feTurbulence type="fractalNoise" baseFrequency="${handwriting_colour.colour_freq}" numOctaves="1" seed="1" result="turbulence1" stitchTiles="stitch"/>
            <feColorMatrix type="matrix" values="${handwriting_colour.colour_r} 0 0 0 0
            0 ${handwriting_colour.colour_g} 0 0 0
            0 0 ${handwriting_colour.colour_b} 0 0
            0 0 0 1 1" in="turbulence1" result="colormatrix"/>
            <feComposite in="SourceGraphic" in2="composite" operator="in" result="composite1"/>
            <feComposite in="colormatrix" in2="composite1" operator="in" result="composite"/>
          </filter>
        </defs>
      </svg>

      <script>

      var text = \`${text}\`

      if (text) {

        window.handwriting_style = ${JSON.stringify(handwriting_style)}
        window.handwriting_colour = ${JSON.stringify(handwriting_colour)}
        window.font_size_mm = ${handwriting_size} * (window.handwriting_style.size_multiplier || 1)

        var seed = Math.floor(Math.random() * Math.floor(10000))

        $( document ).ready(async function() {
          $("head").append(\`<style>@font-face {font-family: "HandwritingFontFamily"; src: url("\${window.handwriting_style.url}");}</style>\`)

          $("#text").css({
            "font-family": \`HandwritingFontFamily\`,
            "font-size": \`\${window.font_size_mm}mm\`,
            "line-height": window.handwriting_style.line_height || 1.2 + "em",
            "filter": "url('#text-colour')"
          })

          var text = \`${text}\`

          let config = {
            top_max: window.font_size_mm * window.handwriting_style.amplitude,
            sleep_max: window.handwriting_style.cap_length,
            direction: window.handwriting_style.speed,
            do_sleep: window.handwriting_style.cap_prob
          }

          let out = wrap_characters(seed, text, window.handwriting_style.variants, undefined, config)[0]
          $("#text").html(out.replace(/\\n/g, "<br>"))
          await resize_text(window.font_size_mm, '#handwriting-image-container', '#text')
        })
      }
    </script>
    </body>
  </html>`

  const dom = new JSDOM(html)
  let paper_preview = dom.window.document.querySelector('.paper-preview')

  dom.window.document.querySelector('#bleed-marks').style['display'] = 'none'

  let paper_width = stationary.paper.width
  let paper_height = stationary.paper.height

  if (alter_paper_to) {
    paper_width = alter_paper_to.width
    paper_height = alter_paper_to.height

    dom.window.document.querySelector('#paper-container').style['position'] = `absolute`

    if (paper_preview) {
      if (no_padding) {
        paper_preview.style['transform'] = `scale(${stationary.paper.scale_back})`
      } else {
        paper_preview.style['transform'] = `scale(${stationary.paper.scale})`
        paper_preview.style['margin'] = `auto`
      }

      let handwriting_image_container_ele = dom.window.document.querySelector("#handwriting-image-container")
      if (handwriting_image_container_ele && alter_paper_to && stationary.paper.handwriting_image_size_override) {
        // handwriting_image_container_ele.style.height = parseFloat(handwriting_image_container_ele.style.height.replace('px', '')) * stationary.paper.scale + 'px'
        handwriting_image_container_ele.style.height = stationary.paper.handwriting_image_size_override + 'px'
      }

      paper_preview.style['position'] = `relative`

      let top = ((stationary.paper.height * stationary.paper.scale) - stationary.paper.height) / 2

      if (no_padding) {
        paper_preview.style['transform-origin'] = `0px 0px`
      } else if (stationary.paper.left) {
        paper_preview.style['transform-origin'] = `0px 0px`
      } else {
        paper_preview.style['transform-origin'] = `center`
      }

      if (!no_padding && stationary.paper.top) {
        paper_preview.style['top'] = `${stationary.paper.top}mm`
      } else if (!no_padding && top > 0) {
        paper_preview.style['top'] = `${top}mm`
      }
    }
  }

  let width = parseFloat(paper_width) + constants.BLEED * 2
  let height = parseFloat(paper_height) + constants.BLEED * 2
  let margin = constants.BLEED

  if (no_padding) {
    // width = paper_width
    // height = paper_height
    margin = 0
  }

  if (!no_bleed) {
    dom.window.document.querySelector('#paper-container').style['width'] = `${width}mm`
    dom.window.document.querySelector('#paper-container').style['height'] = `${height}mm`

    let background_image_container_ele = dom.window.document.querySelector("#back-image-container")

    if (no_padding) {
      if (!alter_paper_to) {
        paper_preview.style['width'] = `${width}mm`
        paper_preview.style['height'] = `${height}mm`
      }

      if (!background_image_container_ele.style.transform) {
        background_image_container_ele.style['width'] = `${parseFloat(stationary.paper.width) + constants.BLEED * 2}mm`
        background_image_container_ele.style['height'] = `${parseFloat(stationary.paper.height) + constants.BLEED * 2}mm`
      } else {
        background_image_container_ele.style['width'] = (constants.BLEED * 2 * 3.7795275591) + parseFloat(background_image_container_ele.style['width'].replace('px', '')) + 'px'
        background_image_container_ele.style['height'] = (constants.BLEED * 2 * 3.7795275591) + parseFloat(background_image_container_ele.style['height'].replace('px', '')) + 'px'
      }
    }

    dom.window.document.querySelector('#bleed-marks').style['width'] = `${width}mm`
    dom.window.document.querySelector('#bleed-marks').style['height'] = `${height}mm`
    dom.window.document.querySelector('#bleed-marks').style['position'] = 'absolute'
    dom.window.document.querySelector('#bleed-marks').style['display'] = 'block'

    if (no_padding) {
      if (alter_paper_to && stationary.paper.back_left) {
        paper_preview.style['margin-left'] = `${constants.BLEED}mm`
      }
    } else if (alter_paper_to) {
      paper_preview.style['margin-top'] = `${constants.BLEED}mm`
      if (stationary.paper.left) {
        paper_preview.style['margin-left'] = `${constants.BLEED}mm`
      }
    } else {
      dom.window.document.querySelector('.paper-preview').style['margin'] = `${margin}mm`
    }
  } else {
    dom.window.document.querySelector('#paper-container').style['width'] = `${paper_width}mm`
    dom.window.document.querySelector('#paper-container').style['height'] = `${paper_height}mm`
  }

  let handwriting_image_ele = dom.window.document.querySelector("#handwriting-image")
  if (text) {
    dom.window.document.querySelector('#handwriting-image-container').insertAdjacentHTML('beforeend', `<p class="home-text" id="text"></p>`)

    if (dom.window.document.querySelector("#handwriting-image")) {
      dom.window.document.querySelector("#handwriting-image").remove()
    }
  }

  if (!text && handwriting_image_ele) {
    dom.window.document.querySelector("#handwriting-image").remove()
  }

  let new_html = dom.serialize()
  new_html = new_html.replace('background-image: linear-gradient(45deg, rgb(232, 232, 232) 25%, rgb(227, 227, 227) 25%, rgb(227, 227, 227) 50%, rgb(232, 232, 232) 50%, rgb(232, 232, 232) 75%, rgb(227, 227, 227) 75%, rgb(227, 227, 227) 100%);', '')

  return new_html
}

async function modify_envelope_html(stationary, envelope, recipient_text, return_address_text) {
  // Fetch the style
  let handwriting_style = await get_handwriting_style(stationary.handwriting_style)

  // Fetch the colour
  let handwriting_colour = await get_handwriting_colour(stationary.handwriting_colour)

  let handwriting_size = 7
  if (stationary.handwriting_size === 'Small') {
    handwriting_size = 6 // Min size is this for envelopes
  } else if (stationary.handwriting_size === 'Medium') {
    handwriting_size = 7
  } else if (stationary.handwriting_size === 'Large') {
    handwriting_size = 8
  }

  console.log(return_address_text, 'ASFSDA', recipient_text)

  let html = `
  <html>
    <head>
      <!-- TODO -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
      <script src="https://hc-handwriting-engine-de-79d8b.web.app/utils.js"></script>

      <style>
        body {
          -webkit-font-smoothing: antialiased;
          -moz-osx-font-smoothing: grayscale;
        }

        * {
          box-sizing: border-box;
        }

        .home-text {
          font-feature-settings: "rand";
          white-space: pre-wrap;
          font-variant-ligatures: none;
        }
      </style>
    </head>
    <body style="margin:0px">
      <div id="paper-container" style="display:flex;align-items:center;justify-content:center;height:${envelope.height}mm;width:${envelope.width}mm;">
        <div id="content-container" style="display:flex;flex-direction:column;position:relative;height:90%;width:90%;">`
  if (return_address_text) {
    html += `
          <div style="max-width:80%;max-height:40%">
            <div id="sender-address-container" style="max-height:100%;max-width:100%;width:90mm;">
              <p class="home-text" id="return_address_text"></p>
            </div>
          </div>
          <div style="position:relative;display:flex;justify-content:flex-end;height:100%;width:100%;">
            <div id="recipient-text-container" style="position:absolute;top:40%;transform:translateY(-50%);max-height:80%;min-height: 80%;max-width:80%;width:120mm;">
              <p class="home-text" id="recipient_text"></p>
            </div>`
  } else {
    if (recipient_text) {
      html += `
          <div style="position:relative;display:flex;justify-content:center;height:100%;width:100%;">
            <div id="sender-address-container" hidden>
              <p class="home-text" id="return_address_text" hidden></p>
            </div>
            <div id="recipient-text-container" style="position:absolute;top:50%;transform:translateY(-40%);max-height:60%;min-height: 60%;max-width:80%;width:120mm;">
              <p class="home-text" id="recipient_text" ></p>
            </div>`
    }
  }
  html += `
          </div>
        </div>
      </div>

      <svg xmlns="http://www.w3.org/2000/svg" version="1.1">
        <defs>
          <filter id="text-colour" color-interpolation-filters="sRGB">
            <feTurbulence type="fractalNoise" baseFrequency="${handwriting_colour.colour_freq}" numOctaves="1" seed="1" result="turbulence1" stitchTiles="stitch"/>
            <feColorMatrix type="matrix" values="${handwriting_colour.colour_r} 0 0 0 0
            0 ${handwriting_colour.colour_g} 0 0 0
            0 0 ${handwriting_colour.colour_b} 0 0
            0 0 0 1 1" in="turbulence1" result="colormatrix"/>
            <feComposite in="SourceGraphic" in2="composite" operator="in" result="composite1"/>
            <feComposite in="colormatrix" in2="composite1" operator="in" result="composite"/>
          </filter>
        </defs>
      </svg>

      <script>

      window.handwriting_style = ${JSON.stringify(handwriting_style)}
      window.handwriting_colour = ${JSON.stringify(handwriting_colour)}
      window.font_size_mm = ${handwriting_size} * (window.handwriting_style.size_multiplier || 1)

      var seed = Math.floor(Math.random() * Math.floor(10000))

      $( document ).ready(async function() {
        $("head").append(\`<style>@font-face {font-family: "HandwritingFontFamily"; src: url("\${window.handwriting_style.url}");}</style>\`)

        $("#recipient_text").css({
          "font-family": \`HandwritingFontFamily\`,
          "font-size": \`\${window.font_size_mm}mm\`,
          "line-height": window.handwriting_style.line_height || 1.2 + "em",
          "filter": "url('#text-colour')"
        })

        $("#return_address_text").css({
          "font-family": \`HandwritingFontFamily\`,
          "font-size": \`\${window.font_size_mm}mm\`,
          "line-height": window.handwriting_style.line_height || 1.2 + "em",
          "filter": "url('#text-colour')"
        })

        let config = {
          top_max: window.font_size_mm * window.handwriting_style.amplitude,
          sleep_max: window.handwriting_style.cap_length,
          direction: window.handwriting_style.speed,
          do_sleep: window.handwriting_style.cap_prob
        }

        var recipient_text = \`${recipient_text}\`
        let out = wrap_characters(seed, recipient_text, window.handwriting_style.variants, undefined, config)[0]
        $("#recipient_text").html(out.replace(/\\n/g, "<br>"))
        await resize_text(window.font_size_mm, '#recipient-text-container', '#recipient_text')

        var return_address_text = \`${return_address_text}\`
        out = wrap_characters(seed, return_address_text, window.handwriting_style.variants, undefined, config)[0]
        $("#return_address_text").html(out.replace(/\\n/g, "<br>"))
        await resize_text(window.font_size_mm, '#sender-text-container', '#return_address_text')
      })
      </script>
    </body>
  </html>`

  const dom = new JSDOM(html)

  let new_html = dom.serialize()

  return new_html
}

function random_path(extension) {
  return `/tmp/${parseInt(Math.random() * 10000000000)}.${extension}`
}

/**
 * Merge two pdfs
 *
 * @param {*} front_pdf_path
 * @param {*} back_pdf_path
 * @returns temporary pdf path for merged pdf
 */
async function merge_pdf(front_pdf_path, back_pdf_path) {
  let merge_pdf_name = random_path('pdf')

  const pdfDoc = new HummusRecipe(front_pdf_path, merge_pdf_name)
  pdfDoc.appendPage(back_pdf_path).endPDF()

  return merge_pdf_name
}

async function load_html_page(html, debug) {
  debug = debug === undefined ? false : debug

  if (!BROWSER || debug) { // cache puppeteer browser
    BROWSER = await puppeteer.launch({headless: !debug, args: ['--no-sandbox', '--disable-setuid-sandbox']})
  }

  let page = await BROWSER.newPage()
  await page.setContent(html, {
    waitUntil: 'networkidle0',
  })

  return page
}

async function render_html_to_pdf(html, width, height, add_bleed, debug) {
  let page = await load_html_page(html, debug)
  let path = random_path('pdf')

  let bleed = constants.BLEED
  if (add_bleed === false) {
    bleed = 0
  }

  let page_width = parseFloat(width) + parseFloat(bleed)*2
  let page_height = parseFloat(height) + parseFloat(bleed)*2

  await page.pdf({
    path: path,
    width: `${page_width}mm`,
    height: `${page_height}mm`,
    printBackground: true,
    pageRanges: '1',
    margin: {top: 0, right: 0, bottom: 0, left: 0},
    displayHeaderFooter: false
  })

  return path
}

async function render_html_to_png(html, width, height, add_bleed) {
  let page = await load_html_page(html)
  let path = random_path('png')

  let bleed = constants.BLEED
  if (add_bleed === false) {
    bleed = 0
  }

  let page_width = parseFloat(width) + parseFloat(bleed)*2
  let page_height = parseFloat(height) + parseFloat(bleed)*2

  let element = await page.$('#paper-container')
  await element.screenshot({
    path: path,
    printBackground: true
  })

  page.close()

  return path
}

async function resize_image(object) {
  let file_path = object.name

  if (object.metadata && object.metadata.resized) {
    // Already resized
    return
  }

  const file_name = Path.basename(file_path)
  if (file_name !== 'logo.png' && file_name !== 'back_image.png' && file_name !== 'front_image.png') {
    // Not a target file
    return
  }

  let source_file = random_path()
  await bucket.file(file_path).download({
    destination: source_file
  })

  let out_file = random_path()

  let image = sharp(source_file)
  let metadata = await image.metadata()
  if (file_name === 'logo.png' && metadata.width >= 1200) {
    image = await image.resize({width: 1200})
  } else if ((file_name === 'back_image.png' || file_name === 'front_image.png') && metadata.width >= 1200) {
    image = await image.resize({width: 1200})
  }

  if (file_name === 'logo.png' && metadata.height >= 600) {
    image = await image.resize({height: 600})
  }

  await image.png({progressive: true, force: false, compressionLevel: 6}).toFile(out_file)

  await bucket.upload(out_file, {destination: file_path, metadata: {contentType: object.contentType, resized: true}})

  await fs.unlinkSync(source_file)
  await fs.unlinkSync(out_file)

  return true
}

module.exports = {
  mm_to_px,
  pdf_mm_to_px,
  load_html_page,
  render_html_to_png,
  render_html_to_pdf,
  merge_pdf,
  upload_image,
  modify_stationary_html,
  modify_envelope_html,
  resize_image,
  copy_file
}
