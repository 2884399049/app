const functions = require('firebase-functions')
const sendgrid_email = require('@sendgrid/mail')

const admin = require('firebase-admin')
const db = admin.firestore()

const configs = functions.config()
sendgrid_email.setApiKey(configs.sendgrid.apikey)

function send_email (to, subject, text, from, cc) {
  const message = {
    to: to,
    subject: subject,
    text: text,
    html: text
    // templateId: '<add send grid custom template id here>',
    // substitutionWrappers: ['{{', '}}'],
    // substitutions: {
    //   name: customerdata.displayName
    // }
  }

  if (cc) {
    message['cc'] = cc
  }

  if (from) {
    message['from'] = from
  } else {
    message['from'] = 'support@scribeless.co'
  }

  console.debug(`Sending`, message)

  return sendgrid_email.send(message)
}

function end_stream (stream) {
  return new Promise((resolve, reject) => {
    stream.on('finish', () => resolve())
    stream.on('error', reject)
  })
}

async function get_user_profile (uid) {
  var user_profile_doc = await db.collection('users').doc(uid).get()
  var user_profile = user_profile_doc.data()
  user_profile['id'] = uid

  return user_profile
}

module.exports = {
  get_user_profile,
  send_email,
  end_stream
}
