function delete_collection (db, collection_ref) {
  var batchSize = 500 // Max in firestore
  var query = collection_ref.orderBy('__name__').limit(batchSize)

  return new Promise((resolve, reject) => {
    deleteQueryBatch(db, query, batchSize, resolve, reject)
  })
}

function deleteQueryBatch (db, query, batchSize, resolve, reject) {
  query
    .get()
    .then(snapshot => {
      // When there are no documents left, we are done
      if (snapshot.size === 0) {
        return 0
      }

      // Delete documents in a batch
      var batch = db.batch()
      snapshot.docs.forEach(doc => {
        batch.delete(doc.ref)
      })

      return batch.commit().then(() => {
        return snapshot.size
      })
    })
    .then(numDeleted => {
      if (numDeleted === 0) {
        resolve()
        return
      }

      // Recurse on the next process tick, to avoid
      // exploding the stack.
      process.nextTick(() => {
        deleteQueryBatch(db, query, batchSize, resolve, reject)
      })
    })
    .catch(reject)
}

function record_status_change_time (snap) {
  if (!snap.after.data()) {
    return -1
  }
  let obj = {}
  let object_status = snap.after.data().status
  let object_status_before = null
  let status_stats = snap.after.data().status_stats || {}
  let key = 'status_stats.' + object_status
  let key_before = null
  if (snap.before.data()) {
    object_status_before = snap.before.data().status
    if (object_status === object_status_before) {
      return -1
    }
    let key_before = 'status_stats.' + object_status_before
    if (!status_stats[object_status_before]) {
      obj[key_before] = {
        last_entered: new Date(),
        times_entered: 1,
        duration_at: 0
      }
    } else {
      let times_entered = status_stats[object_status_before].times_entered
      let entered_into_last_status = status_stats[object_status_before].last_entered.toDate()
      let duration_at_last_status = new Date() - entered_into_last_status
      let total_duration = status_stats[object_status_before].duration_at + duration_at_last_status
      obj[key_before] = {
        last_entered: entered_into_last_status,
        times_entered: times_entered,
        duration_at: total_duration
      }
    }
  }
  if (!status_stats[object_status]) {
    obj[key] = {
      last_entered: new Date(),
      times_entered: 1,
      duration_at: 0
    }
  } else {
    obj[key] = {
      last_entered: new Date(),
      times_entered: status_stats[object_status].times_entered + 1,
      duration_at: status_stats[object_status].duration_at
    }
  }
  let doc_ref = snap.after.ref
  doc_ref.update(obj)
}

module.exports = {
  delete_collection,
  record_status_change_time
}
