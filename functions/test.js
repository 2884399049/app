const moment = require('moment')
const Hubspot = require('hubspot')
const hubspot = new Hubspot({apiKey: '780ee2ee-65fc-4066-a5c5-8662fadda806'})

function date_now(time) {
  return moment.utc(moment(time).format('YYYY-MM-DD'), 'YYYY-MM-DD').unix()
}

async function update_deal_for_invoice () {
  const HUBSPOT_STAGE_NAMES = {
    'draft': 1428884,
    'charged': 1427707
  }

  let dealstage = HUBSPOT_STAGE_NAMES.draft // HUBSPOT_STAGE_NAMES.charged
  let amount_gbp = 100
  let billing_to = new Date()
  let billing_from = new Date()

  let from = moment(billing_from).format('DD/MM/YY')
  let to = moment(billing_to).format('DD/MM/YY')

  // Create a new deal
  let contact = await hubspot.contacts.getByEmail('testasdasd@test.com')

  console.log(contact)

  let deal_data = {
    associations: {associatedVids: [contact.vid]},
    properties: [
      {name: 'dealname', value: `TEST INVOICE invoice ${from}-${to}`},
      {name: 'dealstage', value: dealstage},
      {name: 'pipeline', value: '1427701'}, // Platform pipeline
      {name: 'closedate', value: date_now(billing_to)},
      {name: 'amount', value: `${amount_gbp}`},
      {name: 'recurring_revenue_amount', value: `${0 / 100}`}
    ]
  }
  // console.log('Deal data', deal_data)
  try {
    // let deal = await hubspot.deals.create(deal_data)
    let deal = await hubspot.deals.updateById('1879956055', deal_data)
    console.log(deal)
  } catch(err) {
    console.error(err)
  }

}

function convert_properties (obj) {
  let properties = []
  for (let key in obj) {
    properties.push({property: key, value: obj[key]})
  }
  return properties
}

async function update_user() {
  user_properties = [{'property': 'email', 'value': 'testasdasd2@test.com'}, {'property': 'firstname', 'value': 'ABC'}]

  // let contact = await hubspot.contacts.getByEmail('testasdasd2@test.com')
  let contact = await hubspot.contacts.create({properties: user_properties})

  // contact = await hubspot.contacts.update(contact.vid, {properties: user_properties})
  console.log(contact)
}

update_deal_for_invoice()
// update_user()
