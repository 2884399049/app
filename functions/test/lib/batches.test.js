const projectId = 'batches-test'
process.env.GCLOUD_PROJECT = projectId
process.env.FIRESTORE_EMULATOR_HOST = 'localhost:8080'

const firebase_test = require('firebase-functions-test')()
const firebase = require('@firebase/testing')
const moment = require('moment')

firebase_test.mockConfig({slack: {api_key: '', channels: {}}, environment: 'testing', stripe: {key: ''}, sendgrid: {apikey: ''}, hubspot: {api_key: '', enabled: false}, firestore: {storagebucket: projectId}});

const admin = require('firebase-admin')

admin.initializeApp({ projectId })

const db = admin.firestore()

const printer_utils = require('../../shared/printer')
const constants = require('../../shared/constants')
const batches = require('functions/lib/batches.js')
const pricing = require('functions/lib/pricing.js')
const utils = require('functions/lib/utils')

const profit_code = `function main(goods) {
  let total = 0
  for (let good in goods) {
    let quantity = goods[good]
    if (good === constants.GOOD.full_service_c5) {
      total += 112314 * quantity
    } else if (good === constants.GOOD.full_service_a5) {
      total += 542441 * quantity
    } else if (good === constants.GOOD.stamp_gb_first_class) {
      total += 891934 * quantity
    } else if (good === constants.GOOD.full_service_a4) {
      total += 941949 * quantity
    }
  }

  return total
}`

const mock_printer_1_id = '4'
const mock_printer_1 = {
  profit_code: profit_code,
  location: 'GB',
  serviceable_countries: ['GB', 'FR'],
  serviceable_goods: [constants.GOOD.full_service_c5, constants.GOOD.full_service_a5, constants.GOOD.stamp_gb_first_class]
}

const mock_printer_2_id = '5'
const mock_printer_2 = {
  profit_code: profit_code,
  location: 'US',
  serviceable_countries: ['US'],
  serviceable_goods: [constants.GOOD.full_service_c5, constants.GOOD.full_service_a5, constants.GOOD.stamp_gb_first_class]
}

const mock_campaign_1 = {status: constants.CAMPAIGN_STATUS.pending, product: constants.PRODUCT.full_service, stationary_required: false, envelope: {required: constants.ENVELOPE_REQUIRED.no}}
const mock_campaign_1_id = '7'

const mock_campaign_2 = {status: constants.CAMPAIGN_STATUS.ready_to_ship, product: constants.PRODUCT.full_service, stationary_required: false, envelope: {required: constants.ENVELOPE_REQUIRED.no}}
const mock_campaign_2_id = '8'
const mock_campaign_3_id = '9'

const mock_recipient_1_id = '1'
const mock_recipient_1 = {country: 'GB', status: constants.RECIPIENT_STATUS.ready_to_print}
const mock_recipient_2_id = '2'
const mock_recipient_2 = {country: 'US', status: constants.RECIPIENT_STATUS.ready_to_print}
const mock_recipient_3_id = '3'
const mock_recipient_3 = {country: 'US', status: constants.RECIPIENT_STATUS.pending}

// Testing
describe('batch_printer_check', () => {
  beforeEach(async () => {
    await firebase.clearFirestoreData({ projectId });
  })

  it('Throws warnings', async () => {
    const mock_stationery = {paper: {name: 'A5'}}
    const mock_stationery_id = '5'
    await db.collection('stationary').doc(mock_stationery_id).set(mock_stationery)

    const mock_campaign = {stationary: mock_stationery_id, product: constants.PRODUCT.full_service, stationary_required: true, envelope: {required: constants.ENVELOPE_REQUIRED.yes, stamp: constants.STAMP_TYPES.first_class, paper: {name: 'C5'}}}
    const mock_campaign_id = '7'
    await db.collection('campaigns').doc(mock_campaign_id).set(mock_campaign)

    const mock_recipient_1_id = '1'
    const mock_recipient_1 = {country: 'GB'}
    const mock_recipient_2_id = '2'
    const mock_recipient_2 = {country: 'GB'}
    await db.collection('campaigns').doc(mock_campaign_id).collection('recipinets').doc(mock_recipient_1_id).set(mock_recipient_1)
    await db.collection('campaigns').doc(mock_campaign_id).collection('recipinets').doc(mock_recipient_2_id).set(mock_recipient_2)

    const profit_code = `function main(goods) {
      let total = 0
      for (let good in goods) {
        let quantity = goods[good]
        if (good === constants.GOOD.full_service_a5_pack) {
          total += 654755 * quantity
        } else if (good === constants.GOOD.stamp_gb_first_class) {
          total += 891934 * quantity
        } else if (good === constants.GOOD.full_service_a4) {
          total += 941949 * quantity
        }
      }

      return total
    }`
    const mock_printer_id = '4'
    const mock_printer = {
      profit_code: profit_code,
      serviceable_countries: ['GB'],
      serviceable_goods: [constants.GOOD.full_service_a5_pack, constants.GOOD.stamp_gb_first_class]
    }
    await db.collection('printers').doc(mock_printer_id).set(mock_printer)

    const mock_batch_id = '3'
    const mock_batch = {
      id: mock_batch_id,
      printer_doc: db.collection('printers').doc(mock_printer_id)
    }
    await db.collection('batches').doc(mock_batch.id).set(mock_batch)
    await db.collection('batches').doc(mock_batch.id).collection('recipients').doc(mock_recipient_1_id).set({recipient_doc: db.collection('campaigns').doc(mock_campaign_id).collection('recipinets').doc(mock_recipient_1_id), campaign: mock_campaign_id})
    await db.collection('batches').doc(mock_batch.id).collection('recipients').doc(mock_recipient_2_id).set({recipient_doc: db.collection('campaigns').doc(mock_campaign_id).collection('recipinets').doc(mock_recipient_2_id), campaign: mock_campaign_id})

    const expected_cost = 654755 * 2 + 891934 * 2

    expect(await batches.batch_printer_check(mock_batch)).toEqual({warnings: [], cost: expected_cost})

    const mock_printer_no_c5 = {
      profit_code: profit_code,
      serviceable_countries: ['US'],
      serviceable_goods: [constants.GOOD.stamp_gb_first_class]
    }
    await db.collection('printers').doc(mock_printer_id).set(mock_printer_no_c5)

    expect(await batches.batch_printer_check(mock_batch)).toEqual({warnings: [`Unserviceable country found: GB`, `Unserviceable good found: Full Service A5 pack`], cost: expected_cost})
  })
})

describe('send_to_batches', () => {
  beforeEach(async () => {
    await firebase.clearFirestoreData({ projectId });

    await db.collection('printers').doc(mock_printer_1_id).set(mock_printer_1)
    await db.collection('printers').doc(mock_printer_2_id).set(mock_printer_2)

    // Add some campaigns
    await db.collection('campaigns').doc(mock_campaign_1_id).set(mock_campaign_1)
    await db.collection('campaigns').doc(mock_campaign_2_id).set(mock_campaign_2)
    await db.collection('campaigns').doc(mock_campaign_3_id).set(mock_campaign_2)

    // Add recipients to campaigns
    await db.collection('campaigns').doc(mock_campaign_1_id).collection('recipients').doc(mock_recipient_1_id).set(mock_recipient_1)
    await db.collection('campaigns').doc(mock_campaign_1_id).collection('recipients').doc(mock_recipient_2_id).set(mock_recipient_2)
    await db.collection('campaigns').doc(mock_campaign_1_id).collection('recipients').doc(mock_recipient_3_id).set(mock_recipient_3)

    await db.collection('campaigns').doc(mock_campaign_2_id).collection('recipients').doc(mock_recipient_1_id).set(mock_recipient_1)
    await db.collection('campaigns').doc(mock_campaign_2_id).collection('recipients').doc(mock_recipient_2_id).set(mock_recipient_2)
    await db.collection('campaigns').doc(mock_campaign_2_id).collection('recipients').doc(mock_recipient_3_id).set(mock_recipient_3)

    await db.collection('campaigns').doc(mock_campaign_3_id).collection('recipients').doc(mock_recipient_1_id).set(mock_recipient_1)
    await db.collection('campaigns').doc(mock_campaign_3_id).collection('recipients').doc(mock_recipient_2_id).set(mock_recipient_2)
    await db.collection('campaigns').doc(mock_campaign_3_id).collection('recipients').doc(mock_recipient_3_id).set(mock_recipient_3)
  })

  it('Throws error when no printer choosen', async () => {
    await db.collection('printers').doc(mock_printer_2_id).update({serviceable_countries: ['CA']})

    await expect(batches.send_to_batches()).rejects.toThrowError(/No printer choosen for recipient/)
  })

  it('Throws error if code error from calculate_lead_time', async () => {
    printer_utils.calculate_lead_time = jest.fn().mockReturnValue({code_error: 'code error', code_output: undefined})
    await expect(batches.send_to_batches()).rejects.toThrowError(/Error calculating lead time/)

    printer_utils.calculate_lead_time = jest.fn().mockReturnValue({code_error: undefined, code_output: []})
    await expect(batches.send_to_batches()).rejects.toThrowError(/Error calculating lead time/)
  })

  it('Correctly adds lead times', async () => {
    let delivery_date = moment().add(2, 'days').toDate()
    await db.collection('campaigns').doc(mock_campaign_1_id).update({delivery: {due_date: delivery_date}, status: constants.CAMPAIGN_STATUS.ready_to_ship})
    await db.collection('campaigns').doc(mock_campaign_2_id).update({status: constants.CAMPAIGN_STATUS.pending})
    await db.collection('campaigns').doc(mock_campaign_3_id).update({status: constants.CAMPAIGN_STATUS.pending})

    await db.collection('campaigns').doc(mock_campaign_1_id).collection('recipients').doc(mock_recipient_2_id).set({country: 'GB'})

    printer_utils.calculate_lead_time = jest.fn().mockReturnValue({code_error: undefined, code_output: [2, 3]})

    await batches.send_to_batches()

    let query_docs = await db.collection('batches').get()
    expect(query_docs.docs.length).toEqual(1)

    let batch
    for (let batch_doc of query_docs.docs) {
      let batch = batch_doc.data()
      batch['id'] = batch_doc.id

      let recipients_docs = await db.collection('batches').doc(batch.id).collection('recipients').get()

      expect(recipients_docs.docs.length).toEqual(1)

      for (let recipients_doc of recipients_docs.docs) {
        let recipient = recipients_doc.data()
        recipient['id'] = recipients_doc.id

        expect(recipient.recipient_doc).toEqual(db.collection('campaigns').doc(mock_campaign_1_id).collection('recipients').doc(mock_recipient_1_id))
        expect(recipient.campaign).toEqual(mock_campaign_1_id)
      }
    }
  })

  it('Doesn\'t create batches for empty campaigns', async () => {
    await db.collection('campaigns').doc(mock_campaign_1_id).update({status: constants.CAMPAIGN_STATUS.ready_to_ship})
    await db.collection('campaigns').doc(mock_campaign_2_id).update({status: constants.CAMPAIGN_STATUS.pending})
    await db.collection('campaigns').doc(mock_campaign_3_id).update({status: constants.CAMPAIGN_STATUS.pending})

    await db.collection('campaigns').doc(mock_campaign_1_id).collection('recipients').doc(mock_recipient_1_id).update({status: constants.RECIPIENT_STATUS.pending})
    await db.collection('campaigns').doc(mock_campaign_1_id).collection('recipients').doc(mock_recipient_2_id).update({status: constants.RECIPIENT_STATUS.pending})
    await db.collection('campaigns').doc(mock_campaign_1_id).collection('recipients').doc(mock_recipient_3_id).update({status: constants.RECIPIENT_STATUS.pending})

    await batches.send_to_batches()

    let query_docs = await db.collection('batches').get()
    expect(query_docs.docs.length).toEqual(0)
  })

  it('Adds correct notes', async () => {
    await db.collection('campaigns').doc(mock_campaign_1_id).update({status: constants.CAMPAIGN_STATUS.ready_to_ship})
    await db.collection('campaigns').doc(mock_campaign_2_id).update({status: constants.CAMPAIGN_STATUS.pending})
    await db.collection('campaigns').doc(mock_campaign_3_id).update({status: constants.CAMPAIGN_STATUS.pending})

    await db.collection('campaigns').doc(mock_campaign_1_id).collection('recipients').doc(mock_recipient_2_id).update({country: 'FR'})

    printer_utils.calculate_lead_time = jest.fn().mockReturnValue({code_error: undefined, code_output: [2, 3]})

    await batches.send_to_batches()

    let query_docs = await db.collection('batches').get()

    expect(query_docs.docs.length).toEqual(1)

    let batch
    for (let query_doc of query_docs.docs) {
      batch = query_doc.data()
    }
    expect(batch.notes).toEqual(`Sending ${mock_campaign_1_id + '-' + mock_recipient_2_id}.pdf to country outside of printer location: FR`)
  })

  it('Sends to batches with print_override_location', async () => {
    await db.collection('campaigns').doc(mock_campaign_1_id).update({status: constants.CAMPAIGN_STATUS.ready_to_ship, print_override_location: mock_printer_2_id})
    await db.collection('campaigns').doc(mock_campaign_2_id).update({status: constants.CAMPAIGN_STATUS.ready_to_ship, print_override_location: 'None'})
    await db.collection('campaigns').doc(mock_campaign_3_id).update({status: constants.CAMPAIGN_STATUS.pending})

    await db.collection('campaigns').doc(mock_campaign_1_id).collection('recipients').doc(mock_recipient_1_id).update({country: 'US'})
    await db.collection('campaigns').doc(mock_campaign_1_id).collection('recipients').doc(mock_recipient_2_id).update({country: 'GB'})

    printer_utils.calculate_lead_time = jest.fn().mockReturnValue({code_error: undefined, code_output: [2, 3]})

    await batches.send_to_batches()

    let query_docs = await db.collection('batches').get()

    expect(query_docs.docs.length).toEqual(1)

    let batch
    for (let query_doc of query_docs.docs) {
      batch = query_doc.data()
    }
    expect(batch.printer_doc).toEqual(db.collection('printers').doc(mock_printer_2_id))

  })

  it('Sends to batch deletes existing batches', async () => {
    const new_batch_1_id = '9'
    await db.collection('batches').doc(new_batch_1_id).set({
      printer_doc: db.collection('printers').doc(mock_printer_2_id),
      zip: false,
      status: constants.BATCH_STATUS.pending,
      created: new Date(),
      notes: 'notes',
      count: 1
    })
    const new_batch_2_id = '10'
    await db.collection('batches').doc(new_batch_2_id).set({
      printer_doc: db.collection('printers').doc(mock_printer_2_id),
      zip: false,
      status: constants.BATCH_STATUS.shipped,
      created: new Date(),
      notes: 'notes',
      count: 1
    })

    await batches.send_to_batches()

    let query_docs = await db.collection('batches').get()

    expect(query_docs.docs.length).toEqual(3)

    let batch
    for (let query_doc of query_docs.docs) {
      batch = query_doc.data()
      batch.id = query_doc.id
      if (batch.id === new_batch_1_id) { // Overwrites existing batch
        expect(batch.printer_doc).toEqual(db.collection('printers').doc(mock_printer_2_id))
        expect(batch.status).toEqual(constants.BATCH_STATUS.pending)
        expect(batch.count).toEqual(2)

        let recipients_docs = await db.collection('batches').doc(batch.id).collection('recipients').get()
        let campaigns_docs = await db.collection('batches').doc(batch.id).collection('campaigns').get()

        expect(recipients_docs.docs.length).toEqual(2)
        expect(campaigns_docs.docs.length).toEqual(2)

      } else if (batch.id === new_batch_2_id) { // Dosen't touh existing shipped batch
        expect(batch.printer_doc).toEqual(db.collection('printers').doc(mock_printer_2_id))
        expect(batch.status).toEqual(constants.BATCH_STATUS.shipped)
        expect(batch.count).toEqual(1)
      } else { // Makes a new batch
        expect(batch.printer_doc).toEqual(db.collection('printers').doc(mock_printer_1_id))
        expect(batch.status).toEqual(constants.BATCH_STATUS.pending)
        expect(batch.count).toEqual(2)

        let recipients_docs = await db.collection('batches').doc(batch.id).collection('recipients').get()
        let campaigns_docs = await db.collection('batches').doc(batch.id).collection('campaigns').get()

        expect(recipients_docs.docs.length).toEqual(2)
        expect(campaigns_docs.docs.length).toEqual(2)
      }
    }
  })
})

describe('ship_batch', () => {
  const mock_campaign_id = '12'
  const mock_batch_id = '1'
  const mock_batch = {
    id: mock_batch_id,
    status: constants.BATCH_STATUS.shipped,
    printer_doc: db.collection('printers').doc(mock_printer_1_id),
    zip: false
  }

  const mock_batch_with_zip = {
    id: mock_batch_id,
    status: constants.BATCH_STATUS.shipped,
    printer_doc: db.collection('printers').doc(mock_printer_1_id),
    zip: true
  }

  beforeEach(async () => {
    await firebase.clearFirestoreData({ projectId })

    // Add printer
    await db.collection('printers').doc(mock_printer_1_id).set(mock_printer_1)

    // Add campaign
    const mock_campaign = {product: constants.PRODUCT.full_service, stationary_required: false, envelope: {required: constants.ENVELOPE_REQUIRED.no}}
    await db.collection('campaigns').doc(mock_campaign_1_id).set(mock_campaign)

    // Add recipients to campaigns
    await db.collection('campaigns').doc(mock_campaign_1_id).collection('recipients').doc(mock_recipient_1_id).set(mock_recipient_1)
    await db.collection('campaigns').doc(mock_campaign_1_id).collection('recipients').doc(mock_recipient_2_id).set(mock_recipient_2)

    await db.collection('batches').doc(mock_batch.id).set(mock_batch)
    await db.collection('batches').doc(mock_batch.id).collection('recipients').doc(mock_recipient_1_id).set({recipient_doc: db.collection('campaigns').doc(mock_campaign_1_id).collection('recipients').doc(mock_recipient_1_id), campaign: mock_campaign_1_id})
    await db.collection('batches').doc(mock_batch.id).collection('recipients').doc(mock_recipient_2_id).set({recipient_doc: db.collection('campaigns').doc(mock_campaign_1_id).collection('recipients').doc(mock_recipient_2_id), campaign: mock_campaign_1_id})
  })

  it('Updates batch and recipient status', async () => {
    pricing.printer_cost = jest.fn().mockReturnValue({code_error: undefined, code_output: 10})

    let result = await batches.ship_batch({
      before: {
        data: () => ({status: constants.BATCH_STATUS.pending})
      },
      after: {
        data: () => (mock_batch),
        id: mock_batch_id,
        ref: db.collection('batches').doc(mock_batch.id)
      }
    })

    let batch_doc = await db.collection('batches').doc(mock_batch_id).get()
    let batch = batch_doc.data()

    expect(batch.cost).toEqual(10)
    expect(batch.warnings).toEqual(['Unserviceable country found: US'])

    let recipient_docs = await db.collection('campaigns').doc(mock_campaign_1_id).collection('recipinets').get()
    for (let recipient_doc of recipient_docs.docs) {
      let recipient = recipient_doc.data()

      expect(recipient.status).toEqual(constants.RECIPIENT_STATUS.shipped)
      expect(recipient.batch.id).toEqual(mock_batch_id)
      expect(recipient.printer_doc).toEqual(db.collection('printers').doc(mock_printer_1_id))
    }
  })

  it('Sends emails', async () => {
    pricing.printer_cost = jest.fn().mockReturnValue({code_error: undefined, code_output: 10})
    utils.send_email = jest.fn()

    let result = await batches.ship_batch({
      before: {
        data: () => ({status: constants.BATCH_STATUS.pending})
      },
      after: {
        data: () => (mock_batch),
        id: mock_batch_id,
        ref: db.collection('batches').doc(mock_batch.id)
      }
    })
    expect(utils.send_email).toHaveBeenCalledTimes(0)

    result = await batches.ship_batch({
      before: {
        data: () => ({status: constants.BATCH_STATUS.pending})
      },
      after: {
        data: () => (mock_batch_with_zip),
        id: mock_batch_id,
        ref: db.collection('batches').doc(mock_batch.id)
      }
    })
    expect(utils.send_email).toHaveBeenCalledTimes(0)

    await db.collection('printers').doc(mock_printer_1_id).update({email: 'example@example.com'})
    result = await batches.ship_batch({
      before: {
        data: () => ({status: constants.BATCH_STATUS.pending})
      },
      after: {
        data: () => (mock_batch_with_zip),
        id: mock_batch_id,
        ref: db.collection('batches').doc(mock_batch.id)
      }
    })
    expect(utils.send_email).toHaveBeenCalledTimes(1)
  })

  it('Only ships if status changes', async () => {
    let result = await batches.ship_batch({
      before: {
        data: () => ({status: constants.BATCH_STATUS.pending})
      },
      after: {
        data: () => ({status: constants.RECIPIENT_STATUS.pending})
      }
    })
    expect(result).toEqual(false)

    result = await batches.ship_batch({
      before: {
        data: () => ({status: constants.BATCH_STATUS.shipped})
      },
      after: {
        data: () => ({status: constants.RECIPIENT_STATUS.shipped})
      }
    })
    expect(result).toEqual(false)
  })
})

firebase_test.cleanup()
