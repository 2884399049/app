const projectId = 'route-v1-index-test'
process.env.GCLOUD_PROJECT = projectId
process.env.FIRESTORE_EMULATOR_HOST = 'localhost:8080'

const firebase_test = require('firebase-functions-test')()
const firebase = require('@firebase/testing')

firebase_test.mockConfig({ slack: {api_key: '', channels: {}}, stripe: { key: '' }, sendgrid: {apikey: ''}, hubspot: {api_key: '', enabled: false}, firestore: {apikey: '', authdomain: '', projectid: projectId, storagebucket: '123'} });

const admin = require('firebase-admin')

admin.initializeApp({ projectId })

const db = admin.firestore()

const constants = require('../../../shared/constants')
const api_v1_functions = require('functions/routes/v1/functions')
const api = require('functions/routes/index')

const supertest = require('supertest')

// firestore emulator cannot handle timestamps, so mock them out
const admin_mock = require('firebase-admin')
admin_mock.firestore.Timestamp.now = jest.fn().mockReturnValue('date mock')

async function post_returns_error(request, path, data, error_code) {
  let actual = await request.post(path)
    .type('form')
    .send(data)
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json')

  let { status, body } = actual

  expect(status).toEqual(403)
  expect(body.error_code).toEqual(error_code)
}

async function post_returns_success(request, path, data) {
  let actual = await request.post(path)
    .type('form')
    .send(data)
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json')

  let { status, body } = actual

  expect(status).toEqual(200)
  expect(body.success).toEqual(true)
}

async function get_returns_error(request, path, query, error_code) {
  let actual = await request.get(path).query(query)

  let { status, body } = actual

  expect(String(status)).toMatch(/^403|404$/)
  expect(body.error_code).toEqual(error_code)
}

async function get_returns_success(request, path, query) {
  let actual = await request.get(path).query(query)

  let { status, body } = actual

  // expect(status).toEqual(200)
  // expect(body.success).toEqual(true)

  return body
}

const mock_user_1 = {
  id: '1',
  api_key: '1',
  currency: 'gbp'
}

const mock_user_2 = {
  id: '2',
  api_key: '2',
  currency: 'gbp'
}

const mock_campaign_1 = {
  id: '1',
  name: 'campaign 1',
  uid: mock_user_1.id
}

const mock_campaign_2 = {
  id: '2',
  name: 'campaign 2',
  uid: mock_user_2.id
}

const mock_create_campaign = {
  paper_size: 'A5',
  handwriting_style: 'Plath',
  text: 'This is a unit test',
  recipients: [
    {
      'address line 1': 'testaddress', 
      'address line 2': 'testaddress 2', 
      'address line 3':'testaddress 4', 
      'city':'LA',
      'country':'United Kingdom',
      'first name':'Wim',
      'last name':'Wimson',
      'title':'Mr',
      'zip/postal code':'BS1 1AS',
      'company':'testcompany'
    }
  ]
}

describe('POST /auth', () => {
  let request

  beforeEach(async () => {
    request = supertest(api)
    await firebase.clearFirestoreData({ projectId })

    await db.collection('users').doc(mock_user_1.id).set(mock_user_1)
  })

  it.skip('Authenticates', async () => {
    // TODO note asking for API key for some reason?
    await post_returns_error(request, '/v1/auth', {email: 'a@b.com', password: '123'}, constants.ERROR.field_required)
  })
})

describe('GET /campaigns', () => {
  let request

  beforeEach(async () => {
    request = supertest(api)
    await firebase.clearFirestoreData({ projectId })

    await db.collection('users').doc(mock_user_1.id).set(mock_user_1)
    await db.collection('users').doc(mock_user_2.id).set(mock_user_2)
    await db.collection('campaigns').doc(mock_campaign_1.id).set(mock_campaign_1)
    await db.collection('campaigns').doc(mock_campaign_2.id).set(mock_campaign_2)
  })

  it('throws error when invalid API Key', async () => {
    await get_returns_error(request, '/v1/campaigns', {api_key: 'invalid_api_key'}, constants.ERROR.unauthorised)
  })

  it('Lists all campaigns', async () => {
    let data = await get_returns_success(request, '/v1/campaigns', {api_key: mock_user_1.api_key})
    expect(data).toEqual([mock_campaign_1])
  })

  it.skip('get a specific campaign', async () => {
    // TODO Requires updating
    let data = await get_returns_success(request, `/v1/campaign/${mock_campaign_1.id}`, {api_key: mock_user_1.api_key})
    expect(data).toEqual({a: 1})
    expect(data.name).toEqual(mock_campaign_1.name)
  })

  it('throws an error when geting another users campaign', async () => {
    await get_returns_error(request, `/v1/campaign/${mock_campaign_2.id}`, {api_key: mock_user_1.api_key}, constants.ERROR.unauthorised)
  })

  it('creates a new campaign', async () => {
    // TODO
    await post_returns_success(request, '/v1/campaign', {api_key: mock_user_1.api_key, mock_create_campaign})
  })

  it('throws error when not adding recipient to campaign', async () => {
    let data = {
      api_key: mock_user_1.api_key,
      recipients: []
    }

    let actual = await request.put(`/v1/campaign/${mock_campaign_1.id}`).type('form').send(data).set('Content-Type', 'application/json').set('Accept', 'application/json')
    let { status, body } = actual

    expect(status).toEqual(403)
    expect(body.error_code).toEqual(constants.ERROR.field_required)
  })

  it('throws error when adding recipients to another users campaign', async () => {
    let recipient = {a: 1}

    let data = {
      api_key: mock_user_1.api_key,
      recipients: [recipient]
    }

    let actual = await request.put(`/v1/campaign/${mock_campaign_2.id}`).type('form').send(data).set('Content-Type', 'application/json').set('Accept', 'application/json')
    let { status, body } = actual

    expect(status).toEqual(403)
    expect(body.error_code).toEqual(constants.ERROR.unauthorised)
  })

  it('throws error when bad recipient keys', async () => {
    let recipient = {x: 1}

    let data = {
      api_key: mock_user_1.api_key,
      recipients: [recipient]
    }

    let actual = await request.put(`/v1/campaign/${mock_campaign_1.id}`).type('form').send(data).set('Content-Type', 'application/json').set('Accept', 'application/json')
    let { status, body } = actual

    expect(status).toEqual(403)
    expect(body.error_code).toEqual(constants.ERROR.invalid_key)
  })

  it('throws error when missing recipient keys', async () => {
    let recipient = {city: '123'}

    let data = {
      api_key: mock_user_1.api_key,
      recipients: [recipient]
    }

    let actual = await request.put(`/v1/campaign/${mock_campaign_1.id}`).type('form').send(data).set('Content-Type', 'application/json').set('Accept', 'application/json')
    let { status, body } = actual

    expect(status).toEqual(403)
    expect(body.error_code).toEqual(constants.ERROR.invalid_key)
  })

  it('throws error bad text', async () => {
    let recipient = {}
    recipient['last name'] = '£Robinson'

    let data = {
      api_key: mock_user_1.api_key,
      recipients: [recipient]
    }

    let actual = await request.put(`/v1/campaign/${mock_campaign_1.id}`).type('form').send(data).set('Content-Type', 'application/json').set('Accept', 'application/json')
    let { status, body } = actual

    expect(status).toEqual(403)
    expect(body.error_code).toEqual(constants.ERROR.character_not_supported)
  })

  it('adds recipients to campaign', async () => {
    let recipient = {}
    recipient['last name'] = 'Robinson'

    let data = {
      api_key: mock_user_1.api_key,
      recipients: [recipient]
    }

    let actual = await request.put(`/v1/campaign/${mock_campaign_1.id}`).type('form').send(data).set('Content-Type', 'application/json').set('Accept', 'application/json')
    let { status, body } = actual

    expect(status).toEqual(200)
    expect(body.success).toEqual(true)

    let recipient_docs = await db.collection('campaigns').doc(mock_campaign_1.id).collection('recipients').get()
    expect(recipient_docs.docs.length).toEqual(1)
    expect(recipient_docs.docs[0].data()['last name']).toEqual(recipient['last name'])
  })

  it.skip('deletes a campaign', async () => {
    // TODO
  })

  it.skip('GET price for a campaign', async () => {
    // TODO
  })
})

firebase_test.cleanup()
