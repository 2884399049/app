const { validateCampaignId } = require('../modules')
const HTTP_STATUS = require('http-status')

module.exports = async (req, res, next) => {
  const campaign_id = req.body.campaign_id

  if (req.body.testing) {
    return res.status(HTTP_STATUS.BAD_REQUEST).json({
      success: false,
      error_message: 'Testing must be in the url query not body',
      error_code: 'error/bad-request'
    })
  }

  if (req.query.testing === "0" || req.query.testing === "false" || req.query.testing === 0) {
    req.locals.testing = false
  } else if (req.query.testing) {
    console.log(`API Testing mode`)
    req.locals.testing = true
  }

  if (!campaign_id) {
    console.log('There is no campaign id')
    return next()
  }

  const validateCampaignIdRes = await validateCampaignId(req.locals.uid, campaign_id)

  if (!validateCampaignIdRes.success) {
    return res.status(HTTP_STATUS.BAD_REQUEST).json(validateCampaignIdRes)
  }

  console.log('We are done with validating the request')
  return next()
}
