const HTTP_STATUS = require('http-status')

module.exports = (req, res, next) => {
  return res.status(HTTP_STATUS.NOT_FOUND).send({
    success: false,
    error_message: `Route not found`
  })
}
