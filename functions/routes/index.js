const functions = require('firebase-functions')
const configs = functions.config()

const express = require('express')
const swaggerUi = require('swagger-ui-express')
const swaggerJSDoc = require('swagger-jsdoc')
const cors = require('cors')

const bodyParser = require('body-parser')

const app = express()
const router = express.Router()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(cors())

const routeNotFoundHandler = require('../middlewares/routeNotFoundHandler')

const swaggerOptions = {
  customSiteTitle: 'Scribeless API',
  customCss: '.topbar, .scheme-container { display: none };'
}

const swaggerDocument = swaggerJSDoc({
  definition: {
    info: {
      title: 'Scribeless API',
      version: '1.0.0',
      description: 'Scribeless API'
    },
    produces: ['application/json'],
    consumes: ['application/json'],
    basePath: '/api/v1/' // NOTE add hc-application-interface-dev/us-central1/ for local DEV
  },
  apis: [`${process.cwd()}/routes/v1/index.js`]
})

router.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, swaggerOptions))

// All version 1 Public API's goes here
router.use('/v1', require('./v1'))

app.use(router)
app.use(routeNotFoundHandler)

module.exports = app
