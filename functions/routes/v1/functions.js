const admin = require('firebase-admin')
const functions = require('firebase-functions')
const moment = require('moment')
const sharp = require('sharp')
const download = require('download')
const TextToSVG = require('text-to-svg')
const fs = require('fs')
const utils = require('../../lib/utils')
const validator = require('validator')
const country_data = require('country-data')

const configs = functions.config()
const bucket = admin.storage().bucket(configs.firestore.storagebucket)

const pricing = require('../../lib/pricing')
const constants = require('../../shared/constants')
const firebase_utils = require('../../lib/firebase-utils')
const text_utils = require('../../lib/text-utils')
const layout_utils = require('../../lib/layout-utils')
const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(configs.sendgrid.apikey)

const db = admin.firestore()
const auth = admin.auth();

async function mark_campaign_as_deleted (campaign, updated_by) {
  let recipient_docs = await db.collection('campaigns').doc(campaign.id).collection('recipients').get()

  let proms = []
  for (let recipient_doc of recipient_docs.docs) {
    proms.push(recipient_doc.ref.update({status: constants.RECIPIENT_STATUS.deleted, updated: new Date(), updated_by: updated_by}))
  }

  return await Promise.all(proms)
}

async function toggle_recipient_deleted (recipient, campaign_id) {
  let campaign_doc = await db.collection('campaigns').doc(campaign_id).get()
  let campaign = campaign_doc.data()
  campaign['id'] = campaign_id

  let user_profile = await utils.get_user_profile(campaign.uid)

  // Ignore sample campaigns
  if (campaign.is_sample) {
    return
  }

  let stationary
  if (campaign.stationary_required) {
    let stationary_doc = await db.collection('stationary').doc(campaign.stationary).get()
    stationary = stationary_doc.data()
    stationary['id'] = campaign.stationary
  }

  // Negate goods from usage
  let multiplier = 1
  let goods = pricing.campaign_to_goods(campaign, [recipient], stationary)
  if (recipient.status === constants.RECIPIENT_STATUS.deleted || recipient.testing) {
    multiplier = -1
    for (let good_name in goods) {
      goods[good_name] = multiplier * goods[good_name]
    }
  }

  return pricing.add_usage_record(user_profile, goods, multiplier, undefined, constants.RECIPIENT_SOURCE.system)
}

async function add_recipients (campaign_id, recipients, uid, testing, source) {
  console.log(`[${campaign_id}] add_recipients`, [campaign_id, recipients, uid, testing, source])

  if (!source) {
    source = constants.RECIPIENT_SOURCE.unknown
  }

  for (let recipient of recipients) {
    console.log(`[campaign: ${campaign_id}], recipient:`, recipient)
  }

  // Check call is valid
  for (let recipient of recipients) {
    let result = validate_recipient(campaign_id, recipient)
    if (!result.success) {
      console.log(`Invalid recipient`, [campaign_id, recipient, result])
      return result
    }
  }

  var campaign_doc = await db.collection('campaigns').doc(campaign_id).get()
  var campaign = campaign_doc.data()

  console.log(`[${campaign_id}] Campaign`, campaign)

  var user = await utils.get_user_profile(uid)

  console.log(`[${user.id}] User`, user)

  let recipients_count = campaign.recipients_count || 0

  if (campaign.is_sample && (recipients_count > 0 || recipients.length > 1)) {
    console.log(`[${campaign_id}] Invalid campaign`)
    return {success: false, error_message: 'You cannot add recipients to a sample campaign', error_code: 'error/sample-campaign'}
  }

  let stationary
  if (campaign.stationary_required) {
    let stationary_doc = await db.collection('stationary').doc(campaign.stationary).get()
    stationary = stationary_doc.data()
    console.log(`[${campaign_id}] stationary ${campaign.stationary}`, stationary)
  }

  // Upload recipients
  var upload_recipients = []
  var recipient_ids = []
  for (let recipient of recipients) {
    if (recipient.index !== 0 && !recipient.index) {
      recipient.index = recipients_count // TODO race condition if this function called multiple tiems
      recipients_count++
    }

    recipient.source = source

    if (testing || campaign.testing) {
      recipient.testing = true
    }

    var recipient_doc = db.collection('campaigns').doc(campaign_id).collection('recipients').doc()
    console.log(`[${campaign_id}] Recipient ${recipient_doc.id}`, recipient)

    recipient_ids.push(recipient_doc.id)

    upload_recipients.push(recipient_doc.set({
      ...recipient,
      created: admin.firestore.Timestamp.now(),
      updated: admin.firestore.Timestamp.now(),
      'status': constants.RECIPIENT_STATUS.pending
    }, {merge: true}))
  }

  let goods = pricing.campaign_to_goods(campaign, recipients, stationary)
  console.log('Goods', goods)

  console.log(`[${campaign_id}] Uploading ${recipients.length} recipients...`)
  await Promise.all(upload_recipients)

  console.log(`[${campaign_id}] Calculating price...`)
  let price_table = await pricing.build_price_table(user)
  let cost = await pricing.caluclate_price(price_table, user, goods)

  if (!campaign.is_sample && !(campaign.testing || testing)) {
    console.log(`[${campaign_id}] Adding usage...`)
    await pricing.add_usage_record(user, goods, recipients.length, undefined, source)
  }

  let out = {
    success: true,
    cost: cost.total_with_tax,
    status: constants.CAMPAIGN_STATUS.pending,
    id: campaign_id,
    currency: cost.currency
  }

  if (campaign.stationary_required) {
    out['stationary_thumbnail'] = stationary.thumbnail
  }

  console.log(`[${campaign_id}] Complete`, out)
  return out
}

async function campaign_get (campaign_id, uid) {
  if (!campaign_id) {
    return {success: false, error_message: `The campaign_id is required`, error_code: 'error/field-required'}
  }

  let user_profile = await utils.get_user_profile(uid)

  var campaign_doc = await db.collection('campaigns').doc(campaign_id).get()
  var campaign = campaign_doc.data()
  campaign.id = campaign_doc.id

  console.log(`[${campaign_id}] Campaign GET`, campaign)

  // Get recipients
  let recipients = []
  let recipient_docs = await db.collection('campaigns').doc(campaign_id).collection('recipients').get()
  for (let recipient_doc of recipient_docs.docs) {
    let recipient = recipient_doc.data()
    recipient.id = recipient_doc.id

    recipients.push(recipient)
  }

  // Get Stationary
  let stationary
  if (campaign.stationary_required) {
    let stationary_doc = await db.collection('stationary').doc(campaign.stationary).get()
    stationary = stationary_doc.data()
    stationary.id = stationary_doc.id
  }

  // Calculate price
  let goods = pricing.campaign_to_goods(campaign, recipients, stationary)
  let price_table = await pricing.build_price_table(user_profile)
  let cost = await pricing.caluclate_price(price_table, user_profile, goods)

  return {
    success: true,
    status: campaign.status,
    cost: cost.total_with_tax,
    currency: user_profile.currency
  }
}

async function campaign_delete (campaign_id, uid, testing) {
  if (!campaign_id) {
    return {success: false, error_message: `The campaign_id is required`, error_code: 'error/field-required'}
  }

  var campaign_doc = await db.collection('campaigns').doc(campaign_id).get()
  var campaign = campaign_doc.data()

  console.log(`[${campaign_id}] Campaign DELETE`, campaign)

  if ([constants.CAMPAIGN_STATUS.pending, constants.CAMPAIGN_STATUS.in_progress, constants.CAMPAIGN_STATUS.ready_to_ship].indexOf(campaign.status) === -1) {
    let message = `Campaign status is "${campaign.status}" can only remove when status is "${constants.CAMPAIGN_STATUS.pending}", "${constants.CAMPAIGN_STATUS.in_progress}" or "${constants.CAMPAIGN_STATUS.ready_to_ship}"`
    console.log(message)
    return {
      success: false,
      error_message: message,
      error_code: 'error/invalid-status'
    }
  }

  if (testing && !campaign.testing) {
    console.log(`Campaign is not a testing campaign and testing is set, will not delete`)
    return {
      success: false,
      error_message: `Campaign is not a testing campaign and testing is set, will not delete`,
      error_code: 'error/invalid-status'
    }
  }
  await db.collection('campaigns').doc(campaign_id).update({"status":constants.CAMPAIGN_STATUS.deleted})

  return { success: true }
}

function validate_recipient (campaign_id, recipient) {
  let allowed_keys = ['index', 'address line 1', 'address line 2', 'address line 3', 'city', 'country', 'department', 'last name', 'first name', 'state/region', 'region', 'title', 'zip/postal code', 'postal_code', 'company', 'custom 1', 'custom 2', 'custom 3', 'custom 4', 'custom 5', 'custom 6', 'full address', 'fallback country']

  for (var key of Object.keys(recipient)) {
    if (allowed_keys.indexOf(key) === -1) {
      console.log(`[${campaign_id}] Key ${key} is not in the allowed keys`)

      return {
        success: false,
        error_message: `Key ${key} is not in the allowed keys`,
        error_code: 'error/invalid-key'
      }
    }
  }

  if (!recipient['last name'] || recipient['last name'] < 1) {
    return {
      success: false,
      error_message: `last name is required`,
      error_code: 'error/invalid-key'
    }
  }

  // If has an address
  if (recipient['address line 1'] || recipient['country'] || recipient['city'] || recipient['address line 2'] || recipient['address line 3'] || (recipient['state/region'] || recipient['region']) || (recipient['zip/postal code'] || recipient['postal_code'])) {
    if (!recipient['address line 1'] || recipient['address line 1'] < 1) {
      return {
        success: false,
        error_message: `address line 1 is required`,
        error_code: 'error/invalid-key'
      }
    }

    if ((!recipient['country'] || recipient['country'].length < 2) && recipient['fallback country']) {
      recipient['country'] = recipient['fallback country']
    }

    if (recipient['country']) {
      let match = country_data.lookup.countries({alpha2: recipient['country'].toUpperCase()})[0]
      if (!match) match = country_data.lookup.countries({alpha3: recipient['country'].toUpperCase()})[0]
      if (!match) match = country_data.lookup.countries({name: recipient['country']})[0]

      if (match) {
        recipient['country'] = match.alpha2
      } else {
        recipient['country'] = undefined
      }
    }

    if (!recipient['country'] || recipient['country'].length !== 2) {
      return {
        success: false,
        error_message: `Couldn't find country: "${recipient['country']}"`,
        error_code: 'error/invalid-key'
      }
    }

    try {
      if (!recipient['zip/postal code'] && !recipient['postal_code']) { //  || !validator.isPostalCode(recipient['zip/postal code'], recipient['country']
        return {
          success: false,
          error_message: `Couldn't parse postal_code: "${recipient['zip/postal code'] || recipient['postal_code']}"`,
          error_code: 'error/invalid-key'
        }
      }
    } catch (error) {
      console.warn(error)
      return {
        success: false,
        error_message: `Couldn't parse postal code: "${recipient['zip/postal code'] || recipient['postal_code']}"`,
        error_code: 'error/invalid-key'
      }
    }
  }

  let out = text_utils.check_bad_text(Object.values(recipient).toString())
  if (!out[0]) {
    return {success: false, error_message: out[2], error_code: 'error/character-not-supported'} // TODO
  }

  return {
    success: true
  }
}

async function create_stationery (options) {
  var stationary_doc = db.collection('stationary').doc()

  let stationary = {...options}
  stationary.show_bleed = true
  stationary.add_bleed = true
  stationary.flipped = false
  stationary.front_type = 'editor'
  stationary.rendered_text = null
  stationary.handwriting_colour = 'blue'
  stationary.footer_font = options.footer_font || 'Josefin Sans'
  stationary.header_font = options.header_font || 'Josefin Sans'
  stationary.header_image_align = 'center'
  stationary.header_text_size = 8
  stationary.footer_text_align = 'center'
  stationary.footer_text_size = 5
  stationary.back_image = null
  stationary.front_image = null
  stationary.source = 'interface'

  let stationary_html = `
  <div class='paper-preview' style="position: absolute; box-sizing: border-box; width: WIDTH; height: HEIGHT;">
    <link id='font-loader' rel="stylesheet" href="https://fonts.googleapis.com/css?family=${stationary.footer_font.replace(/ /g, '+')}|${stationary.header_font.replace(/ /g, '+')}=swap"/>

    <div id='vertical-line'></div>
    <div id='horizontal-line'></div>

    <div class='paper-header draggable hover-highlight' style="margin: auto; width: 50mm; height: 25mm; color: #333; font-family: '${stationary.footer_font}', cursive; top: 6mm !important; position: relative; overflow: hidden;">
      <p id="header-text" style="display: ${stationary.header_type === constants.HEADER_TYPE.text ? 'block' : 'none'}; margin-top: 0px; font-size: ${stationary.header_text_size || 8}mm; text-align: center; width: 100%;">${stationary.header_text || ''}</p>
      <img id="header-image" class='paper-logo' style="display: ${stationary.header_type === constants.HEADER_TYPE.text ? 'none' : 'block'}; margin: 0 auto; width: 100%;" ${stationary.header_image ? 'src="' + stationary.header_image + '"' : ''}/>
    </div>

    <div id="background-image-container" class='paper-element' style='position: absolute; width: WIDTH; height: HEIGHT'>
      <img id="background-image" width="100%" />
    </div>

    <div id="handwriting-loading-container" class='paper-text text-center' style="box-sizing: border-box; display: none; padding-left: 6mm; padding-right: 6mm; width: 100%; height: 20mm; z-index: 1; position: absolute; background-size: 56.57px 56.57px; top: HANDWRITING_MARGIN_HEIGHT;">
      <span class='fas fa-spinner fa-pulse mr-2 text_loading_spinner'></span>
    </div>

    <div id="handwriting-image-container" class='paper-text text-center' style="box-sizing: border-box; padding-left: 6mm; padding-right: 6mm; width: 100%; height: HANDWRITING_HEIGHT; overflow: hidden; z-index: 0; position: absolute; background-size: 56.57px 56.57px; top: HANDWRITING_MARGIN_HEIGHT;">
      <img id="handwriting-image" width="100%" />
    </div>

    <div class='paper-footer' style="width: 100%; color: #333; font-family: '${stationary.footer_font}'; position: absolute !important; bottom: 4mm;">
      <p id="footer-text" style="font-size: ${stationary.footer_text_size || 5}mm; text-align: ${stationary.footer_text_align || 'center'}; white-space: pre-line; margin: 0px; padding-right: 4mm; padding-left: 4mm;">${stationary.footer_text || ''}</p>
    </div>
  </div>
  `
  // <div style="height: 100%; width: 100%; position: absolute; pointer-events: none; background: red; opacity: 0.25;" id=""></div>
  // <div style="height: calc(100% - 8mm); width: calc(100% - 8mm); position: absolute; top: 4mm; left: 4mm; background: white; pointer-events: none; "></div>

  let bleed = `${constants.BLEED}mm`
  let bleed_mark_margin = `${constants.BLEED_MARK_MARGIN}mm`

  // <div style="width: 100%; height: 100%; position: absolute !important; pointer-events: none;">
  //   <img style="display: block; margin: auto; height: 100%; width: 100%; object-fit: contain;" src="https://firebasestorage.googleapis.com/v0/b/hc-application-interface-dev.appspot.com/o/paper%2FA5_crop_marks.png?alt=media&token=daf72d4c-3031-4000-97c4-1019bc94b448"/>
  // </div>

  let stationary_back_html = `
  <div class='paper-preview back-preview' style="position: absolute;; width: WIDTH; height: HEIGHT;">
    <div id='vertical-line'></div>
    <div id='horizontal-line'></div>

    <div id="bleed-marks">
      <div style="box-sizing: border-box; height: ${bleed}; width: ${bleed}; position: absolute; top: 0px; left: 0px; pointer-events: none; border-style: solid; border-color: black; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 0px; border-top-width: 0px;"></div>
      <div style="height: ${bleed_mark_margin}; width: ${bleed_mark_margin}; position: absolute; top: calc(${bleed} - ${bleed_mark_margin}); left: calc(${bleed} - ${bleed_mark_margin}); pointer-events: none; background: white;"></div>

      <div style="box-sizing: border-box; height: ${bleed}; width: ${bleed}; position: absolute; bottom: 0px; left: 0px; pointer-events: none; border-style: solid; border-color: black; border-right-width: 1px; border-top-width: 1px; border-left-width: 0px; border-bottom-width: 0px;"></div>
      <div style="height: ${bleed_mark_margin}; width: ${bleed_mark_margin}; position: absolute; bottom: calc(${bleed} - ${bleed_mark_margin}); left: calc(${bleed} - ${bleed_mark_margin}); pointer-events: none; background: white;"></div>

      <div style="box-sizing: border-box; height: ${bleed}; width: ${bleed}; position: absolute; top: 0px; right: 0px; pointer-events: none; border-style: solid; border-color: black; border-left-width: 1px; border-bottom-width: 1px; border-right-width: 0px; border-top-width: 0px;"></div>
      <div style="height: ${bleed_mark_margin}; width: ${bleed_mark_margin}; position: absolute; top: calc(${bleed} - ${bleed_mark_margin}); right: calc(${bleed} - ${bleed_mark_margin}); pointer-events: none; background: white;"></div>

      <div style="box-sizing: border-box; height: ${bleed}; width: ${bleed}; position: absolute; bottom: 0px; right: 0px; pointer-events: none; border-style: solid; border-color: black; border-left-width: 1px; border-top-width: 1px; border-right-width: 0px; border-bottom-width: 0px;"></div>
      <div style="height: ${bleed_mark_margin}; width: ${bleed_mark_margin}; position: absolute; bottom: calc(${bleed} - ${bleed_mark_margin}); right: calc(${bleed} - ${bleed_mark_margin}); pointer-events: none; background: white;"></div>
    </div>

    <div id="back-image-container" class='draggable hover-highlight' style="width: 100%; height: 100%; position: absolute !important">
      <img id="back-image" class="back_image" style="display: block; margin: auto; height: 100%; width: 100%; object-fit: contain; z-index: 1" />
    </div>

  </div>`

  let text_y = 30 // TOP MARGIN
  if (options.header_type !== 'Logo') {
    if (options.header_text && options.header_text.trim().length > 0) {
      text_y = 18 + 6
    } else {
      text_y = 10 + 6
    }
  }

  let footer_margin = 10

  stationary_html = stationary_html.replace(/WIDTH/g, options.paper.width + 'mm')
  stationary_html = stationary_html.replace(/HANDWRITING_HEIGHT/g, `calc(100% - ${text_y + footer_margin}mm)`)
  stationary_html = stationary_html.replace(/HANDWRITING_MARGIN_HEIGHT/g, text_y + 'mm')
  stationary_html = stationary_html.replace(/HEIGHT/g, options.paper.height + 'mm')

  stationary_back_html = stationary_back_html.replace(/WIDTH/g, options.paper.width + 'mm')
  stationary_back_html = stationary_back_html.replace(/HEIGHT/g, options.paper.height + 'mm')

  stationary.html = stationary_html
  stationary.back_html = stationary_back_html

  console.log(`Setting stationary ${stationary_doc.id}:`, stationary)
  await stationary_doc.set(stationary)

  return stationary_doc.id
}

async function campaign_add (data, uid, testing, price_only) {
  var user_profile = await utils.get_user_profile(uid)

  // Get payment method
  if (!user_profile || !user_profile.billing_enabled) {
    return {success: false, error_message: 'Please add a default payment method to account', error_code: 'error/payment-method-required'}
  }

  var min_due_date = moment().add(constants.PRODUCTION_DAYS, 'days')
  var due_date = min_due_date
  if (data.due_date) {
    due_date = moment(data.due_date, 'DD/MM/YYYY')
  }

  if (!due_date.isValid()) {
    return {success: false, error_message: 'Invalid due_date please use DD/MM/YYYY format', error_code: 'error/invalid-date'}
  }

  if (due_date < min_due_date) {
    return {success: false, error_message: `Due date must be at least ${constants.PRODUCTION_DAYS} days in the future`, error_code: 'error/invalid-date'}
  }

  if (Object.values(constants.HANDWRITING_STYLE).indexOf(data.handwriting_style) === -1) {
    return {success: false, error_message: `Invalid handwriting style, must be one of ${Object.values(constants.HANDWRITING_STYLE)}`, error_code: 'error/invalid-handwriting-style'}
  }

  var handwriting_size = Object.values(constants.HANDWRITING_SIZE)[1]
  if (data.handwriting_size) {
    handwriting_size = data.handwriting_size
  }
  if (Object.values(constants.HANDWRITING_SIZE).indexOf(handwriting_size) === -1) {
    return {success: false, error_message: `Invalid handwriting style, must be one of ${Object.values(constants.HANDWRITING_SIZE)}`, error_code: 'error/invalid-handwriting-size'} // TODO
  }

  let footer_font = constants.FONTS[data.footer_font || Object.keys(constants.FONTS)[0]]
  if (!footer_font) {
    return {success: false, error_message: `Invalid footer font, must be one of ${Object.keys(constants.FONTS)}`, error_code: 'error/invalid-font'} // TODO
  }

  let header_font = constants.FONTS[data.header_font || Object.keys(constants.FONTS)[1]]
  if (!header_font) {
    return {success: false, error_message: `Invalid header font, must be one of ${Object.keys(constants.FONTS)}`, error_code: 'error/invalid-font'} // TODO
  }

  if (!constants.PAPER_SIZES[data.paper_size]) {
    return {success: false, error_message: `Invalid paper size, must be one of ${Object.keys(constants.PAPER_SIZES)}`, error_code: 'error/invalid-paper-size'}
  }

  let product = constants.PRODUCT.full_service // Default
  if (data.product) {
    product = data.product
  }
  if (Object.values(constants.PRODUCT).indexOf(product) === -1) {
    return {success: false, error_message: `Invalid product, must be one of ${Object.values(constants.PRODUCT)}`, error_code: 'error/invalid-product'}
  }

  if (!data.text || data.text.length < 1) {
    return {success: false, error_message: `The text field is required`, error_code: 'error/field-required'}
  }

  let out = text_utils.check_bad_text(data.text)
  if (!out[0]) {
    return {success: false, error_message: out[2], error_code: 'error/character-not-supported'} // TODO
  }
  let text = out[1]
  if (!data.recipients || data.recipients.length < 1) {
    return {success: false, error_message: `The recipients field is required`, error_code: 'error/field-required'}
  }

  var campaign_doc = db.collection('campaigns').doc()

  for (let recipient of data.recipients) {
    let result = validate_recipient(campaign_doc.id, recipient)
    if (!result.success) {
      return result
    }
  }

  let stationary = {
    uid: uid,
    paper: constants.PAPER_SIZES[data.paper_size],
    handwriting_style: data.handwriting_style,
    handwriting_size: handwriting_size,
    footer_font: footer_font,
    header_font: header_font,
    header_type: data.header_type || constants.HEADER_TYPE.text,
    footer_text: data.footer_text || null,
    header_text: data.header_text || null,
    header_image: data.header_image || null,
    thumbnail: null,
    background: null,
    source: 'api',
    testing: testing || false
  }

  let campaign = {
    stationary_required: true,
    recipients_count: 0,
    status: constants.CAMPAIGN_STATUS.pending,
    created: admin.firestore.Timestamp.now(),
    last_updated: admin.firestore.Timestamp.now(),
    title: data.title || `API Campaign ${moment().format('ll')}`,
    is_sample: false,
    product: data.product || constants.PRODUCT.full_service,
    uid: uid,
    text: text,
    envelope: {
      required: constants.ENVELOPE_REQUIRED.yes,
      sealed: true,
      stamp: constants.STAMP_TYPES.first_class,
      inserts: data.inserts || null,
      paper: constants.ENVELOPE_SIZES.C5
    },
    data_source: constants.DATA_SOURCE.api_crm,
    delivery: {
      sender: constants.SENDER.ship_for_me,
      user_address: {},
      due_date: due_date.toDate(),
      return_address: data.return_address || null
    },
    source: 'api',
    testing: testing || false,
    notes: data.notes || null
  }

  if (price_only) {
    let goods = pricing.campaign_to_goods(campaign, data.recipients, stationary)
    let price_table = await pricing.build_price_table(user_profile)
    let cost = await pricing.caluclate_price(price_table, user_profile, goods, undefined, undefined,data.currency)

    return {success: true, cost: cost.total_with_tax, currency: cost.currency}
  }

  let stationary_id = await create_stationery(stationary)
  campaign.stationary = stationary_id

  console.log(`Setting campaign ${campaign_doc.id}:`, campaign)
  await campaign_doc.set(campaign)

  console.log(`Adding recipients`, data.recipients)
  return await add_recipients(campaign_doc.id, data.recipients, uid, testing, constants.RECIPIENT_SOURCE.api)
}

async function getUserCampaigns (uid) {
  const campaignsRef = db.collection('campaigns')
  const campaignDocs = await campaignsRef.where('uid', '==', uid).get()

  const campaigns = []

  if (campaignDocs.empty) {
    console.log('empty')
    return campaigns
  }

  for (let campaignDoc of campaignDocs.docs) {
    const id = campaignDoc.id
    const { title } = campaignDoc.data()
    campaigns.push({id, title: title || id})
  }

  return campaigns
}

//TODO: Randomize password
async function addTeamMember(data, uid){
  let base_url = '';
  const urls = {
      local: "https://hc-application-interface-dev.firebaseapp.com",
      development: "https://hc-application-interface-dev.firebaseapp.com",
      testing: "https://app2.scribeless.co",
      production: "https://app.scribeless.co",
  };

  try {
    const environment = configs.environment ? configs.environment.name : '';
    base_url = urls[environment];
    if(!base_url) {
      base_url = urls['production'];
    }
  } catch(e) {
    base_url = urls['production'];
  }

  var user_profile = await utils.get_user_profile(uid)
  var doc_id = ''
  let password = Math.random().toString(36).substr(0, 11);

  // Get payment method
  if (!user_profile) {
    return {success: false, error_message: 'User account not found', error_code: 'error/user-not-found'}
  }

  let userRecord;
  try {
    userRecord = await auth.createUser({
      email: data.email,
      password: password
    })
  } catch (error) {
    switch (error.code) {
      case 'auth/email-already-in-use':
        console.log(`Email address ${data.email} already in use.`)
        return {success:false, error_code: error.code, error_message:`Email address ${data.email} already in use.`}

      case 'auth/invalid-email':
        console.log(`Email address ${data.email} is invalid.`)
        return {success:false, error_code: error.code, error_message:`Email address ${data.email} is invalid.`}

      case 'auth/operation-not-allowed':
        console.log(`Error during sign up.`)
        return {success:false, error_code: error.code, error_message:error.message}

      default:
        console.log(error.message)
        return {success:false, error_code:error.code, error_message:error.message}
    }
  }
  
  // See the UserRecord reference doc for the contents of userRecord.
  console.log("Successfully created new user: ", userRecord.uid);
  
  let user_docs = await db.collection('users').doc(uid).collection('team')
  let curr_date = new Date().toLocaleString()

  let user = {
    first_name: data.first_name,
    last_name: data.last_name,
    email: data.email,
    uid: userRecord.uid,
    parent_id: uid,
    password: password,
    created: curr_date,
    updated: curr_date,
    role: data.role || 'viewer',
    admin: false,
  }

  const team_data = { ...user };

  user['address'] = {
    first_name: data.first_name,
    last_name: data.last_name,
    country: user_profile.country,
  };

  user['country'] = user_profile.country;
  user['currency'] = user_profile.currency;
  user['currency_symbol'] = user_profile.currency_symbol;
  user['billing_enabled'] = user_profile.billing_enabled;
  user['stripe_customer_id'] = user_profile.stripe_customer_id;
  user['stationery_enabled'] = user_profile.stationery_enabled;
  user['use_type'] = user_profile.use_type;
  user['products_enabled'] = user_profile.products_enabled;
  user['handwriting_styles_enabled'] = user_profile.handwriting_styles_enabled;

  if(data.role === 'viewer') {
    user['api_key'] = 'N/A';
  } else if(data.role === 'editor') {
    user['api_key'] = user_profile.api_key;
  }
  
  await user_docs.doc(user.uid).set(team_data);
  await db.collection('users').doc(user.uid).set(user)

  doc_id = userRecord.uid

  console.log('Document Added')

  // let link = await admin.auth().generatePasswordResetLink(data.email)

  // console.log("Reset link: " + link)
  // let text = '<h4>You\'ve been invited to join Scribeless</h4><br/><p>Set a new password here:<a href="' + link + '">Reset Password</a></p>'
  let text = '<h4>You\'ve been invited to join Scribeless</h4><br/><p>Someone created an account for you.<br>Get Started right away!<br>Username: ' + user.email + '<br>Password: ' + password + '<br><br>Log in here: <a href="' + base_url + '">' + base_url + '</a></p>'
  await send_email(user.email,'Invited to Scribeless', text)

  return {success:true, id: doc_id}
}

function send_email (to, subject, text, from, cc) {
  const message = {
    to: to,
    subject: subject,
    text: text,
    html: text
  }

  if (cc) {
    message['cc'] = cc
  }

  if (from) {
    message['from'] = from
  } else {
    message['from'] = 'support@scribeless.co'
  }

  console.debug(`Sending`, message)
  return sgMail.send(message)
}

module.exports = {
  add_recipients,
  campaign_add,
  campaign_delete,
  campaign_get,
  getUserCampaigns,
  mark_campaign_as_deleted,
  toggle_recipient_deleted,
  addTeamMember
}
