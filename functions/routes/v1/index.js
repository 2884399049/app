const functions = require('firebase-functions')
const admin = require('firebase-admin')
const firebase = require('firebase')

const { validateCampaignId } = require('../../modules')
const {
    add_recipients,
    campaign_add,
    campaign_delete,
    campaign_get,
    addTeamMember
} = require('./functions')
const constants = require('../../shared/constants')
const validateApiKey = require('../../middlewares/validateApiKey')
const validateRequest = require('../../middlewares/validateRequest')
var utils = require('../../lib/utils')

const router = require('express').Router()
const HTTP_STATUS = require('http-status')

const configs = functions.config()
const db = admin.firestore()

firebase.initializeApp({
  apiKey: configs.firestore.apikey,
  authDomain: configs.firestore.authdomain,
  projectId: configs.firestore.projectid
})

/**
 * @swagger
 *  /auth:
 *    post:
 *      tags:
 *        - Authentication
 *      summary: Authenticate user
 *      operationId: auth
 *      consumes:
 *        - application/json
 *      produces:
 *        - application/json
 *      parameters:
 *       - in: body
 *         required: true
 *         name: body
 *         schema:
 *          type: object
 *          properties:
 *            email:
 *              type: string
 *              required: true
 *              example: test@gmail.com
 *            password:
 *              type: string
 *              required: true
 *              example: "test"
 *      responses:
 *       400:
 *         description: Bad request
 *       409:
 *         description: User already exist
 */
router.post('/auth', async (req, res) => {
  const {
    body: { email, password }
  } = req
  const result = {
    success: false
  }
  let status = HTTP_STATUS.FORBIDDEN

  if (!email || !password) {
    result.error_code = 'error/field-required'
    result.error_message = 'Email and Password is required'
    return res.status(status).json(result)
  }

  try {
    const { user } = await firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
    var user_profile = await utils.get_user_profile(user.uid)

    result.success = true
    result.user = user_profile
    status = HTTP_STATUS.OK
  } catch (error) {
    result.success = false
    result.error_code = error.code
    result.error_message = error.message
  }

  return res.status(status).json(result)
})

/**
 * @swagger
 * /campaigns:
 *    get:
 *      tags:
 *        - Campaign
 *      summary: Get a list of a users campaigns
 *      operationId: getCampaigns
 *      produces:
 *        - application/json
 *      consumes:
 *        - application/json
 *      parameters:
 *       - in: query
 *         allowReserved: true
 *         name: api_key
 *         required: true
 *         schema:
 *           type: string
 *           default: DJb8iahHxiPd7dbYSlj2xcIQ5wJ2$DJb8iahHxiPd7dbYSlj2xcIQ5wJ2
 *         description: User api key
 *       - in: query
 *         name: testing
 *         required: true
 *         schema:
 *           type: boolean
 *           default: true
 *         description: If true requests will modify the database however you will not be charged for any orders placed
 *      responses:
 *       200:
 *        description: Ok
 *        schema:
 *         type: object
 *         properties:
 *          success:
 *           type: boolean
 *           description: Retrieved successfully
 *          status:
 *           type: string
 *           description: Campaign status
 *          cost:
 *           type: integer
 *           description: Campaign cost
 *          currency:
 *           type: string
 *           description: Campaign currency
 *        examples:
 *          application/json: |-
 *            { success: true, status: "In progress", cost 378, currency: "usd" }
 *       401:
 *         description: Unauthorized
 */
router.get('/campaigns', validateApiKey, validateRequest, async (req, res) => {
  const {
    locals: { uid }
  } = req
  let result = constants.RETURN_OBJECTS.INTERNAL_SERVER_ERROR
  let status = HTTP_STATUS.INTERNAL_SERVER_ERROR

  try {
    console.log(`GET campaigns by ${uid}`)

    let campaign_docs = await db
      .collection('campaigns')
      .where('uid', '==', uid)
      .get()
    let campaigns = []
    for (var campaign_doc of campaign_docs.docs) {
      let campaign = campaign_doc.data()
      campaign.id = campaign_doc.id
      campaigns.push(campaign)
    }

    result = campaigns
    status = HTTP_STATUS.OK
  } catch (error) {
    console.error(
      'Internal Server Error in `GET /campaign/:campaign_id`: ',
      error
    )
  }

  return res.status(status).json(result)
})

router.get('/pong', async (req, res) => {
  res.send('Success')
})

/**
 * @swagger
 * /campaign/{campaign_id}:
 *    get:
 *      tags:
 *        - Campaign
 *      summary: Get a particular campaign
 *      operationId: getCampaign
 *      produces:
 *        - application/json
 *      consumes:
 *        - application/json
 *      parameters:
 *       - in: path
 *         required: true
 *         name: campaign_id
 *         schema:
 *           type: string
 *           default: 6AYveslBNhO3Qlcuq7JI
 *       - in: query
 *         allowReserved: true
 *         name: api_key
 *         required: true
 *         schema:
 *           type: string
 *           default: DJb8iahHxiPd7dbYSlj2xcIQ5wJ2$DJb8iahHxiPd7dbYSlj2xcIQ5wJ2
 *         description: User api key
 *       - in: query
 *         name: testing
 *         required: true
 *         schema:
 *           type: boolean
 *           default: true
 *         description: If true requests will modify the database however you will not be charged for any orders placed
 *      responses:
 *       200:
 *        description: Ok
 *        schema:
 *         type: object
 *         properties:
 *          success:
 *           type: boolean
 *           description: Retrieved successfully
 *          status:
 *           type: string
 *           description: Campaign status
 *          cost:
 *           type: integer
 *           description: Campaign cost
 *          currency:
 *           type: string
 *           description: Campaign currency
 *        examples:
 *          application/json: |-
 *            { success: true, status: "In progress", cost 378, currency: "usd" }
 *       401:
 *         description: Unauthorized
 */
router.get('/campaign/:campaign_id', validateApiKey, validateRequest, async(req, res) => {
        const {
            locals: { uid },
            params: { campaign_id = '' }
        } = req
        let result = constants.RETURN_OBJECTS.INTERNAL_SERVER_ERROR
        let status = HTTP_STATUS.INTERNAL_SERVER_ERROR

        try {
            const validateCampaignIdRes = await validateCampaignId(uid, campaign_id)

            if (!validateCampaignIdRes.success) {
                return res.status(HTTP_STATUS.FORBIDDEN).json(validateCampaignIdRes)
            }

            console.log(`GET ${campaign_id} by ${uid}`)

            result = await campaign_get(campaign_id, uid)
            status = HTTP_STATUS.OK
        } catch (error) {
            console.error(
                'Internal Server Error in `GET /campaign/:campaign_id`: ',
                error
            )
        }

        return res.status(status).json(result)
    }
)

/**
 * @swagger
 *  /campaign:
 *    post:
 *      tags:
 *        - Campaign
 *      summary: Create a new campaign
 *      operationId: createCampaign
 *      produces:
 *        - application/json
 *      consumes:
 *        - application/json
 *      parameters:
 *       - in: query
 *         allowReserved: true
 *         name: api_key
 *         required: true
 *         schema:
 *           type: string
 *           default: DJb8iahHxiPd7dbYSlj2xcIQ5wJ2$DJb8iahHxiPd7dbYSlj2xcIQ5wJ2
 *         description: User api key
 *       - in: query
 *         name: testing
 *         required: true
 *         schema:
 *           type: boolean
 *           default: true
 *         description: If true requests will modify the database however you will not be charged for any orders placed
 *       - in: body
 *         required: true
 *         name: body
 *         description: This endpoint creates a new campaign
 *         schema:
 *          type: object
 *          properties:
 *            paper_size:
 *              type: string,
 *              required: true,
 *              example: A5 # Read more here https://support.thehandwriting.company/portal/kb/articles/api-endpoints#Paper_size
 *            handwriting_style:
 *              type: string,
 *              required: true,
 *              example: Jane # https://support.thehandwriting.company/portal/kb/articles/api-endpoints#Products
 *            title:
 *              type: string,
 *              required: false,
 *              example: My campaign
 *            product:
 *              type: string,
 *              required: false,
 *              example: Advanced # https://support.thehandwriting.company/portal/kb/articles/api-endpoints#Products
 *            text:
 *              type: string,
 *              required: true,
 *              example: Hello {{{first name}}},This is a test!
 *            inserts:
 *              type: string,
 *              required: false,
 *              example: $5 Starbucks gift card
 *            due_date:
 *              type: string,
 *              required: true,
 *              example: "31/01/2021"
 *            notes:
 *              type: string,
 *              required: false,
 *              example: Campaign notes
 *            header_image:
 *              type: string,
 *              required: false,
 *              example: http://app.thehandwriting.company/static/images/your_logo.png
 *            header_type:
 *              type: string,
 *              required: false,
 *              example: Logo # Either "Text" (default) or "Logo"
 *            header_font:
 *              type: string,
 *              required: false,
 *              example: josefin # Either "josefin", "playfair", or "poiret"
 *            header_text:
 *              type: string,
 *              required: false,
 *              example: Header text # Optional text to add to stationary as header (instead of a logo), leave blank for no header
 *            footer_text:
 *              type: string,
 *              required: false,
 *              example: Footer text
 *            footer_font:
 *              type: string,
 *              required: false,
 *              example: josefin # Either "josefin", "playfair", or "poiret"
 *            return_address:
 *              type: object,
 *              required: false,
 *              example: { "address line 1": "Flat 1", "address line 2": "123 Broom Road", "address line 3": "Bathwick hill", "city": "London", "country": "United Kingdom", "department": "HR","first name": "Tim", "last name": "Johnson","state/region": "London", "title": "Mr","zip/postal code": "TW11 9PG" }
 *            recipients:
 *              type: array,
 *              required: true,
 *              example: [{ "address line 1": "Flat 1", "address line 2": "123 Broom Road", "address line 3": "Bathwick hill", "city": "London", "country": "United Kingdom", "department": "HR","first name": "Tim", "last name": "Johnson","state/region": "London", "title": "Mr","zip/postal code": "TW11 9PG" }]
 *      responses:
 *       200:
 *        description: Ok
 *        schema:
 *         type: object
 *         properties:
 *          success:
 *           type: boolean
 *           description: Added successfully
 *          status:
 *           type: string
 *           description: Campaign status
 *          id:
 *           type: string
 *           description: Campaign id
 *          cost:
 *           type: integer
 *           description: Campaign cost
 *          currency:
 *           type: string
 *           description: Campaign currency
 *          stationary_thumbnail:
 *           type: string
 *           description: Stationary thumbnail
 *        examples:
 *          application/json: |-
 *            {
 *               "success": true,
 *               "cost": 378,
 *               "status": "Pending",
 *               "id": "6AYveslBNhO3Qlcuq7JI",
 *               "currency": "usd",
 *               "stationary_thumbnail": "https://storage.googleapis.com/hc-application-interface-dev.appspot.com/stationary/JhciAEGt59RomVnVdTdE/thumbnail.jpg"
 *            }
 *       401:
 *         description: Unauthorized
 */
router.post('/campaign', validateApiKey, validateRequest, async(req, res) => {
    let result = constants.RETURN_OBJECTS.INTERNAL_SERVER_ERROR
    let status = HTTP_STATUS.INTERNAL_SERVER_ERROR

    try {
        const { testing, uid } = req.locals

        console.log(`POST by ${uid}`)
        result = await campaign_add(req.body, uid, testing)
        status = HTTP_STATUS.OK
    } catch (error) {
        console.error(
            'Internal Server Error in `POST /campaign/:campaign_id`: ',
            error
        )
    }

    return res.status(status).json(result)
})

/**
 * @swagger
 *  /campaign/{campaign_id}:
 *    put:
 *      tags:
 *        - Campaign
 *      summary: Update a campaign
 *      operationId: updateCampaign
 *      consumes:
 *        - application/json
 *      produces:
 *        - application/json
 *      parameters:
 *       - in: path
 *         required: true
 *         name: campaign_id
 *         schema:
 *           type: string
 *           default: 6AYveslBNhO3Qlcuq7JI
 *       - in: query
 *         allowReserved: true
 *         name: api_key
 *         required: true
 *         schema:
 *           type: string
 *           default: DJb8iahHxiPd7dbYSlj2xcIQ5wJ2$DJb8iahHxiPd7dbYSlj2xcIQ5wJ2
 *         description: User api key
 *       - in: query
 *         name: testing
 *         required: true
 *         schema:
 *           type: boolean
 *           default: true
 *         description: If true requests will modify the database however you will not be charged for any orders placed
 *       - in: body
 *         required: true
 *         name: body
 *         description: This endpoint creates a new campaign
 *         schema:
 *          type: object
 *          properties:
 *            recipients:
 *              type: array,
 *              required: true,
 *              example: [{ "address line 1": "Flat 1", "address line 2": "123 Broom Road", "address line 3": "Bathwick hill", "city": "London", "country": "United Kingdom", "department": "HR","first name": "Tim", "last name": "Johnson","state/region": "London", "title": "Mr","zip/postal code": "TW11 9PG" }]
 *      responses:
 *       200:
 *        description: Ok
 *        schema:
 *         type: object
 *         properties:
 *          success:
 *           type: boolean
 *           description: Added successfully
 *          status:
 *           type: string
 *           description: Campaign status
 *          id:
 *           type: string
 *           description: Campaign id
 *          cost:
 *           type: integer
 *           description: Campaign cost
 *          stationary_thumbnail:
 *           type: string
 *           description: Stationary thumbnail
 *        examples:
 *          application/json: |-
 *            {
 *               "success": true,
 *               "cost": 378,
 *               "status": "Pending",
 *               "id": "6AYveslBNhO3Qlcuq7JI",
 *               "currency": "usd",
 *               "stationary_thumbnail": "https://storage.googleapis.com/hc-application-interface-dev.appspot.com/stationary/JhciAEGt59RomVnVdTdE/thumbnail.jpg"
 *            }
 *       401:
 *         description: Unauthorized
 */
router.put('/campaign/:campaign_id', validateApiKey, validateRequest, async(req, res) => {
        let {
            locals: { uid, testing },
            params: { campaign_id = '' },
            body: { recipients }
        } = req

        let result = constants.RETURN_OBJECTS.INTERNAL_SERVER_ERROR
        let status = HTTP_STATUS.INTERNAL_SERVER_ERROR

        if (!campaign_id && req.body.campaign_id) {
            campaign_id = req.body.campaign_id
        }

        try {
            const validateCampaignIdRes = await validateCampaignId(uid, campaign_id)

            if (!validateCampaignIdRes.success) {
                return res.status(HTTP_STATUS.FORBIDDEN).json(validateCampaignIdRes)
            }

            if (!recipients || recipients.length === 0) {
                return res.status(HTTP_STATUS.FORBIDDEN).json({ error_code: 'error/field-required', error_message: 'Recipients are required' })
            }

            console.log(`POST by ${uid}`)
            result = await add_recipients(campaign_id, recipients, uid, testing, constants.RECIPIENT_SOURCE.api)
            if (result.success) {
                status = HTTP_STATUS.OK
            } else {
                status = HTTP_STATUS.FORBIDDEN
            }
        } catch (error) {
            console.error(
                'Internal Server Error in `PUT /campaign/:campaign_id`: ',
                error
            )
        }

        return res.status(status).json(result)
    }
)

/**
 * @swagger
 *  /campaign/{campaign_id}:
 *    delete:
 *      tags:
 *        - Campaign
 *      summary: Delete a campaign
 *      operationId: deleteCampaign
 *      consumes:
 *        - application/json
 *      produces:
 *        - application/json
 *      parameters:
 *       - in: path
 *         required: true
 *         name: campaign_id
 *         schema:
 *           type: string
 *           default: 6AYveslBNhO3Qlcuq7JI
 *       - in: query
 *         allowReserved: true
 *         name: api_key
 *         required: true
 *         schema:
 *           type: string
 *           default: DJb8iahHxiPd7dbYSlj2xcIQ5wJ2$DJb8iahHxiPd7dbYSlj2xcIQ5wJ2
 *         description: User api key
 *       - in: query
 *         name: testing
 *         required: true
 *         schema:
 *           type: boolean
 *           default: true
 *         description: If true requests will modify the database however you will not be charged for any orders placed
 *      responses:
 *       200:
 *        description: Ok
 *        schema:
 *         type: object
 *         properties:
 *          success:
 *           type: boolean
 *           description: Deleted successfully
 *        examples:
 *          application/json: |-
 *            { "success": true }
 *       401:
 *        description: Unauthorized
 */
router.delete('/campaign/:campaign_id', validateApiKey, validateRequest, async(req, res) => {
        let {
            locals: { uid, testing },
            params: { campaign_id = '' }
        } = req

        let result = constants.RETURN_OBJECTS.INTERNAL_SERVER_ERROR
        let status = HTTP_STATUS.INTERNAL_SERVER_ERROR

        if (!campaign_id && req.body.campaign_id) {
            campaign_id = req.body.campaign_id
        }

        try {
          const validateCampaignIdRes = await validateCampaignId(uid, campaign_id)

          if (!validateCampaignIdRes.success) {
            return res.status(HTTP_STATUS.FORBIDDEN).json(validateCampaignIdRes)
          }

          console.log(`DELETE ${campaign_id} by ${uid}`)
          result = await campaign_delete(campaign_id, uid, testing)
          status = HTTP_STATUS.OK
        } catch (error) {
          console.error(
            'Internal Server Error in `DELETE /campaign/:campaign_id`: ',
            error
          )
        }

        return res.status(status).json(result)
    }
)

/**
 * @swagger
 *  /price:
 *    post:
 *      tags:
 *        - Price
 *      summary: Price a campaign
 *      operationId: priceCampaign
 *      consumes:
 *        - application/json
 *      produces:
 *        - application/json
 *      parameters:
 *       - in: query
 *         allowReserved: true
 *         name: api_key
 *         required: true
 *         schema:
 *           type: string
 *           default: DJb8iahHxiPd7dbYSlj2xcIQ5wJ2$DJb8iahHxiPd7dbYSlj2xcIQ5wJ2
 *         description: User api key
 *       - in: query
 *         name: testing
 *         required: true
 *         schema:
 *           type: boolean
 *           default: true
 *         description: If true requests will modify the database however you will not be charged for any orders placed
 *       - in: body
 *         required: true
 *         name: body
 *         description: This endpoint creates a new campaign
 *         schema:
 *          type: object
 *          properties:
 *            paper_size:
 *              type: string,
 *              required: true,
 *              example: A5 # Read more here https://support.thehandwriting.company/portal/kb/articles/api-endpoints#Paper_size
 *            handwriting_style:
 *              type: string,
 *              required: true,
 *              example: Jane # https://support.thehandwriting.company/portal/kb/articles/api-endpoints#Products
 *            title:
 *              type: string,
 *              required: false,
 *              example: My campaign
 *            product:
 *              type: string,
 *              required: false,
 *              example: Advanced # https://support.thehandwriting.company/portal/kb/articles/api-endpoints#Products
 *            text:
 *              type: string,
 *              required: true,
 *              example: Hello {{{first name}}},This is a test!
 *            inserts:
 *              type: string,
 *              required: false,
 *              example: $5 Starbucks gift card
 *            due_date:
 *              type: string,
 *              required: true,
 *              example: "31/01/2021"
 *            notes:
 *              type: string,
 *              required: false,
 *              example: Campaign notes
 *            header_image:
 *              type: string,
 *              required: false,
 *              example: http://app.scribeless.co/static/images/your_logo.png
 *            header_type:
 *              type: string,
 *              required: false,
 *              example: Logo # Either "Text" (default) or "Logo"
 *            footer_text:
 *              type: string,
 *              required: false,
 *              example: Footer text
 *            footer_font:
 *              type: string,
 *              required: false,
 *              example: osefin # Either "osefin", "playfair", or "poiret"
 *            return_address:
 *              type: object,
 *              required: false,
 *              example: { "address line 1": "Flat 1", "address line 2": "123 Broom Road", "address line 3": "Bathwick hill", "city": "London", "country": "United Kingdom", "department": "HR","first name": "Tim", "last name": "Johnson","state/region": "London", "title": "Mr","zip/postal code": "TW11 9PG" }
 *            recipients:
 *              type: array,
 *              required: true,
 *              example: [{ "address line 1": "Flat 1", "address line 2": "123 Broom Road", "address line 3": "Bathwick hill", "city": "London", "country": "United Kingdom", "department": "HR","first name": "Tim", "last name": "Johnson","state/region": "London", "title": "Mr","zip/postal code": "TW11 9PG" }]
 *      responses:
 *       200:
 *        description: Ok
 *        schema:
 *         type: object
 *         properties:
 *          success:
 *           type: boolean
 *           description: Added successfully
 *          cost:
 *           type: integer
 *           description: Campaign cost
 *          currency:
 *           type: string
 *           description: Campaign currency
 *        examples:
 *          application/json: |-
 *            {
 *               "success": true,
 *               "cost": 378,
 *               "currency": "usd"
 *            }
 *       401:
 *        description: Unauthorized
 */
router.post('/price', validateApiKey, validateRequest, async(req, res) => {
    const { testing, uid } = req.locals

    let result = constants.RETURN_OBJECTS.INTERNAL_SERVER_ERROR
    let status = HTTP_STATUS.INTERNAL_SERVER_ERROR

    try {
        console.debug(`POST by ${uid}`)
        result = await campaign_add(req.body, uid, testing, true)
        status = HTTP_STATUS.OK
    } catch (error) {
        console.error('Internal Server Error in `POST /price`: ', error)
    }

    return res.status(status).json(result)
})

/**
 * @swagger
 *  /campaign/{campaign_id}:
 *    delete:
 *      tags:
 *        - Campaign
 *      summary: Delete a campaign
 *      operationId: deleteCampaign
 *      consumes:
 *        - application/json
 *      produces:
 *        - application/json
 *      parameters:
 *       - in: path
 *         required: true
 *         name: campaign_id
 *         schema:
 *           type: string
 *           default: 6AYveslBNhO3Qlcuq7JI
 *       - in: query
 *         allowReserved: true
 *         name: api_key
 *         required: true
 *         schema:
 *           type: string
 *           default: DJb8iahHxiPd7dbYSlj2xcIQ5wJ2$DJb8iahHxiPd7dbYSlj2xcIQ5wJ2
 *         description: User api key
 *       - in: query
 *         name: testing
 *         required: true
 *         schema:
 *           type: boolean
 *           default: true
 *         description: If true requests will modify the database however you will not be charged for any orders placed
 *      responses:
 *       200:
 *        description: Ok
 *        schema:
 *         type: object
 *         properties:
 *          success:
 *           type: boolean
 *           description: Deleted successfully
 *        examples:
 *          application/json: |-
 *            { "success": true }
 *       401:
 *        description: Unauthorized
 */
router.post('/teammember', validateApiKey, validateRequest, async(req, res) => {
  let result = constants.RETURN_OBJECTS.INTERNAL_SERVER_ERROR
  let status = HTTP_STATUS.INTERNAL_SERVER_ERROR

  try {
      const { uid } = req.locals

      console.log(`POST by ${uid}`)
      result = await addTeamMember(req.body, uid)
      status = HTTP_STATUS.OK
  } catch (error) {
      console.error(
          'Internal Server Error in `POST /teammember`: ',
          error
      )
  }

  return res.status(status).json(result)
})

module.exports = router
