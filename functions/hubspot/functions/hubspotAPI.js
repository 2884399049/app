const fetch = require('node-fetch')
const request = require('request-promise-native')
const functions = require('firebase-functions')
const configs = functions.config()
const {app_id: APP_ID} = configs.hubspot

const api = {}

api.exchangeForTokens = async (exchangeProof) => {
  const url = 'https://api.hubapi.com/oauth/v1/token'
  const responseBody = await request.post(url, {
    form: exchangeProof
  })
  return JSON.parse(responseBody)
}

api.getAccessTokenInfo = async (accessToken) => {
  const url = `https://api.hubapi.com/oauth/v1/access-tokens/${accessToken}`
  return fetch(url)
    .then((res) => {
      if (res.ok) { // res.status >= 200 && res.status < 300
        return res
      } else {
        throw Error(res.statusText)
      }
    })
    .then(res => res.json())
}

api.getContactDataByVid = async (vid, accessToken) => {
  const params = {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${accessToken}`
    }
  }

  const url = `https://api.hubapi.com/contacts/v1/contact/vid/${vid}/profile`
  return fetch(url, params)
    .then((res) => {
      if (res.ok) { // res.status >= 200 && res.status < 300
        return res
      } else {
        throw Error(res.statusText)
      }
    })
    .then(res => res.json())
}

api.addTimelineEvent = async (vid, accessToken, campaign_id) => {
  const params = {
    method: 'PUT',
    headers: {
      'Accept': 'application/json, text/plain, */*',
      Authorization: `Bearer ${accessToken}`,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({id: Math.random().toString(36).substring(7), eventTypeId: 394133, objectId: vid, 'timelineIFrame': {'linkLabel': 'View campaign on scribeless', 'iframeLabel': 'Campaign', 'iframeUri': `https://app.scribeless.co/campaign/${campaign_id}`, 'width': 800, 'height': 800}})
  }

  const url = `https://api.hubapi.com/integrations/v1/${APP_ID}/timeline/event`

  console.log(url, params)

  return fetch(url, params)
    .then((res) => {
      if (res.ok) { // res.status >= 200 && res.status < 300
        return res
      } else {
        throw Error(res.statusText)
      }
    })
}

module.exports = api
