const HTTP_STATUS = require('http-status')
const crypto = require('crypto')
const functions = require('firebase-functions')

const configs = functions.config()

const {
  client_secret: CLIENT_SECRET, scopes, api_address: SERVICE_ADDRESS
} = configs.hubspot

const _ = {}

// _.validateV2RequestSignature = (req, res, next) => {
//   const signature = req.get('X-HubSpot-Signature')
//
//   if (!signature) {
//     return res.sendStatus(HTTP_STATUS.OK)
//   }
//
//   const appSecret = CLIENT_SECRET
//   const httpMethod = req.method
//   const URI = SERVICE_ADDRESS + req.url
//   const requestBody = req.body && JSON.stringify(req.body) || ''
//
//   const sourceString = appSecret + httpMethod + URI + requestBody
//
//   const hash = crypto.createHash('sha256').update(sourceString).digest('hex')
//
//   const ok = signature === hash
//   if (ok) {
//     return next()
//   }
//   return res.sendStatus(HTTP_STATUS.OK)
// }

module.exports = _
