const functions = require('firebase-functions')
const admin = require('firebase-admin')
const firebase = require('firebase')
const router = require('express').Router()
const HTTP_STATUS = require('http-status')

const {
  required, getHubspotAuthUrl, exchangeForTokens, getRedirectUrl
} = require('../functions')
const { getAccessTokenInfo } = require('../functions/hubspotAPI')
// const { validateV2RequestSignature } = require('../middlewares')

const db = admin.firestore()
const Hubspot = db.collection('hubspot')

const configs = functions.config()
const {
  client_id: CLIENT_ID, client_secret: CLIENT_SECRET, scopes, api_address,
  testing: TESTING, app_store_origin: APP_STORE_ORIGIN, local_dev: LOCAL_DEV
} = configs.hubspot

const {
  HUBSPOT: {
    WEBHOOK_ROUTE, INSTALL_ROUTE, CALLBACK_ROUTE, AUTH_ROUTE, ERROR_ROUTE
  }
} = require('../../shared/constants')

let { HUBSPOT: { BASE_ROUTE } } = require('../../shared/constants')
BASE_ROUTE = LOCAL_DEV === '1' ? `/${configs.firestore.projectid}/us-central1` + BASE_ROUTE : BASE_ROUTE

module.exports = function (apiFunctions) {
  // NOTE ignoring signed requests
  // router.use(WEBHOOK_ROUTE, validateV2RequestSignature, require('./webhook')(apiFunctions));
  router.use(WEBHOOK_ROUTE, require('./webhook')(apiFunctions))

  /**
   * @swagger
   *  /hubspot/auth-form.html:
   */
  router.get('/auth-form.html', (req, res) => {
    const vars = {
      appStoreOrigin: TESTING === '1' ? `*` : APP_STORE_ORIGIN,
      authRoute: BASE_ROUTE + AUTH_ROUTE
    }
    return res.render('auth-form', vars)
  })

  /**
   * @swagger
   *  /hubspot/auth:
   *    post:
   *      tags:
   *        - Authentication
   *      summary: Authenticate user
   *      operationId: auth
   *      consumes:
   *        - application/json
   *      produces:
   *        - application/json
   *      parameters:
   *       - in: body
   *         required: true
   *         name: body
   *         schema:
   *          type: object
   *          properties:
   *            email:
   *              type: string
   *              required: true
   *              example: test@gmail.com
   *            password:
   *              type: string
   *              required: true
   *              example: "test"
   *      responses:
   *       400:
   *         description: Bad request
   *       409:
   *         description: User already exist
   */
  router.post(AUTH_ROUTE, async (req, res) => {
    let { email, password, uid } = req.body

    const result = {success: false}
    let status = HTTP_STATUS.FORBIDDEN

    if (uid) {
      console.log('Setting session id to ', uid)
      req.session.handwritinguid = uid
      result.success = true
      status = HTTP_STATUS.OK
    } else {
      if (!email || !password) {
        result.error_code = 'error/field-required'
        result.error_message = 'Email and Password is required'
        return res.status(status).json(result)
      }

      try {
        const { user } = await firebase.auth().signInWithEmailAndPassword(email, password)
        let uid = user.uid

        req.session.handwritinguid = uid

        result.success = true
        status = HTTP_STATUS.OK
      } catch (error) {
        result.success = false
        result.error_code = error.code
        result.error_message = error.message
      }
    }

    return res.status(status).json(result)
  })

  // ================================//
  //   Running the OAuth 2.0 Flow   //
  // ================================//

  // Step 1
  // Build the authorization URL to redirect a user
  // to when they choose to install the app
  // Redirect the user from the installation page to
  // the authorization URL
  router.get(INSTALL_ROUTE, (req, res) => {
    console.log('Initiating OAuth 2.0 flow with HubSpot')
    console.log("Step 1: Redirecting user to HubSpot's OAuth 2.0 server")

    const { uid } = req.query
    req.session.handwritinguid = uid
    console.log(uid)

    res.redirect(getHubspotAuthUrl())
    console.log('Step 2: User is being prompted for consent by HubSpot')
  })

  // Step 2
  // The user is prompted to give the app access to the requested
  // resources. This is all done by HubSpot, so no work is necessary
  // on the app's end

  // Step 3
  // Receive the authorization code from the OAuth 2.0 Server,
  // and process it based on the query parameters that are passed
  router.get(CALLBACK_ROUTE, async (req, res) => {
    console.log('Step 3: Handling the request sent by the server')
    console.log('req.query=', req.query)
    console.log('req.session.handwritinguid=', req.session.handwritinguid)

    const uid = req.session.handwritinguid

    if (!uid) {
      const message = 'There is no connected "Scribeless" service account'
      return redirectToErrorPage(res, message)
    }

    // Received a user authorization code, so now combine that with the other
    // required values and exchange both for an access token and a refresh token
    if (req.query.code) {
      console.log('  > Received an authorization token')

      const authCodeProof = {
        grant_type: 'authorization_code',
        client_id: CLIENT_ID,
        client_secret: CLIENT_SECRET,
        redirect_uri: getRedirectUrl(),
        code: req.query.code
      }

      // Step 4
      // Exchange the authorization code for an access token and refresh token
      console.log('Step 4: Exchanging authorization code for an access token and refresh token')

      let refresh_token
      let access_token
      let expires_in
      try {
        ({ refresh_token, access_token, expires_in } = await exchangeForTokens(authCodeProof))
      } catch (error) {
        return redirectToErrorPage(res, error.message)
      }

      let hub_id
      let app_id
      let scopes
      try {
        ({ hub_id, app_id, scopes } = await getAccessTokenInfo(access_token))
        hub_id = String(hub_id)
      } catch (error) {
        return redirectToErrorPage(res, error.message)
      }

      // if app_id has changed we can know that it is the installation

      const dataToUpdate = {
        scopes,
        access_token,
        refresh_token,
        expires_in,
        uid,
        app_id
      }

      let hubDoc
      try {
        const hubDocRef = await Hubspot.doc(hub_id)
        const hubDocSnapshot = await hubDocRef.get()
        if (hubDocSnapshot.exists) {
          hubDoc = await hubDocRef.update(dataToUpdate)
        } else {
          hubDoc = await hubDocRef.set(dataToUpdate)
        }

        await db.collection('users').doc(uid).update({hubspot_integration_doc: hubDocRef})
      } catch (error) {
        const message = 'storing hubspot data error'
        console.error(message, error.message)
        return redirectToErrorPage(res, message)
      }

      // Once the tokens have been retrieved, use them to make a query
      // to the HubSpot API

      // back to hubspot
      const redirectUrl = `https://app.hubspot.com/integrations-settings/${hub_id}/installed`
      res.redirect(redirectUrl)
    } else {
      const message = 'code parameter is not specified in query'
      return redirectToErrorPage(res, message)
    }
  })

  router.get(ERROR_ROUTE, (req, res) => {
    res.setHeader('Content-Type', 'text/html')
    res.write(`<h4>Error: ${req.query.msg}</h4>`)
    res.end()
  })

  return router
}

function redirectToErrorPage (res, message) {
  const route = BASE_ROUTE + ERROR_ROUTE
  return res.redirect(`${route}?msg=${encodeURIComponent(message)}`)
}
