const firebase = require('firebase')
const functions = require('firebase-functions')
const configs = functions.config()
const admin = require('firebase-admin')
const db = admin.firestore()

const express = require('express')
const path = require('path')
const session = require('express-session')
const consolidate = require('consolidate')
const cors = require('cors')

const {FirestoreStore} = require('@google-cloud/connect-firestore')

const constants = require('../shared/constants')

const BASE_ROUTE = constants.HUBSPOT.BASE_ROUTE

if (!firebase.apps.length) {
  firebase.initializeApp({
    apiKey: configs.firestore.apikey,
    authDomain: configs.firestore.authdomain,
    projectId: configs.firestore.projectid
  })
}

const app = express()
const router = express.Router()

app.use(cors())

app.use(session({
  secret: 'RSQSyKZHRSQSyKZH213',
  resave: false,
  saveUninitialized: true
}))

// app.use(
//   session({
//     store: new FirestoreStore({
//       database: db
//     }),
//     secret: 'RSQSyKZHRSQSyKZH213',
//     resave: false,
//     saveUninitialized: true,
//   })
// );

// view engine setup
// assign the handlebars engine to .hbs files
app.engine('hbs', consolidate.handlebars)
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'hbs')

app.use(router)

module.exports = function (apiFunctions) {
  router.use('/', require('./routes')(apiFunctions))
  return app
}
