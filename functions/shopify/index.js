const firebase = require('firebase')
const functions = require('firebase-functions')
const configs = functions.config()

const express = require('express')
const path = require('path')
const consolidate = require('consolidate')
const swaggerUi = require('swagger-ui-express')
const swaggerJSDoc = require('swagger-jsdoc')
// const helmet = require('helmet')

if (!firebase.apps.length) {
  firebase.initializeApp({
    apiKey: configs.firestore.apikey,
    authDomain: configs.firestore.authdomain,
    projectId: configs.firestore.projectid
  })
}

const app = express()
const router = express.Router()

// view engine setup
// assign the handlebars engine to .hbs files
app.engine('hbs', consolidate.handlebars)
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'hbs')

const swaggerOptions = {
  customSiteTitle: 'Scribeless shopify API',
  customCss: '.topbar, .scheme-container { display: none };'
}

const swaggerDocument = swaggerJSDoc({
  definition: {
    info: {
      title: 'Scribeless shopify API',
      version: '1.0.0',
      description: 'Scribeless shopify API'
    },
    produces: ['application/json'],
    consumes: ['application/json'],
    basePath: '/shopify/' // TODO add hc-application-interface-dev/us-central1/ for local DEV
  },
  apis: [`${process.cwd()}/shopify/routes/index.js`, `${process.cwd()}/shopify/routes/webhhok.js`]
})

console.log(`${process.cwd()}/routes/index.js`)

router.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, swaggerOptions))

app.use(router)

// app.use(helmet.frameguard({ action: 'sameorigin' }))

module.exports = function (apiFunctions) {
  router.use('/', require('./routes')(apiFunctions))
  return app
}
