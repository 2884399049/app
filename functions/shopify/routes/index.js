const functions = require('firebase-functions')
const admin = require('firebase-admin')
const firebase = require('firebase')
const router = require('express').Router()
const HTTP_STATUS = require('http-status')
const crypto = require('crypto')
const cookie = require('cookie')
const nonce = require('nonce')()
const { verifyOAuth, obtainAccessToken } = require('../../lib/shopifyHelpers')
const notifications = require('../../lib/notifications')
const verifyWebhook = require('../middlewares').verifyWebhook
const querystring = require('querystring')
var utils = require('../../lib/utils')

const {
  createOnCustomerCreateWebhook,
  isCustomerCreatedNotifyActive,
  createOnDeleteWebhook,
  deleteWebhook,
  isOrderCreatedNotifyActive,
  createOnOrderCreateWebhook
} = require('../functions')

const constants = require('../../shared/constants')

const {
  SHOPIFY: {
    BASE_ROUTE, WEBHOOK_ROUTE, INSTALL_ROUTE, CALLBACK_ROUTE, SETTINGS_ROUTE
  }
} = constants

// TODO add read_checkouts
const scope = 'read_customers,read_orders,read_content,write_content'

const configs = functions.config()
const db = admin.firestore()

const Shopify = db.collection('shopify')

const {
  app_name: appName,
  testing = false,
  api_version: apiVersion,
  api_key: apiKey,
  api_secret: apiSecret,
  service_address: serviceAddress,
  front_url: frontUrl
} = configs.shopify

module.exports = function (apiFunctions) {
  router.use(WEBHOOK_ROUTE, verifyWebhook, require('./webhook')(apiFunctions))

  router.get('/error', (req, res) => res.render('shopify/error', { message: 'Something went wrong!' }))

  /**
   * @swagger
   *  /shopify/connectAccount:
   *    post:
   *      tags:
   *        - Authentication
   *      summary: Authenticate user
   *      operationId: auth
   *      consumes:
   *        - application/json
   *      produces:
   *        - application/json
   *      parameters:
   *       - in: body
   *         required: true
   *         name: body
   *         schema:
   *          type: object
   *          properties:
   *            email:
   *              type: string
   *              required: true
   *              example: test@gmail.com
   *            password:
   *              type: string
   *              required: true
   *              example: "test"
   *      responses:
   *       400:
   *         description: Bad request
   *       409:
   *         description: User already exist
   */
  router.post('/connectAccount', async (req, res) => {
    const { email, password, shop } = req.body

    const result = {
      success: false
    }
    let status = HTTP_STATUS.FORBIDDEN

    if (!email || !password) {
      result.error_code = 'error/field-required'
      result.error_message = 'Email and Password is required'
      return res.status(status).json(result)
    }

    let uid
    try {
      const { user } = await firebase.auth().signInWithEmailAndPassword(email, password)
      uid = user.uid

      const shopDocRef = await Shopify.doc(shop)
      const shopDocSnapshot = await shopDocRef.get()
      const shopDoc = shopDocSnapshot.data()
      if (shopDoc.uid) {
        throw new Error('Account was connected earlier')
      }
      await shopDocRef.update({ uid: uid })

      result.success = true
      status = HTTP_STATUS.OK
    } catch (error) {
      result.success = false
      result.error_code = error.code
      result.error_message = error.message
    }

    return res.status(status).json(result)
  })

  /**
   * @swagger
   *  /shopify/saveSettings:
   *    post:
   *      tags:
   *        - Application
   */
  router.post('/saveSettings', async (req, res) => {
    // TODO !!! add auth check
    const {
      customerCreatedCampaignId,
      customerCreatedNotifyActive,
      orderCreatedCampaignId,
      orderCreatedMinTotalPrice,
      orderCreatedNotifyActive,
      checkoutsAbandonedCampaignId,
      checkoutsAbandonedNotifyActive,
      shop,
      onCustomerCreate,
      onOrderCreate
    } = req.body

    const promises = []

    const shopDocRef = await Shopify.doc(shop)
    const shopDocSnapshot = await shopDocRef.get()
    if (!shopDocSnapshot.exists) {
      return res.status(HTTP_STATUS.NOT_FOUND).json({ success: false, error_message: 'shop is not found' })
    }
    const shopDoc = shopDocSnapshot.data()
    const { accessToken, onCustomerCreateWebhookId, onOrderCreateWebhookId } = shopDoc

    const shopDataToUpdate = {}

    // Test

    if (
      typeof onCustomerCreate !== 'undefined' &&
      shopDoc.customerCreatedCampaignId !== customerCreatedCampaignId
    ) {
      Object.assign(shopDataToUpdate, { onCustomerCreate })
    }

    if (
      typeof onOrderCreate !== 'undefined' &&
      shopDoc.orderCreatedCampaignId !== orderCreatedCampaignId
    ) {
      Object.assign(shopDataToUpdate, { onOrderCreate })
    }

    // End test

    if (
      typeof customerCreatedCampaignId !== 'undefined' &&
      shopDoc.customerCreatedCampaignId !== customerCreatedCampaignId
    ) {
      Object.assign(shopDataToUpdate, { customerCreatedCampaignId })
    }

    if (typeof orderCreatedCampaignId !== 'undefined' && shopDoc.orderCreatedCampaignId !== orderCreatedCampaignId) {
      Object.assign(shopDataToUpdate, { orderCreatedCampaignId })
    }

    if (
      typeof orderCreatedMinTotalPrice !== 'undefined' &&
      shopDoc.orderCreatedMinTotalPrice !== orderCreatedMinTotalPrice
    ) {
      const minTotalPrice = orderCreatedMinTotalPrice === '0' ? '' : orderCreatedMinTotalPrice
      Object.assign(shopDataToUpdate, { orderCreatedMinTotalPrice: minTotalPrice })
    }

    if (
      typeof checkoutsAbandonedCampaignId !== 'undefined' &&
      shopDoc.checkoutsAbandonedCampaignId !== checkoutsAbandonedCampaignId
    ) {
      Object.assign(shopDataToUpdate, { checkoutsAbandonedCampaignId })
    }

    if (
      typeof checkoutsAbandonedNotifyActive !== 'undefined' &&
      shopDoc.checkoutsAbandonedNotifyActive !== checkoutsAbandonedNotifyActive
    ) {
      if (
        !checkoutsAbandonedNotifyActive ||
        ((shopDoc.checkoutsAbandonedCampaignId && typeof checkoutsAbandonedCampaignId === 'undefined') ||
          checkoutsAbandonedCampaignId)
      ) {
        const dataToSave = {
          checkoutsAbandonedNotifyActive,
          createdSince: checkoutsAbandonedNotifyActive ? new Date() : null
        }
        Object.assign(shopDataToUpdate, dataToSave)
      }
    }

    const processWebhook = (
      { active, storedCampaignId, newCampaignId, createWebhook, deleteWebhook, storedWebhookId, storedWebhookIdName }
    ) => {
      if (active && (storedCampaignId || newCampaignId)) {
        return createWebhook({ shop, accessToken, apiVersion }).then(webhookCreateResult => {
          const id = webhookCreateResult && webhookCreateResult.id
          if (!id) return {}
          return { [storedWebhookIdName]: id }
        })
      } else if (typeof active !== 'undefined' && storedWebhookId) {
        return deleteWebhook({ shop, accessToken, webhookId: storedWebhookId, apiVersion }).then(() => ({
          [storedWebhookIdName]: null
        }))
      }

      return Promise.resolve({})
    }

    {
      const promise = processWebhook({
        active: customerCreatedNotifyActive,
        storedCampaignId: shopDoc.customerCreatedCampaignId,
        newCampaignId: customerCreatedCampaignId,
        createWebhook: createOnCustomerCreateWebhook,
        deleteWebhook,
        storedWebhookId: onCustomerCreateWebhookId,
        storedWebhookIdName: 'onCustomerCreateWebhookId'
      })
      promises.push(promise)
    }

    {
      const promise = processWebhook({
        active: orderCreatedNotifyActive,
        storedCampaignId: shopDoc.orderCreatedCampaignId,
        newCampaignId: orderCreatedCampaignId,
        createWebhook: createOnOrderCreateWebhook,
        deleteWebhook,
        storedWebhookId: onOrderCreateWebhookId,
        storedWebhookIdName: 'onOrderCreateWebhookId'
      })
      promises.push(promise)
    }

    try {
      const webhookProcessingResult = await Promise.all(promises)
      webhookProcessingResult.forEach(r => Object.assign(shopDataToUpdate, r))

      if (Object.keys(shopDataToUpdate).length) {
        shopDocRef.update(shopDataToUpdate).catch(error => {
          console.error(
            `Webhooks processing has been finished, but shop record was not updated because of error: ${error}`
          )
        })
      }
    } catch (error) {
      console.error(`Saving settings error: `, error)
      return res
        .status(HTTP_STATUS.INTERNAL_SERVER_ERROR)
        .json({ success: false, error_message: 'Saving settings error' })
    }

    return res.status(HTTP_STATUS.OK).json({ success: true })
  })

  /**
   * @swagger
   *  /shopify/userCampaigns:
   *    get:
   *      tags:
   *        - Application
   */
  router.get('/userCampaigns', async (req, res) => {
    // TODO !!! add auth check
    const shop = req.query.shop

    if (!shop) {
      return res.status(HTTP_STATUS.BAD_REQUEST).json({ success: false, error_message: 'shop is required' })
    }
    const shopDocSnapshot = await Shopify.doc(shop).get()

    if (!shopDocSnapshot.exists) {
      return res.status(HTTP_STATUS.NOT_FOUND).json({ success: false, error_message: 'shop is not found' })
    }
    const shopDoc = shopDocSnapshot.data()

    if (!shopDoc.uid) {
      const error_message = 'shop is not connected to service account'
      return res.status(HTTP_STATUS.BAD_REQUEST).json({ success: false, error_message })
    }

    const uid = shopDoc.uid

    let campaigns
    try {
      campaigns = await apiFunctions.getUserCampaigns(uid)
    } catch (error) {
      console.error(`Saving settings error: ${error}`)
      return res
        .status(HTTP_STATUS.INTERNAL_SERVER_ERROR)
        .json({ success: false, error_message: 'Saving settings error' })
    }

    return res.json({ success: true, campaigns })
  })

  /**
   * @swagger
   *  /shopify/settings:
   *    get:
   *      tags:
   *        - Application
   */
  router.get(SETTINGS_ROUTE, async (req, res) => {
    const state = nonce()

    res.cookie('state', state, { httpOnly: true })

    const shop = req.query.shop

    try {
      if (verifyOAuth(req.query)) {
        const shopDocRef = await Shopify.doc(shop)
        const shopDocSnapshot = await shopDocRef.get()

        if (!shopDocSnapshot.exists) {
          // to install
          const query = Object.keys(req.query)
            .map(key => `${key}=${req.query[key]}`)
            .join('&')
          return res.redirect(`${BASE_ROUTE + INSTALL_ROUTE}?${query}`)
        }

        const queryEndpoint = Object.keys(req.query)
          .map(key => `${key}=${req.query[key]}`)
          .join('&')

        const oneHour = 3600000
        res.cookie('query', queryEndpoint, { expires: new Date(Date.now() + oneHour), httpOnly: true })

        const shopDoc = shopDocSnapshot.data()
        const accountConnected = Boolean(shopDoc.uid)

        let customerCreatedCampaignId = ''
        let orderCreatedCampaignId = ''
        let checkoutsAbandonedCampaignId = ''
        let campaigns = []
        let customerCreatedNotifyActive = false
        let orderCreatedNotifyActive = false
        let checkoutsAbandonedNotifyActive = false
        let createdSince

        let onCustomerCreate = false
        let onOrderCreate = false

        let orderCreatedMinTotalPrice = ''

        const shopDataToUpdate = {}

        if (accountConnected) {
          ({
            customerCreatedCampaignId,
            orderCreatedCampaignId,
            orderCreatedMinTotalPrice = '',
            checkoutsAbandonedCampaignId,
            checkoutsAbandonedNotifyActive,
            createdSince = Date(),
            onOrderCreate,
            onCustomerCreate
          } = shopDoc)

          const { shop, uid, accessToken } = shopDoc
          const promises = [
            apiFunctions.getUserCampaigns(uid),
            isCustomerCreatedNotifyActive({ shop, accessToken, apiVersion }),
            isOrderCreatedNotifyActive({ shop, accessToken, apiVersion })
          ]
          ;[campaigns = [], customerCreatedNotifyActive, orderCreatedNotifyActive] = await Promise.all(promises)

          // check if current campaignId is present in service
          if (!campaigns.find(({ id }) => id === customerCreatedCampaignId)) {
            customerCreatedCampaignId = ''
            shopDataToUpdate.customerCreatedCampaignId = null
          }

          if (!campaigns.find(({ id }) => id === orderCreatedCampaignId)) {
            orderCreatedCampaignId = ''
            shopDataToUpdate.orderCreatedCampaignId = null
          }

          if (!campaigns.find(({ id }) => id === checkoutsAbandonedCampaignId)) {
            checkoutsAbandonedCampaignId = ''
            shopDataToUpdate.checkoutsAbandonedCampaignId = null
            checkoutsAbandonedNotifyActive = false
            shopDataToUpdate.checkoutsAbandonedNotifyActive = false
          }
        }

        if (shopDoc.onCustomerCreateWebhookId && !customerCreatedNotifyActive) {
          shopDataToUpdate.onCustomerCreateWebhookId = null
        }

        if (shopDoc.onOrderCreateWebhookId && !orderCreatedNotifyActive) {
          shopDataToUpdate.onOrderCreateWebhookId = null
        }

        if (Object.keys(shopDataToUpdate).length) {
          shopDocRef.update(shopDataToUpdate).catch(error => {
            console.error(`Shop record updating error: ${error}`)
          })
        }

        const customerCreatedCampaignsOptions = [
          `<option disabled ${customerCreatedCampaignId ? '' : 'selected'} >Campaign ID</option>`
        ]
        campaigns.forEach(({ id, title }) => {
          const opt = `<option value="${id}" ${id === customerCreatedCampaignId ? 'selected' : ''}>${title}</option>`
          customerCreatedCampaignsOptions.push(opt)
        })

        const orderCreatedCampaignsOptions = [
          `<option disabled ${orderCreatedCampaignId ? '' : 'selected'} >Campaign ID</option>`
        ]
        campaigns.forEach(({ id, title }) => {
          const opt = `<option value="${id}" ${id === orderCreatedCampaignId ? 'selected' : ''}>${title}</option>`
          orderCreatedCampaignsOptions.push(opt)
        })

        const checkoutsAbandonedCampaignsOptions = [
          `<option disabled ${checkoutsAbandonedCampaignId ? '' : 'selected'} >Campaign ID</option>`
        ]
        campaigns.forEach(({ id, title }) => {
          const opt = `<option value="${id}" ${id === checkoutsAbandonedCampaignId ? 'selected' : ''}>${title}</option>`
          checkoutsAbandonedCampaignsOptions.push(opt)
        })

        const vars = {
          debug: Boolean(testing),
          shopOrigin: shop,
          apiKey,
          serviceAddress,
          newCampaignUrl: `${frontUrl}/stationery/new/stationery`,
          appName,
          scope,
          state,
          accountConnected,

          connectAccountSectionClass: accountConnected ? 'hidden' : '',
          notificationPreferenceSectionClass: campaigns.length ? '' : 'hidden',
          settingsSectionClass: accountConnected ? '' : 'hidden',
          customerCreatedCampaignsOptions,
          orderCreatedCampaignsOptions,
          customerCreatedCampaignId,
          customerCreatedNotifyActive,
          orderCreatedCampaignId,
          orderCreatedMinTotalPrice,
          orderCreatedNotifyActive,

          onOrderCreate,
          onCustomerCreate,

          checkoutsAbandonedCampaignsOptions,
          checkoutsAbandonedCampaignId,
          checkoutsAbandonedNotifyActive,
          createdSince:
            createdSince && typeof createdSince === 'object' && createdSince.toDate
              ? createdSince.toDate()
              : createdSince,

          customerCreatedNotifyActiveChecked: onCustomerCreate ? 'checked' : '',
          customerCreatedNotifyActiveDisabled: accountConnected && customerCreatedCampaignId ? '' : 'disabled',

          orderCreatedNotifyActiveChecked: onOrderCreate ? 'checked' : '',
          orderCreatedMinTotalPriceDisabled: accountConnected && orderCreatedCampaignId ? '' : 'disabled',
          orderCreatedNotifyActiveDisabled: accountConnected && orderCreatedCampaignId ? '' : 'disabled',

          checkoutsAbandonedNotifyActiveChecked: checkoutsAbandonedNotifyActive ? 'checked' : '',
          checkoutsAbandonedNotifyActiveDisabled: accountConnected && checkoutsAbandonedCampaignId ? '' : 'disabled'
        }
        return res.render('app', vars)
      } else {
        return res.render('index', { title: req.query.shop })
      }
    } catch (error) {
      const vars = { message: 'Something went wrong!', error: testing ? error : {} }
      return res.render('error', vars)
    }
  })

  /**
   * @swagger
   *  /shopify:
   *    get:
   *      tags:
   *        - Application
   */
  router.get(INSTALL_ROUTE, async (req, res) => {
    const shop = req.query.shop
    if (shop) {
      const initialAddress = serviceAddress + '/shopify/settings/?'
      const queryCookie = cookie.parse(req.headers.cookie || '').query

      const shopDocRef = await Shopify.doc(shop)
      const shopDoc = await shopDocRef.get()

      if (queryCookie && shopDoc.exists) {
        return res.redirect(initialAddress + queryCookie)
      } else {
        const state = nonce()
        const redirectUri = serviceAddress + BASE_ROUTE + CALLBACK_ROUTE
        const installUrl =
          'https://' +
          shop +
          '/admin/oauth/authorize?client_id=' +
          apiKey +
          '&scope=' +
          scope +
          '&state=' +
          state +
          '&redirect_uri=' +
          redirectUri

        res.cookie('state', state, { httpOnly: true })
        return res.redirect(installUrl)
      }
    } else {
      return res
        .status(HTTP_STATUS.BAD_REQUEST)
        .send('Missing shop parameter. Please add ?shop=your-development-shop.myshopify.com to your request')
    }
  })

  /**
   * @swagger
   *  /shopify/callback:
   *    get:
   *      tags:
   *        - Application
   */
  router.get(CALLBACK_ROUTE, async (req, res) => {
    const { shop, hmac, code, state } = req.query
    const stateCookie = cookie.parse(req.headers.cookie || '').state

    if (stateCookie && state !== stateCookie) {
      return res.status(HTTP_STATUS.FORBIDDEN).send('Request origin cannot be verified')
    }

    try {
      if (shop && hmac && code) {
        const map = Object.assign({}, req.query)
        delete map['signature']
        delete map['hmac']
        const message = querystring.stringify(map)
        const providedHmac = Buffer.from(hmac, 'utf-8')
        const generatedHash = Buffer.from(
          crypto
            .createHmac('sha256', apiSecret)
            .update(message)
            .digest('hex'),
          'utf-8'
        )
        let hashEquals = false

        try {
          hashEquals = crypto.timingSafeEqual(generatedHash, providedHmac)
        } catch (e) {
          hashEquals = false
        }

        if (!hashEquals) {
          return res.status(HTTP_STATUS.NOT_FOUND).send('HMAC validation failed')
        }

        const shopDocRef = await Shopify.doc(shop)
        const shopDoc = await shopDocRef.get()

        if (shopDoc.exists) {
          // how often we should receive new access token?
          const state = nonce()
          const redirectUri = serviceAddress + BASE_ROUTE + SETTINGS_ROUTE
          const installUrl =
            'https://' +
            shop +
            '/admin/oauth/authorize?client_id=' +
            apiKey +
            '&scope=' +
            scope +
            '&state=' +
            state +
            '&redirect_uri=' +
            redirectUri

          res.cookie('state', state, { httpOnly: true })
          return res.redirect(installUrl)
        } else {
          const accessTokenResponse = await obtainAccessToken(shop, code)
          const { access_token: accessToken, scope } = accessTokenResponse

          const newShopData = {
            shop,
            scope,
            accessToken,
            innerShopId: Date.now()
          }

          await Shopify.doc(shop).set(newShopData)

          createOnDeleteWebhook({ shop, accessToken, apiVersion, innerShopId: newShopData.innerShopId }).catch(
            error => {
              console.error(`Creating on delete app webhook error: ${error}`)
            }
          )

          return res.redirect(`https://${shop}/admin/apps/${appName || ''}`)
        }
      } else {
        return res.status(HTTP_STATUS.NOT_FOUND).send('Required parameters missing')
      }
    } catch (error) {
      const vars = { message: 'Something went wrong!', error: testing ? error : {} }
      return res.render('error', vars)
    }
  })

  /**
   * @swagger
   *  /shopify/add_recipient:
   *    post:
   *      tags:
   *        - Flow Application
   */
  router.post('/add_recipient', async (req, res) => {
    // const result = {
    //   success: false
    // }
    console.log(req.body)

    try {
      let api_key = req.body.properties.api_key

      if (!req.body.properties.campaign_id) {
        console.error(`Invalid Campaign ID`)
        return res
          .status(HTTP_STATUS.FORBIDDEN)
          .json({ success: false, error_message: `Invalid Campaign ID`, error_code: 'error/unauthorised' })
      }

      if (!api_key) {
        console.error(`Invalid API key`)
        return res
          .status(HTTP_STATUS.FORBIDDEN)
          .json({ success: false, error_message: `Invalid API key`, error_code: 'error/unauthorised' })
      }

      const uid = api_key.split('$')[0]
      try {
        user = await utils.get_user_profile(uid)
      } catch (error) {
        console.error(error)
        return res.status(HTTP_STATUS.FORBIDDEN).json(errorResponse)
      }

      if (user.api_key !== api_key) {
        console.error(`Invalid API key`, user.api_key)
        return res
          .status(HTTP_STATUS.FORBIDDEN)
          .json({ success: false, error_message: `Invalid API key`, error_code: 'error/unauthorised' })
      }

      const recipient = {
        'zip/postal code': req.body.properties.zip || null,
        city: req.body.properties.city || null,
        'first name': req.body.properties.first_name || null,
        'last name': req.body.properties.last_name || null,
        country: req.body.properties.country_code_v2 || null,
        company: req.body.properties.company || '',
        'state/region': req.body.properties.province || '',
        'address line 1': req.body.properties.address_1 || null,
        'address line 2': req.body.properties.address_2 || ''
      }

      const result = await apiFunctions.add_recipients(req.body.properties.campaign_id, [recipient], uid, undefined, constants.RECIPIENT_SOURCE.shopify_flow)

      if (!result.success) {
        let message = `A shopify contact has insufficent details to send a letter: "${result.error_message}". Please update the contact at https://${req.query.shop}.myshopify.com/admin/customers/${req.body.id} and retrigger the letter`
        await notifications.add(uid, constants.NOTIFICATION_LEVEL.error, 'Invalid Shopify contact', message)
      }

      let status = HTTP_STATUS.FORBIDDEN

      result.success = true
      status = HTTP_STATUS.OK

      return res.status(status).json(result)
    } catch (error) {
      console.error(`Shopify Webhook "POST /add_recipient": Sending handwriting post card error: `, error)
      return res.status(HTTP_STATUS.INTERNAL_SERVER_ERROR).json({ success: false })
    }
  })

  return router
}
