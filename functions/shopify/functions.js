const functions = require('firebase-functions')
const ShopifyAPI = require('shopify-api-node')
const { SHOPIFY:
  { BASE_ROUTE, WEBHOOK_ROUTE }
} = require('../shared/constants')
const configs = functions.config()

const {
  service_address: serviceAddress
} = configs.shopify

const webhookRoute = BASE_ROUTE + WEBHOOK_ROUTE

async function checkIfWebhookIsCreated ({ shop, accessToken, apiVersion, topic }) {
  const shopAPI = new ShopifyAPI({
    shopName: shop,
    accessToken,
    apiVersion
  })

  const webhookParams = {
    topic,
    format: 'json'
  }
  const webhooks = await shopAPI.webhook.list(webhookParams)

  return Boolean(webhooks.length) // TODO is this a bug?
}

async function isCustomerCreatedNotifyActive ({ shop, accessToken, apiVersion }) {
  const topic = 'customers/create'
  return checkIfWebhookIsCreated({shop, accessToken, apiVersion, topic})
}

async function isOrderCreatedNotifyActive ({ shop, accessToken, apiVersion }) {
  const topic = 'orders/create'
  return checkIfWebhookIsCreated({shop, accessToken, apiVersion, topic})
}

async function createWebhook ({ shop, accessToken, apiVersion, topic, address }) {
  const shopAPI = new ShopifyAPI({
    shopName: shop,
    accessToken,
    apiVersion
  })
  const webhookParams = {
    topic,
    address,
    format: 'json'
  }
  return shopAPI.webhook.create(webhookParams)
}

async function createOnCustomerCreateWebhook ({ shop, accessToken, apiVersion }) {
  const topic = 'customers/create'
  const address = `${serviceAddress}${webhookRoute}/customers/create/`
  return createWebhook({shop, accessToken, apiVersion, topic, address})
}

function createOnOrderCreateWebhook ({ shop, accessToken, apiVersion }) {
  const topic = 'orders/create'
  const address = `${serviceAddress}${webhookRoute}/orders/create/`
  return createWebhook({shop, accessToken, apiVersion, topic, address})
}

async function deleteWebhook ({ shop, accessToken, apiVersion, webhookId }) {
  const shopAPI = new ShopifyAPI({
    shopName: shop,
    accessToken,
    apiVersion
  })
  return shopAPI.webhook.delete(webhookId)
}

// add webhook on delete shop
// add webhook on delete app
async function createOnDeleteWebhook ({shop, accessToken, apiVersion, innerShopId}) {
  const shopAPI = new ShopifyAPI({
    shopName: shop,
    accessToken,
    apiVersion
  })

  const webhookParams = {
    topic: 'app/uninstalled',
    address: `${serviceAddress}${webhookRoute}/app/uninstalled/?innerShopId=${innerShopId}`,
    format: 'json',
    fields: ['domain', 'myshopify_domain']
  }
  return shopAPI.webhook.create(webhookParams)
}

// TODO these are not used, remove?
function createOnCustomersRedactWebhook ({ shop, accessToken, apiVersion }) {
  const topic = 'customers/redact'
  const address = `${serviceAddress}${webhookRoute}/${topic}/`
  return createWebhook({shop, accessToken, apiVersion, topic, address})
}

// TODO these are not used, remove?
function createOnShopRedactWebhook ({ shop, accessToken, apiVersion }) {
  const topic = 'shop/redact'
  const address = `${serviceAddress}${webhookRoute}/${topic}/`
  return createWebhook({shop, accessToken, apiVersion, topic, address})
}

// TODO these are not used, remove?
function createOnCustomersDataRequestWebhook ({ shop, accessToken, apiVersion }) {
  const topic = 'customers/data_request'
  const address = `${serviceAddress}${webhookRoute}/${topic}/`
  return createWebhook({shop, accessToken, apiVersion, topic, address})
}

async function getAbandonedCheckouts ({ shop, accessToken, apiVersion, requestParams = {} }) {
  const shopAPI = new ShopifyAPI({
    shopName: shop,
    accessToken,
    apiVersion
  })
  const {
    limit = 250,
    sinceId: since_id,
    createdAtMin: created_at_min,
    createdAtMax: created_at_max,
    updatedAtMin: updated_at_min,
    updatedAtMax: updated_at_max,
    status
  } = requestParams
  const params = {
    limit,
    since_id,
    created_at_min, // Show checkouts created after the specified date. (format: 2014-04-25T16:15:47-04:00)
    created_at_max,
    updated_at_min,
    updated_at_max,
    status
  }

  return shopAPI.checkout.list(params)
}

module.exports = {
  createOnCustomerCreateWebhook,
  isCustomerCreatedNotifyActive,
  isOrderCreatedNotifyActive,
  createOnDeleteWebhook,
  deleteWebhook,
  createOnOrderCreateWebhook,
  getAbandonedCheckouts,
  createOnCustomersRedactWebhook,
  createOnShopRedactWebhook,
  createOnCustomersDataRequestWebhook
}
