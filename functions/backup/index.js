const functions = require('firebase-functions')
const moment = require('moment')
const { google } = require('googleapis')

const environment = functions.config()

/**
 * Generate backup of the firestore database
 * and upload to the Google Cloud bucket.
 */
const generateBackup = async () => {
  console.log('Called generate backup')
  const client = await google.auth.getClient({
    scopes: [
      'https://www.googleapis.com/auth/datastore',
      'https://www.googleapis.com/auth/cloud-platform' // We need these scopes
    ]
  })
  const timestamp = moment().format('YYYY-MM-DD') // the  name of the folder
  const path = `${timestamp}`
  const BUCKET_NAME = environment.firestore.backup_bucket_name

  const projectId = await google.auth.getProjectId()
  const url = `https://firestore.googleapis.com/v1beta1/projects/${projectId}/databases/(default):exportDocuments`
  const backup_route = `gs://${BUCKET_NAME}/${path}`
  return client
    .request({
      url,
      method: 'POST',
      data: {
        outputUriPrefix: backup_route
        // collectionsIds: [] // if you want to specify which collections to export, none means all
      }
    })
    .then(async res => {
      console.log(`Backup saved on folder on ${backup_route}`)
      return null
    })
    .catch(async e => {
      console.log(BUCKET_NAME, path)
      await console.log(e, { message: e.message })
    })
}

module.exports = generateBackup
