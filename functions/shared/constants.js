const GOOD = {
  full_service_a4_pack: 'Full Service A4 pack',
  full_service_a5_pack: 'Full Service A5 pack',
  full_service_a6_pack: 'Full Service A6 pack',

  self_service_a4: 'Self Service A4',
  self_service_a5: 'Self Service A5',
  self_service_a6: 'Self Service A6',
  self_service_c5: 'Self Service C5',
  self_service_c6: 'Self Service C6',
  self_service_dl: 'Self Service DL',

  full_service_a4: 'Full Service A4',
  full_service_a5: 'Full Service A5',
  full_service_a6: 'Full Service A6',
  full_service_c4: 'Full Service C4',
  full_service_c5: 'Full Service C5',
  full_service_c6: 'Full Service C6',
  full_service_dl: 'Full Service DL',

  insert: 'Insert',
  insert_light: 'Insert (light)',
  stamp_gb_first_class: 'Stamp GB First Class',
  stamp_gb_second_class: 'Stamp GB Second Class',
  stamp_gb_europe: 'Stamp GB Europe',
  stamp_gb_world_zone_1: 'Stamp GB World Zone 1',
  stamp_gb_world_zone_2: 'Stamp GB World Zone 2',
  stamp_us_international: 'Stamp US International',
  stamp_us_first_class: 'Stamp US First Class',
  ship_to_self_us: 'Ship to self (US)',
  ship_to_self_gb: 'Ship to self (GB)',

  membership: 'Membership'
}

const EUROPE_VAT_COUNTRIES = [
  'BE', 'BG', 'HR', 'CZ', 'DK', 'DE', 'EE', 'IE',
  'GR', 'ES', 'FR', 'IT', 'CY', 'LV', 'LT', 'LU',
  'HU', 'MT', 'NL', 'AT', 'PL', 'PT', 'RO', 'SI',
  'SK', 'FI', 'SE', 'GB'
]

const TAX_RATE = {
  gb_20_vat: 'GB 20% VAT',
  gb_0_vat: 'GB 0% VAT',
  outside_eu_0_vat: 'Outside Europe 0% VAT'
}

let currency = {
  gbp_to_usd: 1.2,
  gbp_to_eur: 1.1
}
currency.usd_to_gbp = 1 / currency.gbp_to_usd
currency.eur_to_gbp = 1 / currency.gbp_to_eur
currency.usd_to_eur = currency.gbp_to_eur / currency.gbp_to_usd
currency.eur_to_usd = currency.gbp_to_usd / currency.gbp_to_eur

const ERROR = {
  sample_campaign: 'error/sample-campaign',
  field_required: 'error/field-required',
  invalid_status: 'error/invalid-status',
  invalid_key: 'error/invalid-key',
  character_not_supported: 'error/character-not-supported',
  payment_method_required: 'error/payment-method-required',
  invalid_date: 'error/invalid-date',
  invalid_handwriting_style: 'error/invalid-handwriting-style',
  invalid_font: 'error/invalid-font',
  invalid_paper_size: 'error/invalid-paper-size',
  invalid_product: 'error/invalid-product',
  unauthorised: 'error/unauthorised',
  not_found: 'error/not-found'
}

const RECIPIENT_SOURCE = {
  hubspot: 'HubSpot',
  shopify: 'Shopify',
  shopify_flow: 'Shopify Flow',
  interface: 'Interface',
  api: 'API',
  system: 'System',
  unknown: 'Unknown'
}

const SAMPLE_MESSAGE = `Dear {{{first name}}},

Thanks for requesting a sample to see one of our handwritten notes.

Whether you are sending to prospects, customers or ambassadors, personalise the customer experience and achieve better engagement rates.

 Kind regards,
   Scribeless`

module.exports = {
  SAMPLE_MESSAGE: SAMPLE_MESSAGE,
  BLEED: 4,
  BLEED_MARK_MARGIN: 2,
  ERROR: ERROR,
  CURRENCY: currency,
  MIN_DELIVERY_DATE: 4,
  BILLING_PERIOD_DAYS: 30,
  BILLING_INVOICE_DUE_DAYS: 30,
  RECIPIETS_PER_SHIP_TO_SELF: 1000,
  RECIPIENT_SOURCE: RECIPIENT_SOURCE,
  RETURN_OBJECTS: {
    INTERNAL_SERVER_ERROR: {
      success: false,
      error_message: 'Something went wrong, Please contact the administrator',
      error_code: 'error/internal-server-error'
    }
  },
  CAMPAIGN_STATUS: {
    pending: 'Pending',
    in_progress: 'In progress',
    ready_to_ship: 'Ready to Ship',
    complete: 'Complete',
    error: 'Error',
    deleted: 'Deleted'
  },
  RECIPIENT_STATUS: {
    pending: 'Pending',
    in_progress: 'AI Writing',
    ready_to_print: 'Ready',
    shipped: 'Shipped',
    returned: 'Returned',
    received: 'Received',
    lost: 'Lost',
    error: 'Error',
    deleted: 'Deleted'
  },
  BATCH_STATUS: {
    pending: 'Pending',
    shipped: 'Shipped',
    error: 'Error'
  },
  NOTIFICATION_LEVEL: {
    error: 'error',
    info: 'info'
  },
  NOTIFICATION_STATUS: {
    new: 'new',
    notified: 'notified'
  },
  RECIPIENTS_ERROR: {
    last_name: 0,
    address: 1,
    city: 2,
    quantity: 3,
    postcode: 4,
    country: 5,
    us_state: 6,
    unsupported_chars: 100
  },
  MIN_UNITS: 1,
  MAX_UNITS: 4000,
  PRODUCTION_DAYS: 5,
  PAPER_SIZES: {
    'A4': {'name': 'A4', key: 'A4', 'width': '210', height: '297', orientation: 'portrait', envelope: 'C5', us_standard: 'letter', scale: 0.84, left: 4, top: 0, iso_standard: null, scale_back: 0.86, back_left: 0, handwriting_image_size_override: 800},
    'A5': {'name': 'A5', key: 'A5', 'width': '148', height: '210', orientation: 'portrait', envelope: 'C5', us_standard: 'half_letter', scale: 0.84, left: 4, top: 0, iso_standard: null, scale_back: 0.86, back_left: 0, handwriting_image_size_override: 530},
    'A5_landscape': {'name': 'A5', key: 'A5_landscape', 'width': '210', height: '148', orientation: 'landscape', envelope: 'C5', us_standard: 'half_letter_landscape', scale: 0.84, left: 4, top: 0, iso_standard: null, scale_back: 0.86, back_left: 0, handwriting_image_size_override: 350},
    'A6_landscape': {'name': 'A6', key: 'A6_landscape', 'width': '148', height: '105', orientation: 'landscape', envelope: 'C5', us_standard: 'postcard_landscape', scale: 0.97, top: 0, left: 0, iso_standard: null, scale_back: 0.98, back_left: 4},
    'A6': {'name': 'A6', key: 'A6', 'width': '105', height: '148', orientation: 'portrait', envelope: 'C5', us_standard: 'postcard', scale: 0.97, top: 3, left: 0, iso_standard: null, scale_back: 0.98, back_left: 0},
    'A7_landscape': {'name': 'A7', key: 'A7_landscape', 'width': '105', height: '74', orientation: 'landscape', envelope: null, scale: 0.97, top: 0, left: 0, iso_standard: null, scale_back: 0.98, back_left: 0},
    'half_letter_landscape': {'name': 'Half Letter', key: 'half_letter_landscape', width: '177.8', height: '127', orientation: 'landscape', envelope: 'NAS_A7', iso_standard: 'A5_landscape', scale: 1.16, scale_back: 1.16, left: 0, top: 0, us_standard: null, back_left: 0},
    'half_letter': {'name': 'Half Letter', key: 'half_letter', width: '127', height: '177.8', orientation: 'portrait', envelope: 'NAS_A7', iso_standard: 'A5', scale: 1.16, left: 0, top: 0, us_standard: null, scale_back: 1.16, back_left: 0},
    'letter': {'name': 'Letter', key: 'letter', width: '177.8', height: '254', orientation: 'portrait', envelope: 'NAS_A7', iso_standard: 'A4', scale: 1.16, left: 0, top: 0, us_standard: null, scale_back: 1.16, back_left: 0},
    'postcard_landscape': {'name': 'Postcard', key: 'postcard_landscape', width: '152.4', height: '101.6', orientation: 'landscape', envelope: 'NAS_A4', iso_standard: 'A6_landscape', scale: 0.97, left: 0, top: 0, us_standard: null, scale_back: 0.99, back_left: 0},
    'postcard': {'name': 'Postcard', key: 'postcard', width: '101.6', height: '152.4', orientation: 'portrait', envelope: 'NAS_A4', iso_standard: 'A6', scale: 0.97, left: 0, top: 0, us_standard: null, scale_back: 1.02, back_left: 0},
  },
  ENVELOPE_SIZES: {
    'DL': {name: 'DL', key: 'DL', width: '220', height: '110', us_standard: null, iso_standard: null},
    'C5': {name: 'C5', key: 'C5', width: '229', height: '162', us_standard: 'NAS_A7', iso_standard: null, paper: 'A5_landscape'},
    'C6': {name: 'C6', key: 'C6', width: '162', height: '114', us_standard: 'NAS_A4', iso_standard: null, paper: 'A6_landscape'},
    'NAS_A7': {name: 'A7', key: 'NAS_A7', width:'184.15', height: '133.35', us_standard: null, iso_standard: 'C5', paper: 'half_letter_landscape'},
    'NAS_A4': {name: 'A4', key: 'NAS_A4', width:'158.75', height: '107.95', us_standard: null, iso_standard: 'C6', paper: 'postcard_landscape'},
    'Sticker': {name: 'Sticker', key: 'Sticker', width: '99', height: '38', us_standard: null, iso_standard: null}
  },
  HANDWRITING_SIZE: {
    5: 'Small',
    6: 'Medium',
    7: 'Large'
  },
  INVOICE_STATUS: {
    'draft': 'draft',
    'open': 'open',
    'paid': 'paid',
    'uncollectible': 'uncollectible',
    'void': 'void'
  },
  //TODO: Uncomment Jane after adding Jane font from API
  HANDWRITING_STYLE: {
    'George': 'George', // Whitewell
    // 'Jane': 'Jane', // Autumn 5WGWVZ1G00XB
    
    'Ernest': 'Ernest', // Barrow
    'Mark': 'Mark', // Selfridge
    'Virginia': 'Virginia', // Thama
    'William': 'William', // Hampton

    'new_Lytle': 'Lytle',
    'new_Parker': 'Parker',
    'new_Usther': 'Usther',
    'new_Williams': 'Williams',
    'new_Kunjar': 'Kunjar',
    'new_Stafford': 'Stafford',
    'new_Tremblay': 'Tremblay',
    'new_Ginsberg': 'Ginsberg',
    'new_George': 'George',
    'new_Foster': 'Foster',

    'Whitman': 'Whitman', // Autumn, 5WGWVZ1G00XB
    'Dickinson': 'Dickinson', // Irwin, 8X3WQ64R00B2
    'Plath': 'Plath', // Abbey, 8X3WQ4D800B0
    'Ginsberg': 'Ginsberg', // Lafayette, 5WGWVY9R00WY
    'Emerson': 'Emerson', // Dumont, 8X3WPWHG00AQ

    'Peyton': 'Peyton',
    'Easley': 'Easley',
    'Lytle': 'Lytle',
    'Parker': 'Parker',
    'Brady': 'Brady',
    'Sawdon': 'Sawdon',
    'Sutton': 'Sutton',
    'Tremblay': 'Tremblay',
    'Abrahim': 'Abrahim',
    'Mabus': 'Mabus',

    'Simon': 'Simon',
    'Freud': 'Freud',
    'Sellers': 'Sellers',
    'Chaplin': 'Chaplin',
    'Robinson': 'Robinson',
    'Thomas': 'Thomas'
  },
  FONTS: {
    'josefin': 'Josefin Sans',
    'playfair': 'Playfair Display',
    'poiret': 'Poiret One',
    'Josefin Sans': 'Josefin Sans',
    'Playfair Display': 'Playfair Display',
    'Poiret One': 'Poiret One'
  },
  HEADER_TYPE: {
    'text': 'Text',
    'logo': 'Logo'
  },
  STAMP_TYPES: {
    'first_class': 'First Class',
    'no_stamp': 'No Stamp'
  },
  DATA_SOURCE: {
    'upload': 'Upload',
    'api_crm': 'API / CRM',
    'audience': 'Audience',
  },
  ENVELOPE_REQUIRED: {
    'yes': 'Envelope',
    'no': 'No Envelope'
  },
  SENDER: {
    'ship_for_me': 'Ship for me',
    'ship_to_self': 'Ship to self'
  },
  DELAY_DELIVERY_DAYS: {
    0: '0 days',
    1: '1 day',
    2: '2 days',
    3: '3 days',
    4: '4 days',
    5: '5 days',
    6: '6 days',
    7: '7 days',
    8: '8 days',
    9: '9 days',
    10: '10 days',
    11: '11 days',
    12: '12 days',
    13: '13 days',
    14: '14 days',
    15: '15 days',
    16: '16 days',
    17: '17 days',
    18: '18 days',
    19: '19 days',
    20: '20 days'
  },
  PRODUCT: {
    'self_service': 'Self Service',
    'full_service': 'Full Service',
    'full_service_note_only': 'Full Service Note Only',
    'full_service_envelope_only': 'Full Service Envelope Only'
  },
  INSERTS: {
    // 'amazon_5_dollars': 357,
    // 'amazon_10_dollars': 714,
    // 'amazon_15_dollars': 1071,
    // 'amazon_20_dollars': 1428,
    // 'visa_5_dollars': 357,
    // 'visa_10_dollars': 714,
    // 'visa_15_dollars': 1071,
    // 'visa_20_dollars': 1428,
    'starbucks_5_dollars': '$5 Starbucks gift card',
    'starbucks_10_dollars': '$10 Starbucks gift card',
    'starbucks_15_dollars': '$15 Starbucks gift card'
    // 'starbucks_20_dollars': 1428,
  },
  STYLE_LOOKUP: {
    'Jane': '5WGWVZ1G00XB', // Autumn
    'Ernest': '7EQKAXTR00FR', // Barrow
    'Mark': '62AEE4XG00PS', // Selfridge
    'Virginia': '8X3WQ2ER00AY', // Thama
    'William': '8V2RBTWG008P', // Hampton
    'George': '31SB2CWG00DZ', // Whitewell
    'Whitman': '5WGWVZ1G00XB', // Autumn,
    'Dickinson': '8X3WQ64R00B2', // Irwin,
    'Plath': '8X3WQ4D800B0', // Abbey,
    'Ginsberg': '5WGWVY9R00WY', // Lafayette,
    'Emerson': '8X3WPWHG00AQ', // Dumont,
    'Peyton': '8X3WQ4D800B0',
    'Easley': '8X3WPWHG00AQ',
    'Lytle': '5WGWVWFG00VZ',
    'Parker': '5WGWVXNR00WK',
    'Brady': '7EQKAXTR00FR',
    'Sawdon': '2D5S46JG0004',
    'Sutton': '62AEE4XG00PS',
    'Tremblay': '31SAZEF000DX',
    'Abrahim': '7EQMJ1K800H8',
    'Mabus': '5WGWVXG800WG',
    // 'Tonia': '$5' // Handwriting Engine test
  },
  MARGIN: 6, // mm
  MM_IN_PX: 0.0393701,
  GOOD: GOOD,
  TAX_RATE: TAX_RATE,
  EUROPE_VAT_COUNTRIES: EUROPE_VAT_COUNTRIES,
  VOID_GOOD_TAX: [ // Nothing VOID
    // GOOD.stamp_gb_first_class,
    // GOOD.stamp_gb_second_class,
    // GOOD.stamp_gb_europe,
    // GOOD.stamp_gb_world_zone_1,
    // GOOD.stamp_gb_world_zone_2,
    // GOOD.stamp_us_international,
    // GOOD.stamp_us_first_class
  ],
  SHOPIFY: {
    BASE_ROUTE: '/shopify',
    WEBHOOK_ROUTE: '/webhook',
    INSTALL_ROUTE: '/',
    CALLBACK_ROUTE: '/callback',
    SETTINGS_ROUTE: '/settings'
  },
  HUBSPOT: {
    BASE_ROUTE: '/hubspot',
    WEBHOOK_ROUTE: '/webhook',
    INSTALL_ROUTE: '/install',
    CALLBACK_ROUTE: '/oauth-callback',
    AUTH_ROUTE: '/auth',
    ERROR_ROUTE: '/error'
  },
  REFERRAL_CREDIT: 500,
}
