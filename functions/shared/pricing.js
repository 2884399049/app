const constants = require('./constants')

/*
 * Split on product, paper size, envelope, stamp, delivery
 */

 /**
  * @typedef {Object} Goods
  * @property {string} key The name of the good
  * @property {string} value The quantity of goods
  */

  /**
   * @typedef {Object.<string>} PriceTable
   * @example
   *
   * {
   *   'Full Service A5': {
   *     goods: [constants.GOOD.full_service_a5],
   *     tiers: {
   *       'usd': {1: 500, 10: 400, 100: 300},
   *       ...
   *     }
   *   },
   *   ...
   * }
   */

/**
 * @typedef {Object} Recipient
 * @property {number} index
 * @property {boolean} testing
 * @property {timestamp} created
 * @property {timestamp} updated
 * @property {string} status
 * @property {string} address line 1
 * @property {string} address line 2
 * @property {string} address line 3
 * @property {string} city
 * @property {string} country
 * @property {string} department
 * @property {string} last name
 * @property {string} first name
 * @property {string} region
 * @property {string} title
 * @property {string} postal_code
 * @property {string} company
 * @property {string} custom 1
 * @property {string} custom 2
 * @property {string} custom 3
 * @property {string} custom 4
 * @property {string} custom 5
 * @property {string} custom 6
 * @property {string} full address
 * @property {string} fallback country
 */

 /**
  * recipient_stamp - Calculates the stamp required for a recipient
  *
  * @param  {Campaign} campaign
  * @param  {<Recipient>} recipient
  * @param  {Goods} goods
  */
 function recipient_stamp (campaign, recipient) {
   // Only for full service
   if (campaign.product !== constants.PRODUCT.full_service) {
     return
   }

   // If nothing required
   if (campaign.envelope.required === constants.ENVELOPE_REQUIRED.no || campaign.envelope.stamp === constants.STAMP_TYPES.no_stamp) {
     return
   }

   let domestic_gb = campaign.envelope.stamp === constants.STAMP_TYPES.first_class ? constants.GOOD.stamp_gb_first_class : constants.GOOD.stamp_gb_second_class

   // Un comment if using UK as default for international stamps
   // let countries_to_stamp = [
   //   {countires: ['GB', 'IM'], stamp: domestic_gb},
   //   {countires: ['US'], stamp: constants.GOOD.stamp_us_first_class},
   //   {countires: ['MX', 'CA'], stamp: constants.GOOD.stamp_us_international},
   //   {countires: ['AL', 'DK', 'KG', 'RU', 'AD', 'EE', 'LV', 'SM', 'AM', 'FO', 'LI', 'RS', 'AT', 'FI', 'LT', 'SK', 'AZ', 'FR', 'LU', 'SI', 'PT', 'GS', 'GE', 'MK', 'ES', 'DE', 'SE', 'BY', 'GI', 'MT', 'CH', 'BE', 'GR', 'MD', 'TJ', 'BA', 'GL', 'MC', 'TR', 'BG', 'HU', 'ME', 'TM', 'IS', 'NL', 'UA', 'IE', 'IT', 'NO', 'UZ', 'HR', 'PL', 'VA', 'CY', 'KZ', 'CZ', 'XK', 'RO'], stamp: constants.GOOD.stamp_gb_europe},
   //   {countires: ['AU', 'IO', 'CX', 'CC', 'CK', 'FJ', 'PF', 'TF', 'PF', 'CC', 'KI', 'MO', 'NR', 'NC', 'NZ', 'NU', 'NF', 'MP', 'PG', 'LA', 'PN', 'SG', 'SB', 'TK', 'TO', 'TV', 'AS', 'WS'], stamp: constants.GOOD.stamp_gb_world_zone_1},
   //   {countires: ['AF', 'AX', 'DZ', 'AO', 'AI', 'AQ', 'AG', 'AR', 'AW', 'BS', 'BH', 'BD', 'BB', 'BZ', 'BJ', 'BM', 'BT', 'BO', 'BW', 'BV', 'BR', 'BN', 'BF', 'BI', 'KH', 'CM', 'CV', 'KY', 'CF', 'TD', 'CL', 'CN', 'CO', 'KM', 'CG', 'CD', 'CR', 'CI', 'CU', 'DJ', 'DM', 'DO', 'EC', 'EG', 'SV', 'GQ', 'ER', 'ET', 'FK', 'GF', 'GA', 'GM', 'GH', 'GD', 'GP', 'GU', 'GT', 'GG', 'GN', 'GW', 'GY', 'HT', 'HM', 'HN', 'HK', 'IN', 'ID', 'IR', 'IQ', 'IL', 'JM', 'JP', 'JE', 'JO', 'KE', 'KP', 'KR', 'KW', 'LB', 'LS', 'LR', 'LY', 'MG', 'MW', 'MY', 'MV', 'ML', 'MH', 'MQ', 'MR', 'MU', 'YT', 'FM', 'MN', 'MS', 'MA', 'MZ', 'MM', 'NA', 'NP', 'AN', 'NI', 'NE', 'NG', 'OM', 'PK', 'PW', 'PS', 'PA', 'PY', 'PE', 'PH', 'PR', 'QA', 'RE', 'RW', 'SH', 'KN', 'LC', 'PM', 'VC', 'ST', 'SA', 'SN', 'SC', 'SL', 'SO', 'ZA', 'LK', 'SD', 'SR', 'SJ', 'SZ', 'SY', 'TW', 'TZ', 'TH', 'TL', 'TG', 'TT', 'TN', 'TC', 'UG', 'AE', 'UM', 'UY', 'VU', 'VE', 'VN', 'VG', 'VI', 'WF', 'EH', 'YE', 'ZM', 'ZW', 'PF', 'CC'], stamp: constants.GOOD.stamp_gb_world_zone_2}
   // ]

   let countries_to_stamp = [
     {countires: ['GB', 'IM'], stamp: domestic_gb},
     {countires: ['US'], stamp: constants.GOOD.stamp_us_first_class},
     {countires: ['AU', 'IO', 'CX', 'CC', 'CK', 'FJ', 'PF', 'TF', 'PF', 'CC', 'KI', 'MO', 'NR', 'NC', 'NZ', 'NU', 'NF', 'MP', 'PG', 'LA', 'PN', 'SG', 'SB', 'TK', 'TO', 'TV', 'AS', 'WS', 'AL', 'DK', 'KG', 'RU', 'AD', 'EE', 'LV', 'SM', 'AM', 'FO', 'LI', 'RS', 'AT', 'FI', 'LT', 'SK', 'AZ', 'FR', 'LU', 'SI', 'PT', 'GS', 'GE', 'MK', 'ES', 'DE', 'SE', 'BY', 'GI', 'MT', 'CH', 'BE', 'GR', 'MD', 'TJ', 'BA', 'GL', 'MC', 'TR', 'BG', 'HU', 'ME', 'TM', 'IS', 'NL', 'UA', 'IE', 'IT', 'NO', 'UZ', 'HR', 'PL', 'VA', 'CY', 'KZ', 'CZ', 'XK', 'RO', 'MX', 'CA', 'AF', 'AX', 'DZ', 'AO', 'AI', 'AQ', 'AG', 'AR', 'AW', 'BS', 'BH', 'BD', 'BB', 'BZ', 'BJ', 'BM', 'BT', 'BO', 'BW', 'BV', 'BR', 'BN', 'BF', 'BI', 'KH', 'CM', 'CV', 'KY', 'CF', 'TD', 'CL', 'CN', 'CO', 'KM', 'CG', 'CD', 'CR', 'CI', 'CU', 'DJ', 'DM', 'DO', 'EC', 'EG', 'SV', 'GQ', 'ER', 'ET', 'FK', 'GF', 'GA', 'GM', 'GH', 'GD', 'GP', 'GU', 'GT', 'GG', 'GN', 'GW', 'GY', 'HT', 'HM', 'HN', 'HK', 'IN', 'ID', 'IR', 'IQ', 'IL', 'JM', 'JP', 'JE', 'JO', 'KE', 'KP', 'KR', 'KW', 'LB', 'LS', 'LR', 'LY', 'MG', 'MW', 'MY', 'MV', 'ML', 'MH', 'MQ', 'MR', 'MU', 'YT', 'FM', 'MN', 'MS', 'MA', 'MZ', 'MM', 'NA', 'NP', 'AN', 'NI', 'NE', 'NG', 'OM', 'PK', 'PW', 'PS', 'PA', 'PY', 'PE', 'PH', 'PR', 'QA', 'RE', 'RW', 'SH', 'KN', 'LC', 'PM', 'VC', 'ST', 'SA', 'SN', 'SC', 'SL', 'SO', 'ZA', 'LK', 'SD', 'SR', 'SJ', 'SZ', 'SY', 'TW', 'TZ', 'TH', 'TL', 'TG', 'TT', 'TN', 'TC', 'UG', 'AE', 'UM', 'UY', 'VU', 'VE', 'VN', 'VG', 'VI', 'WF', 'EH', 'YE', 'ZM', 'ZW', 'PF', 'CC'], stamp: constants.GOOD.stamp_us_international},
   ]

   for (let country_stamp of countries_to_stamp) {
     if (country_stamp.countires.indexOf(recipient.country) > -1) {
       return country_stamp.stamp
     }
   }
 }

/**
 * goods_to_stamps - Calculates the stamps required from a set of goods
 *
 * @param  {Campaign} campaign
 * @param  {Array.<Recipient>} recipients Array of recipiets
 * @param  {Goods} goods
 */
function goods_to_stamps (campaign, recipients, goods) {
  for (let recipient of recipients) {
    let stamp = recipient_stamp(campaign, recipient)
    if (!stamp) {
      continue
    }

    if (!goods[stamp]) {
      goods[stamp] = 0
    }
    goods[stamp] += 1
  }
}

/**
 * campaign_to_goods - Calculates the goods required from a campagin
 *
 * @param  {Campaign} campaign
 * @param  {Array.<Recipient>} recipients
 * @param  {Stationary} stationary
 * @return {Goods}
 */
function campaign_recipient_batch_good (campaign, recipient, stationary) {
  let batch_good = 'other'

  if (campaign.product === constants.PRODUCT.full_service) {
    if (campaign.stationary_required && campaign.envelope.required === constants.ENVELOPE_REQUIRED.yes) {
      if (stationary.paper.name === 'A4' || stationary.paper.name === 'Letter') {
        batch_good = constants.GOOD.full_service_a4_pack
      } else if (stationary.paper.name === 'A5' || stationary.paper.name === 'Half Letter') {
        batch_good = constants.GOOD.full_service_a5_pack
      } else if (stationary.paper.name === 'A6' || stationary.paper.name === 'Postcard') {
        batch_good = constants.GOOD.full_service_a6_pack
      } else {
        console.error(`Cannot find matching good for paper ${stationary.paper.name}`)
      }
    } else if (campaign.stationary_required) {
      if (stationary.paper.name === 'A4' || stationary.paper.name === 'Letter') {
        batch_good = constants.GOOD.full_service_a4
      } else if (stationary.paper.name === 'A5' || stationary.paper.name === 'Half Letter') {
        batch_good = constants.GOOD.full_service_a5
      } else if (stationary.paper.name === 'A6' || stationary.paper.name === 'Postcard') {
        batch_good = constants.GOOD.full_service_a6
      } else {
        console.error(`Cannot find matching good for paper ${stationary.paper.name}`)
      }
    } else if (campaign.envelope.required === constants.ENVELOPE_REQUIRED.yes) {
      if (campaign.envelope.paper.name === 'DL') {
        batch_good = constants.GOOD.full_service_dl
      } else if (campaign.envelope.paper.name === 'C5' || campaign.envelope.paper.name === 'NAS_A7') {
        batch_good = constants.GOOD.full_service_c5
      } else if (campaign.envelope.paper.name === 'Sticker' || campaign.envelope.paper.name === 'C6' || campaign.envelope.paper.name === 'NAS_A4') {
        batch_good = constants.GOOD.full_service_c6
      } else {
        console.error(`Cannot find matching good for envelope paper ${stationary.paper.name}`)
      }
    }
  }

  batch_good = batch_good + '-' + recipient_stamp(campaign, recipient)

  if (campaign.delivery && campaign.delivery.sender === constants.SENDER.ship_to_self) {
    batch_good = batch_good + ' - ship to self - ' + campaign.title
  }

  if (campaign.envelope && campaign.envelope.required === constants.ENVELOPE_REQUIRED.yes && campaign.envelope.inserts) {
    batch_good = batch_good + ' - inserts'
  }

  if (stationary) {
    if (stationary.back_image) {
      batch_good = batch_good + ' - duplex'
    } else {
      batch_good = batch_good + ' - simplex'
    }
  }

  if (campaign.envelope && !campaign.envelope.sealed) {
    batch_good = batch_good + ' - unsealed envelopes'
  }

  return batch_good
}

/**
 * campaign_to_goods - Calculates the goods required from a campagin
 *
 * @param  {Campaign} campaign
 * @param  {Array.<Recipient>} recipients
 * @param  {Stationary} stationary
 * @return {Goods}
 */
function campaign_to_goods (campaign, recipients, stationary) {
  let goods = {}

  if (campaign.product === constants.PRODUCT.self_service) {
    if (campaign.stationary_required) {
      if (stationary.paper.name === 'A4' || stationary.paper.name === 'Letter') {
        goods[constants.GOOD.self_service_a4] = recipients.length
      } else if (stationary.paper.name === 'A5' || stationary.paper.name === 'Half Letter') {
        goods[constants.GOOD.self_service_a5] = recipients.length
      } else if (stationary.paper.name === 'A6' || stationary.paper.name === 'Postcard') {
        goods[constants.GOOD.self_service_a6] = recipients.length
      } else {
        console.error(`Cannot find matching good for paper ${stationary.paper.name}`)
      }
    }

    if (campaign.envelope.required === constants.ENVELOPE_REQUIRED.yes) {
      if (campaign.envelope.paper.name === 'DL') {
        goods[constants.GOOD.self_service_dl] = recipients.length
      } else if (campaign.envelope.paper.name === 'C5' || campaign.envelope.paper.name === 'NAS_A7') {
        goods[constants.GOOD.self_service_c5] = recipients.length
      } else if (campaign.envelope.paper.name === 'Sticker' || campaign.envelope.paper.name === 'C6' || campaign.envelope.paper.name === 'NAS_A4') {
        goods[constants.GOOD.self_service_c6] = recipients.length
      } else {
        console.error(`Cannot find matching good for envelope paper ${stationary.paper.name}`)
      }
    }
  } else if (campaign.product === constants.PRODUCT.full_service) {
    if (campaign.stationary_required && campaign.envelope.required === constants.ENVELOPE_REQUIRED.yes) {
      if (stationary.paper.name === 'A4' || stationary.paper.name === 'Letter') {
        goods[constants.GOOD.full_service_a4_pack] = recipients.length
      } else if (stationary.paper.name === 'A5' || stationary.paper.name === 'Half Letter') {
        goods[constants.GOOD.full_service_a5_pack] = recipients.length
      } else if (stationary.paper.name === 'A6' || stationary.paper.name === 'Postcard') {
        goods[constants.GOOD.full_service_a6_pack] = recipients.length
      } else {
        console.error(`Cannot find matching good for paper ${stationary.paper.name}`)
      }
    } else if (campaign.stationary_required) {
      if (stationary.paper.name === 'A4' || stationary.paper.name === 'Letter') {
        goods[constants.GOOD.full_service_a4] = recipients.length
      } else if (stationary.paper.name === 'A5' || stationary.paper.name === 'Half Letter') {
        goods[constants.GOOD.full_service_a5] = recipients.length
      } else if (stationary.paper.name === 'A6' || stationary.paper.name === 'Postcard') {
        goods[constants.GOOD.full_service_a6] = recipients.length
      } else {
        console.error(`Cannot find matching good for paper ${stationary.paper.name}`)
      }
    } else if (campaign.envelope.required === constants.ENVELOPE_REQUIRED.yes) {
      if (campaign.envelope.paper.name === 'DL') {
        goods[constants.GOOD.full_service_dl] = recipients.length
      } else if (campaign.envelope.paper.name === 'C5' || campaign.envelope.paper.name === 'A7') {
        goods[constants.GOOD.full_service_c5] = recipients.length
      } else if (campaign.envelope.paper.name === 'Sticker' || campaign.envelope.paper.name === 'C6' || campaign.envelope.paper.name === 'A4') {
        goods[constants.GOOD.full_service_c6] = recipients.length
      } else {
        console.error(`Cannot find matching good for envelope paper ${stationary.paper.name}`)
      }
    }
  }

  if (campaign.envelope && campaign.envelope.required === constants.ENVELOPE_REQUIRED.yes && campaign.envelope.inserts) {
    goods[constants.GOOD.insert] = recipients.length * campaign.envelope.inserts.length
  }

  goods_to_stamps(campaign, recipients, goods)

  if (campaign.delivery && campaign.delivery.sender === constants.SENDER.ship_to_self) {
    goods[constants.GOOD.ship_to_self_gb] = Math.ceil(recipients.length / constants.RECIPIETS_PER_SHIP_TO_SELF)
  }

  return goods
}

/**
 * price_good - Calculates the price for a single good
 *
 * @param  {String} currency
 * @param  {PriceTable} price_table
 * @param  {String} good_name
 * @param  {Integer} good_quantity
 * @return {Integer}
 */
function price_good (currency, price_table, good_name, good_quantity, recipients_count) {
  if (good_name === constants.GOOD.membership) {
    return good_quantity
  }

  for (let price_key in price_table) {
    let price = price_table[price_key]
    if (price.goods.indexOf(good_name) > -1) {
      let tiers = price.tiers[currency]
      let selected_tier = Infinity // there should be one of these
      let largest_tier = 0
      for (let tier in tiers) {
        tier = parseInt(tier)
        if (tier >= largest_tier) {
          largest_tier = tier
        }
        if (recipients_count <= tier && tier <= selected_tier) {
          selected_tier = tier
        }
      }

      if (tiers[selected_tier]) {
        return parseInt(tiers[selected_tier]) * good_quantity
      } else {
        return parseInt(tiers[largest_tier]) * good_quantity
      }
    }
  }

  return 0
}

/**
 * Calculates the tax for a good
 * @param {User} user - User object
 * @returns {Object} tax rate
 */
function caluclate_customer_tax_rate (user) {
  if (constants.EUROPE_VAT_COUNTRIES.indexOf(user.country) === -1) {
    return constants.TAX_RATE.outside_eu_0_vat
  }

  return constants.TAX_RATE.gb_20_vat
}

/**
 * Calculates the tax for a good
 * @param {User} user - User object
 * @param  {String} good_name
 * @param {Integer} price
 * @returns {Object} {tax_rate: String, tax: Integer, price: Integer, good: good}
 */
function caluclate_good_tax (user, good_name, price) {
  // Determine the TAX rate for void goods
  let tax_rate = caluclate_customer_tax_rate(user)
  if (tax_rate === constants.TAX_RATE.gb_20_vat && constants.VOID_GOOD_TAX.indexOf(good_name) > -1) {
    tax_rate = constants.TAX_RATE.gb_0_vat
  }

  // Determine the tax value
  let tax_to_pay
  if (tax_rate === constants.TAX_RATE.gb_20_vat) {
    tax_to_pay = price * 0.2
  } else if (tax_rate === constants.TAX_RATE.gb_0_vat) {
    tax_to_pay = price * 0
  } else if (tax_rate === constants.TAX_RATE.outside_eu_0_vat) {
    tax_to_pay = price * 0
  } else {
    throw new Error('Unknown tax rate')
  }

  return {
    tax_rate: tax_rate,
    total: Math.ceil(tax_to_pay),
    good: good_name,
    price: Math.ceil(price)
  }
}

/**
 * Calculates the price of an array of goods
 * @param {PriceTable} price_table
 * @param {User} user - User object
 * @param {Goods} goods - Array of goods to calculate price of
 * @param {Integer} recipients_count_adjustment
 * @returns {Object} {total: <Integer>, currency: <String>}
 */
function caluclate_price (price_table, user, goods, recipients_count_adjustment, credit, currency_override) {
  let price = {total: 0, total_with_tax: 0, tax: 0, currency: user.currency, currency_symbol: to_currency_symbol(user.currency), goods: goods, good_tax: [], total_gbp: 0, goods_with_price: {}, credit_left: 0}
  if (currency_override !== undefined) {
    price = {total: 0, total_with_tax: 0, tax: 0, currency: currency_override, currency_symbol: to_currency_symbol(currency_override), goods: goods, good_tax: [], total_gbp: 0, goods_with_price: {}, credit_left: 0}
  }

  if (user.admin) {
    return price
  }

  let recipients_count = user.billing_period_recipients_count || 0
  let credit_left = parseInt(credit || 0)

  if (recipients_count_adjustment) {
    recipients_count += parseInt(recipients_count_adjustment)
  }

  for (let good_name in goods) {
    let good_price;
    if(currency_override !== undefined){
      good_price = price_good(currency_override, price_table, good_name, goods[good_name], recipients_count)
    } else {
      good_price = price_good(user.currency, price_table, good_name, goods[good_name], recipients_count)
    }
    if (good_name !== constants.GOOD.membership && credit_left > 0) {
      if (credit_left - good_price >= 0) {
        credit_left -= good_price
        good_price = 0
      } else if (good_price - credit_left >= 0) {
        good_price = good_price - credit_left
        credit_left = 0
      }
    }

    let good_tax = caluclate_good_tax(user, good_name, good_price)
    price.total += good_price
    price.tax += good_tax.total
    price.total_with_tax += good_price + good_tax.total
    price.good_tax.push(good_tax)

    price.goods_with_price[good_name] = {
      quantity: goods[good_name],
      price: good_price
    }
  }

  price.credit_left = credit_left

  if(currency_override !== undefined){
    if (currency_override === 'usd') {
      price.total_gbp = price.total * constants.CURRENCY.usd_to_gbp
    } else if (currency_override === 'eur') {
      price.total_gbp = price.total * constants.CURRENCY.eur_to_gbp
    } else if (currency_override === 'gbp') {
      price.total_gbp = price.total
    } else {
      throw new Error(`Unknown currency: ${currency_override}`)
    }
  } else {
    if (user.currency === 'usd') {
      price.total_gbp = price.total * constants.CURRENCY.usd_to_gbp
    } else if (user.currency === 'eur') {
      price.total_gbp = price.total * constants.CURRENCY.eur_to_gbp
    } else if (user.currency === 'gbp') {
      price.total_gbp = price.total
    } else {
      throw new Error(`Unknown currency: ${user.currency}`)
    }
  }

  price.total_gbp = Math.ceil(price.total_gbp)
  price.total = Math.ceil(price.total)
  price.tax = Math.ceil(price.tax)
  price.total_with_tax = Math.ceil(price.total_with_tax)
  price.credit_left = Math.ceil(price.credit_left)

  return price
}

function to_currency_symbol (currency) {
  let currency_symbol = '£'
  if (currency === 'gbp') {
    currency_symbol = '£'
  } else if (currency === 'usd') {
    currency_symbol = '$'
  } else if (currency === 'eur') {
    currency_symbol = '€'
  }

  return currency_symbol
}

function printer_cost (printer, goods) {
  let code = `
  let goods = ${JSON.stringify(goods)}
  let constants = ${JSON.stringify(constants)}
  ${printer.profit_code}

  main(goods)
  `

  let result = {code_output: undefined, code_error: undefined}

  let out = ''
  try {
    out = eval(code)
    result.code_output = out
  } catch (err) {
    result.code_error = err
  }

  return result
}

module.exports = {
  caluclate_price,
  price_good,
  goods_to_stamps,
  campaign_to_goods,
  caluclate_customer_tax_rate,
  printer_cost,
  recipient_stamp,
  campaign_recipient_batch_good,
  caluclate_good_tax,
}
