# Create
# curl -X POST "https://api.hubapi.com/automationextensions/v1/definitions?hapikey=7f8bc6c3-d2a6-449f-960a-fc2a297202be&applicationId=202755" -d @hubspot-workflow-extension-def.json --header "Content-Type: application/json"

# Update
curl -X PUT "https://api.hubapi.com/automationextensions/v1/definitions/890?hapikey=7f8bc6c3-d2a6-449f-960a-fc2a297202be&applicationId=202755" -d @hubspot-workflow-extension-def.json --header "Content-Type: application/json"

# List
# curl -X GET "https://api.hubapi.com/automationextensions/v1/definitions?hapikey=7f8bc6c3-d2a6-449f-960a-fc2a297202be&applicationId=202755"

# Delete
# curl -X DELETE "https://api.hubapi.com/automationextensions/v1/definitions/890?hapikey=7f8bc6c3-d2a6-449f-960a-fc2a297202be&applicationId=202755"
