function generateCategories (baseDate, numberOfdays) {
  const categories = []

  if (baseDate && numberOfdays) {
    let i = 0
    while (i < numberOfdays) {
      const x = baseDate
      categories.push(x)
      baseDate += 86400000
      i++
    }
  }

  return categories
}

module.exports = generateCategories
