export const CAMPAIGN_STATUS = {
  pending: 'Pending',
  in_progress: 'In progress',
  ready_to_ship: 'Ready to Ship',
  complete: 'Complete',
  error: 'Error',
  deleted: 'Deleted'
}

export const RECIPIENT_STATUS = {
  pending: 'Pending',
  in_progress: 'AI Writing',
  ready_to_print: 'Ready',
  shipped: 'Shipped',
  error: 'Error',
  deleted: 'Deleted'
}

export const CHARACTER_COUNTS = {
  'A4': 750,
  'A5': 400
}

export const MIN_DELIVERY_DATE = 4

export const MIN_UNITS = 1
export const MAX_UNITS = 4000

export const BATCH_STATUS = {
  pending: 'Pending',
  shipped: 'Shipped',
  error: 'Error'
}

export const PAPER_SIZES = {
  'A4': {'name': 'A4', key: 'A4', 'width': '210', height: '297'},
  'half_letter_landscape': {'name': 'Half Letter', key: 'half_letter_landscape', 'width': '209.55', height: '142.875'},
  'A5': {'name': 'A5', key: 'A5', 'width': '148', height: '210'},
  'A5_landscape': {'name': 'A5', key: 'A5_landscape', 'width': '210', height: '148'},
  'A6_landscape': {'name': 'A6', key: 'A6_landscape', 'width': '148', height: '105'}
}

export const ENVELOPE_SIZES = {
  'DL': {'name': 'DL', 'width': '220', height: '110'},
  'C5': {'name': 'C5', 'width': '229', height: '162'},
  'Sticker': {'name': 'Sticker', 'width': '99', height: '38'}
}

export const HANDWRITING_SIZE = {
  'small': 'Small',
  'medium': 'Medium',
  'large': 'Large'
}

export const HANDWRITING_SIZE_TOOLTIP = {
  'small': 'Set message size to small',
  'medium': 'Set message size to medium',
  'large': 'Set message size to large'
}

export const HANDWRITING_STYLE = {
  // 'George': 'George', // Whitewell
  'George': 'George', // Whitewell
  'Jane': 'Jane', // Autumn 5WGWVZ1G00XB
  // 'Ernest': 'Ernest', // Barrow
  // 'Mark': 'Mark', // Selfridge
  // 'Virginia': 'Virginia', // Thama
  // 'William': 'William', // Hampton
  'Ginsberg': 'Ginsberg', // Lafayette, 5WGWVY9R00WY
  'Dickinson': 'Dickinson',
  'Peyton': 'Peyton',
  'Easley': 'Easley',
  'Lytle': 'Lytle',
  'Parker': 'Parker',
  'Brady': 'Brady',
  'Sawdon': 'Sawdon',
  'Sutton': 'Sutton',
  'Tremblay': 'Tremblay',
  'Abrahim': 'Abrahim',
  'Mabus': 'Mabus',
  'Simon': 'Simon',
  'Freud': 'Freud',
  'Sellers': 'Sellers',
  'Robinson': 'Robinson',
  'Thomas': 'Thomas'
}

export const FONTS = {
  'Open Sans': 'Open Sans',
  'josefin': 'Josefin Sans',
  'playfair': 'Playfair Display',
  'poiret': 'Poiret One'
}

export const HEADER_TYPE = {
  'text': 'Text',
  'logo': 'Logo'
}

export const STAMP_TYPES = {
  'first_class': 'First Class',
  'second_class': 'Second Class',
  'no_stamp': 'No Stamp'
}

export const DATA_SOURCE = {
  'upload': 'Upload',
  'api_crm': 'API / CRM'
}

export const ENVELOPE_REQUIRED = {
  'yes': 'Envelope',
  'no': 'No Envelope'
}

export const SENDER = {
  'ship_for_me': 'Ship for me',
  'ship_to_self': 'Ship to self'
}

export const PRODUCT = {
  'self_service': 'Self Service',
  'full_service': 'Full Service'
}

export const DEFAULT_PRICE_ID = '1krQDzdryxWrZqT6IWiP'

export const RECIPIENTS_ERROR = {
  last_name: 0,
  address: 1,
  city: 2,
  quantity: 3,
  postcode: 4,
  country: 5,
  us_state: 6,
  unsupported_chars: 100
}

export const INSERTS = {
  'starbucks_5_dollars': '$5 Starbucks gift card',
  'starbucks_10_dollars': '$10 Starbucks gift card',
  'starbucks_15_dollars': '$15 Starbucks gift card'
}

export const GOOD = {
  self_service_a4: 'Self Service A4',
  self_service_a5: 'Self Service A5',
  self_service_a6: 'Self Service A6',
  self_service_c5: 'Self Service C5',
  self_service_c6: 'Self Service C6',
  self_service_dl: 'Self Service DL',

  full_service_a4: 'Full Service A4',
  full_service_a5: 'Full Service A5',
  full_service_a6: 'Full Service A6',
  full_service_c5: 'Full Service C5',
  full_service_c6: 'Full Service C6',
  full_service_dl: 'Full Service DL',

  insert_light: 'Insert (light)',
  stamp_gb_first_class: 'Stamp GB First Class',
  stamp_gb_second_class: 'Stamp GB Second Class',
  stamp_gb_europe: 'Stamp GB Europe',
  stamp_gb_world_zone_1: 'Stamp GB World Zone 1',
  stamp_gb_world_zone_2: 'Stamp GB World Zone 2',
  stamp_us_international: 'Stamp US International',
  stamp_us_first_class: 'Stamp US First Class',
  ship_to_self_us: 'Ship to self (US)',
  ship_to_self_gb: 'Ship to self (GB)'
}

export const SAMPLE_MESSAGE = `Dear {{{first name}}},

Thanks for requesting a sample to see one of our handwritten notes.

Whether you are sending to prospects, customers or ambassadors, personalise the customer experience and achieve better engagement rates.

 Kind regards,
   Scribeless`

export const TEAM_ROLES = {
  viewer: 'Viewer',
  editor: 'Editor'
}
