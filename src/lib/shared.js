var seed = 0

function is_tag (section) {
  var result = section.match(/<\/?(.*)>/)
  if (!result) {
    return false
  }

  return [result[1], result[0].match(/\//) ? -1 : 1]
}

function random () {
  seed = seed + 1
  var x = Math.sin(seed) * 10000
  return x - Math.floor(x)
}

function getRandomInt (seed, max) {
  return Math.floor(random() * Math.floor(max))
}

/**
 * Shuffles array in place. ES6 version
 * @param {Array} a items An array containing the items.
 */
function shuffle (seed, a) {
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(random() * (i + 1));
    [a[i], a[j]] = [a[j], a[i]]
  }
  return a
}

function wrap_characters (_seed, text, letter_variants, debug, config) {
  console.log('wrap_characters')
  if (!config) {
    config = {}
  }

  seed = 0 + _seed

  var s = text.split(/(<.*?>)/)

  let colours = ['red', 'blue', 'green', 'yellow', 'purple', 'orange', 'pink']

  var html = ''
  var count = 0
  for (var section of s) {
    var result = is_tag(section)
    if (result) {
      if (result[1] === 1) {
        html += `<span class="${result[0]}">`
      } else {
        html += '</span>'
      }
      continue
    }

    let top_max = config.top_max || 2.5
    let sleep_max = config.sleep_max || 15
    let top = -top_max
    let direction = config.direction || 0.4
    let sleep = getRandomInt(seed, sleep_max)
    let do_sleep = config.do_sleep || 0.05

    let character_variants = {}
    // let new_lines = []
    for (let character of section) {
      count++

      // if (character === '\n') {
      //   new_lines.push('\n')
      //   continue
      // } else if (new_lines.length) {
      //
      //   // TODO Weird bug with new lines, seem to be duplicating??
      //   // This is a hacky fix
      //   let new_line_count = Math.max(1, Math.ceil(((new_lines.length + 1) / 2)))
      //   for (let i=0; i<new_line_count; i++) {
      //     html += `<br>`
      //   }
      //
      //   new_lines = []
      // }

      if (!character_variants[character] || character_variants[character].length === 0) {
        let vs = []
        for (let i = 0; i < letter_variants; i++) {
          vs[i] = i + 1
        }
        character_variants[character] = shuffle(seed, vs)
      }

      let variant = character_variants[character].pop()

      let colour = 'none'
      if (debug) {
        colour = colours[variant - 1]
      }

      if (sleep) {
        sleep--
      } else {
        if (random(seed) <= do_sleep) {
          sleep = getRandomInt(seed, sleep_max)
          if (random(seed) < 0.5) {
            direction = -1 * direction
          }
        }

        top += direction
        if (top >= top_max || top <= -top_max) {
          direction = -1 * direction
        }
      }

      html += `<span style='font-feature-settings: "ss0${variant}"; background-color: ${colour}; position: relative; top: ${top}px;'>${character}</span>`
    }
  }

  return [html, count]
}

export default { wrap_characters }
