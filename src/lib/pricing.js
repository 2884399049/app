const pricing = require('../../functions/shared/pricing')

module.exports = {
  caluclate_price: pricing.caluclate_price,
  price_good: pricing.price_good,
  // build_price_table,
  caluclate_customer_tax_rate: pricing.caluclate_customer_tax_rate,
  goods_to_stamps: pricing.goods_to_stamps,
  campaign_to_goods: pricing.campaign_to_goods,
  calculate_good_tax: pricing.caluclate_good_tax
}
