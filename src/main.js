import Vue from 'vue'
import '@/lib/vue_mixin'
import App from '@/App'

import VueSweetalert2 from 'vue-sweetalert2'
import VueGtm from 'vue-gtm'
import { firestorePlugin } from 'vuefire'

import router from '@/router'

import config from '@/lib/config.js'

let version = process.env.VERSION || 'development'
console.log(`Scribeless ${process.env.NODE_ENV} version: ${version}`)

// Mixin here as breaks tests
Vue.use(VueSweetalert2)
Vue.use(firestorePlugin)

Vue.use(VueGtm, {
  id: config.google_tag_manager.id,
  debug: config.google_tag_manager.debug,
  vueRouter: router,
  enabled: !config.google_tag_manager.debug, // TODO do this with enviroments
  queryParams: {
    gtm_auth: config.google_tag_manager.gtm_auth,
    gtm_preview: config.google_tag_manager.gtm_preview,
    gtm_cookies_win: 'x'
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App />'
  // created () {
  //   firebase.initializeApp(config.firebase_config)
  // }
})
