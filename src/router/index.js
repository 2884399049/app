import Vue from 'vue'
import Router from 'vue-router'
import StationaryList from '@/pages/StationaryList'
import EditStationery from '@/pages/EditStationery'
import Campaign from '@/pages/Campaign'
import Onboarding from '@/pages/Onboarding'
import BatchList from '@/views/admin/BatchList'
import Batch from '@/views/admin/Batch'
import UserList from '@/views/admin/UserList'
import User from '@/views/admin/User'
import PrinterList from '@/views/admin/PrinterList'
import Printer from '@/views/admin/Printer'
import Analytics from '@/views/admin/Analytics'
import Team from '@/pages/Team'
import Invoices from '@/pages/Invoices'
import Referral from '@/pages/Referral'
import Settings from '@/pages/Settings'
import Integrations from '@/pages/Integrations'
import Dashboard from '@/pages/Dashboard'
import Support from '@/pages/Support'
import Billing from '@/pages/Billing'
import CampaignList from '@/pages/CampaignList'
import NewCampaign from '@/pages/NewCampaign'

import Audience from '@/pages/Audience'
import AudienceDetail from '@/pages/AudienceDetail'
import Calendar from '@/pages/Calendar'
import OnboardingQuestions from '@/components/OnboardingQuestions'

Vue.use(Router)

// https://stackoverflow.com/questions/51980296/detect-back-button-in-navigation-guards-of-vue-router
// window.popStateDetected = false
// window.addEventListener('popstate', () => {
//   window.popStateDetected = true
// })

var router = new Router({
  mode: 'history',
  routes: [
    { path: '/', name: 'Dashboard', component: Dashboard, props: true, meta: { title: `Stationery | Scribeless` } },
    { path: '/stationery', name: 'Stationery', component: StationaryList, props: true, meta: { title: `Stationery | Scribeless` } },
    { path: '/edit/stationery/:id', name: 'EditStationery', component: EditStationery, props: true, meta: { title: `Edit Stationery | Scribeless` } },

    { path: '/audience/:id', alias: '/audience/:id/*', name: 'AudienceDetail', component: AudienceDetail, props: true, meta: { title: `Audience | Scribeless` } },
    { path: '/team', name: 'Team', component: Team, meta: { title: `Team | Scribeless` } },
    { path: '/settings', name: 'Settings', component: Settings, meta: { title: `Settings | Scribeless` } },
    { path: '/integration', name: 'Integrations', component: Integrations, meta: { title: `Integrations | Scribeless` } },
    { path: '/referral', name: 'Referral', component: Referral, meta: { title: `Referral` } },
    { path: '/invoice', name: 'Invoices', component: Invoices, meta: { title: `Invoices` } },
    { path: '/audience', name: 'Audience', component: Audience, meta: { title: `Audience | Scribeless` } },
    { path: '/support', name: 'Support', component: Support, meta: { title: `Support | Scribeless` } },
    { path: '/billing', name: 'Billing', component: Billing, meta: { title: `Billing | Scribeless` } },
    { path: '/scheduled-campaigns', name: 'Calendar', component: Calendar, meta: { title: `Calendar | Scribeless` } },

    { path: '/create/:id', alias: '/create/:id/*', name: 'CreateNewCampaign', component: NewCampaign, props: true, meta: { title: `Stationery | Scribeless` } },

    {
      path: '/onboarding',
      name: 'Onboarding',
      component: Onboarding,
      meta: { title: `Onboarding | Scribeless`, hide_navbar: true },
      children: [
        // { path: 'organisation', name: 'OnboardingOrganisation', component: OnboardingOrganisation, meta: {title: `Onboarding | Scribeless`, hide_navbar: true} },
        { path: 'questions', name: 'OnboardingQuestions', component: OnboardingQuestions, meta: { title: `Onboarding | Scribeless`, hide_navbar: true } }
      ]
    },
    { path: '/campaigns', name: 'CampaignList', component: CampaignList, meta: { title: `Campaigns | Scribeless` } },
    { path: '/campaign/:id', name: 'Campaign', component: Campaign, meta: { title: `Campaign | Scribeless` } },
    { path: '/batches', name: 'BatchList', component: BatchList, meta: { title: `Batches | Scribeless` } },
    { path: '/users', name: 'UserList', component: UserList, meta: { title: `Users | Scribeless` } },
    { path: '/analytics', name: 'Analytics', component: Analytics, meta: { title: `Analytics | Scribeless` } },
    { path: '/printers', name: 'PrinterList', component: PrinterList, meta: { title: `Printers | Scribeless` } },
    { path: '/printer/:id', name: 'Printer', component: Printer, meta: { title: `Printer | Scribeless` } },
    { path: '/user/:id', name: 'User', component: User, meta: { title: `User | Scribeless` } },
    { path: '/batch/:id', name: 'Batch', component: Batch, meta: { title: `Batch | Scribeless` } },
    { path: '/logout', name: 'Logout', component: Settings, meta: { title: `Logout | Scribeless` } },
    { path: '*', redirect: { name: 'Stationery' } }
  ]
})

router.beforeEach((to, from, next) => {
  // if (from.path.indexOf('edit') > -1 && from.path.indexOf('stationery') > -1) {
  //   // Pass
  // } else if (from.path.indexOf('stationery') > -1 && to.path.indexOf('stationery') === -1 && to.path.match(/campaign\/.+/) == null) {
  //   var result = confirm('You may lose unsaved work if you go back (to go back a stage click on the stage number). Are you sure you want to go back?')
  //   window.popStateDetected = false
  //   if (!result) {
  //     next(false)
  //     return ''
  //   }
  // }
  window.popStateDetected = false
  document.title = to.meta.title
  next()
})

export default router
